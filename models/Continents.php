<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "continents".
 *
 * @property string $country
 * @property string $continent
 */
class Continents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'continents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'continent'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getAllContinents()
    {
        $continents = self::find()->select('continent')->distinct()->all();
        return ArrayHelper::getColumn($continents, 'continent');
    }
}
