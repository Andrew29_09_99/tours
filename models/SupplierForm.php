<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 11.05.2017
 * Time: 18:41
 */

namespace app\models;

use yii\base\Model;
use yii\db\Exception;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\BaseFileHelper;
use Yii;

class SupplierForm extends Model
{
    public $userId;
    public $nameProvider;
    public $regNumberProvider;
    public $countryId;
    public $country;
//    public $region;
    public $city;
    public $zipProvider;
    public $addressProvider;
    public $descProvider;
    public $document;
    public $residence;
    public $phoneProvider;
    public $emailProvider;

    public function rules() {
        return [
            // name
            'name'              => ['nameProvider', 'string', 'min' => 3],
            'nameTrim'          => ['nameProvider', 'filter', 'filter' => 'trim'],
            'nameRequired'      => ['nameProvider', 'required'],

            // registration number
            'regNumber'         => ['regNumberProvider', 'string'],
            'regNumberTrim'     => ['regNumberProvider', 'filter', 'filter' => 'trim'],
            'regNumberRequired' => ['regNumberProvider', 'required'],

            // all location
            'location'          => [['country', 'city', 'countryId'], 'safe'],
            'locationRequired'  => [['country', 'city'], 'required'],

            //zipcode
            'zipcode'           => ['zipProvider', 'string'],
            'zipcodeTrim'       => ['zipProvider', 'filter', 'filter' => 'trim'],
            'zipcodeRequired'   => ['zipProvider', 'required'],

            //address
            'address'           => ['addressProvider', 'string'],
            'addressRequired'   => ['addressProvider', 'required'],

            //short company description
            'description'       => ['descProvider', 'string', 'max' => 500],

            //file
            'file'              => [['document'], 'file', 'extensions' => 'png, jpg, gif, pdf', 'maxFiles' => 10, 'maxSize' => 5*1024*1024],
            'fileRequired'      => ['document', 'required'],

            //phone and email
            'phone'             => ['phoneProvider', 'string'],
            'phoneRequired'     => ['phoneProvider', 'required'],
            'email'             => ['emailProvider', 'string'],
            'emailRequired'     => ['emailProvider', 'required'],
        ];
    }

    public function attributeLabels() {
        return [
            'nameProvider'    => 'Company\'s name',
            'regNumber' => 'Company registration number',
            'country' => 'Country',
            'region' => 'State/Region',
            'city' => 'City',
            'zipcode' => 'Zipcode',
            'address' => 'Address',
            'description' => 'Short company description',
            'document' => 'file',
        ];
    }

    public function upload() {
        if($this->validate()) {
            $path_to_file = 'uploads/docs/'. Yii::$app->user->identity->getId();

            if(!is_dir($path_to_file)){
                FileHelper::createDirectory($path_to_file, 0755);
            }
            foreach ($this->document as $file) {
                $file->saveAs($path_to_file .'/'. $file->baseName . '.' . $file->extension);
            }
            return true;
        } else {
            return false;
        }
    }

    public function save(){
        if($this->validate()){
            $supplier = new Providers;
            $this->loadAttributes($supplier);
//            debug($this);
//            die;
            /*$supplier->userId = Yii::$app->user->identity->getId();
            $supplier->nameProvider = $this->nameProvider;
            $supplier->regNumberProvider = $this->regNumber;
            $supplier->descProvider = $this->description;
            $supplier->addressProvider = $this->address;
            $supplier->zipProvider = $this->zipcode;
            $supplier->phoneProvider = $this->phoneSupplier;
            $supplier->emailProvider = $this->emailSupplier;*/
            if(!$supplier->save()){
                /* ToDo add Exception */
            }
            $this->upload();

            $supplierLocation = new ProviderLocation;
            $supplierLocation->userId = $this->userId;

            if($country = Countries::findOne($this->countryId)){
                $supplierLocation->countryId = $country->id;
            } else {
                $country = new Countries;
                $country->id = $this->countryId;
                $country->name = $this->country;
                if(!$country->save()){
//                    Exception
                    /* ToDo add Exception */
                }
                $supplierLocation->countryId = $this->countryId;
            }
            if($city = Cities::findOne(['name' => $this->city, 'countryId' => $this->countryId])) {
                $supplierLocation->cityId = $city->id;
            } else {
                $city = new Cities;
                $city->name = $this->city;
                $city->countryId = $this->countryId;
                if(!$city->save()){
//                    Exception
                    /* ToDo add Exception */
                }
                $supplierLocation->cityId = $this->city;
            }
            if(!$supplierLocation->save()){
//                    Exception
                    /* ToDo add Exception */
            }

            foreach ($this->document as $file) {
                $docs = new ProviderDocs;
                $docs->userId = Yii::$app->user->identity->getId();
                $docs->srcDoc = $file->baseName . '.' . $file->extension;
                $docs->save();
            }
        }
        else {
            return false;
        }
        return true;
    }

    protected function loadAttributes(Providers $supplier)
    {
        $supplier->setAttributes($this->attributes);
    }

}