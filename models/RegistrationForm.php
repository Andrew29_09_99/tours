<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 25.04.2017
 * Time: 14:22
 */

namespace app\models;

use dektrium\user\models\RegistrationForm as DektiumRegistrationForm;
use app\models\User;
use Yii;

class RegistrationForm extends DektiumRegistrationForm
{

    public $id;
    /**
     * @var string User first name (aka John)
     */
    public $firstname;

    /**
     * @var string User last name (aka Doe)
     */
    public $lastname;

    /**
     * @var string confirm password
     */
    public $confirm;

    /**
     * @inheritdoc overriding rules with first and last name and remove username
     */
    public function rules()
    {
        $user = $this->module->modelMap['User'];

        return [
            // username rules
            'usernameLength'   => ['username', 'string', 'min' => 3, 'max' => 255],
            /*'usernameTrim'     => ['username', 'filter', 'filter' => 'trim'],
            'usernamePattern'  => ['username', 'match', 'pattern' => $user::$usernameRegexp],
            'usernameRequired' => ['username', 'required'],
            'usernameUnique'   => [
                'username',
                'unique',
                'targetClass' => $user,
                'message' => Yii::t('user', 'This username has already been taken')
            ],
*/
            // first name rules
         //   'firstnameTrim'     => ['firstname', 'filter', 'filter' => 'trim'],
          //  'firstnameRequired' => ['firstname', 'required'],
           // 'firstnamePattern'   => ['firstname', 'match', 'pattern' => $user::$firstAndLastNameRegexp],
            'firstnameLength'   => ['firstname', 'string', 'max' => 50],

            // first name rules
        //    'lastnameTrim'     => ['lastname', 'filter', 'filter' => 'trim'],
         //   'lastnameRequired' => ['lastname', 'required'],
         //   'lastnamePattern'   => ['lastname', 'match', 'pattern' => $user::$firstAndLastNameRegexp],
            'lastnameLength'   => ['lastname', 'string', 'max' => 75],

            // email rulesPasswords don't match
            'emailTrim'     => ['email', 'filter', 'filter' => 'trim'],
            'emailRequired' => ['email', 'required'],
            'emailPattern'  => ['email', 'email'],
//            'emailDNS'      => [['email', 'checkDNS' => 'true'], 'message' => 'False DNS'],
            'emailUnique'   => [
                'email',
                'unique',
                'targetClass' => $user,
                'message' => Yii::t('user', 'This email address has already been taken')
            ],
            // password rules
            'passwordRequired' => ['password', 'required', 'skipOnEmpty' => $this->module->enableGeneratingPassword],
            'passwordLength'   => ['password', 'string', 'min' => 6, 'max' => 72],

            //confirm (password) rules
            'confirmRequired' => ['confirm', 'required', 'skipOnEmpty' => $this->module->enableGeneratingPassword],
            'confirmLength'   => ['confirm', 'string', 'min' => 6, 'max' => 72],
            'confirmCompare'  => ['confirm', 'compare', 'compareAttribute' => 'password', 'message' => 'Passwords don\'t match']
        ];
    }

    /**
     * @inheritdoc overriding attributeLabels with first and last name and remove username
     */

    public function attributeLabels(){
        return [
            'email'    => Yii::t('user', 'Email'),
            'firstname' => Yii::t('user', 'First name'),
            'lastname' => Yii::t('user', 'Last name'),
            'password' => Yii::t('user', 'Password'),
            'confirm' => Yii::t('user', 'Confirm Password')
        ];
    }

    public function register()
    {
        if (!$this->validate()) {
            return false;
        }

        /** @var User $user */
        $user = Yii::createObject(User::className());
//        $profile = Yii::createObject(Profile::className());

        $user->setScenario('register');
        $this->loadAttributes($user);

        
		
		
			$firstpartemail='';
			if($user->email)
			{
				$dataemail = explode('@',$user->email);
				
				$firstpartemail = $dataemail[0];
			}
			
			
		
		$user->username = $firstpartemail;


        /*$profile->firstname = $user->firstname;
        $profile->lastname = $user->lastname;
        $profile->public_email = $user->email;
        $profile->save();*/

        if (!$user->register()) {
            return false;
        }
//        debug($profile);
//        die;
        /** @var Profile $profile */
        $profile = Yii::createObject(Profile::className());
        $this->loadProfileAttributes($profile);
        $profile->save();

        $userRole = Yii::$app->authManager->getRole('user');
        Yii::$app->authManager->assign($userRole, $user['id']);


        Yii::$app->session->setFlash(
            'info',
            Yii::t(
                'user',
                'Your account has been created and a message with further instructions has been sent to your email. <br/>
                Please check email and complete your registration.'
            )
        );

        return true;
    }

    protected function loadProfileAttributes(Profile $profile)
    {
        $profile->setAttributes($this->attributes);
    }

}