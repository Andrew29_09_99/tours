<?php
//modify by mischenkoa@ukr.net at 20171113 23:14
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tour".
 *
 * @property integer $id
 * @property string $name
 * @property integer $providerId
 * @property integer $typeId
 * @property double $priceAdult
 * @property double $priceChild
 * @property double $priceInfant
 * @property double $priceSenior
 * @property string $descShort
 * @property string $descLong
 * @property string $descExtra
 * @property string $inclusion
 * @property string $exclusion
 * @property string $addInfo
 * @property string $itinerary
 * @property string $duration
 * @property string $payMethod
 * @property integer $hotelPickup
 * @property integer $status
 * @property integer $showMain
 *
 * @property Favorites[] $favorites
 * @property Groups[] $groups
 * @property Messages[] $messages
 * @property Orders[] $orders
 * @property PickupPoints[] $pickupPoints
 * @property Ratings[] $ratings
 * @property Reviews[] $reviews
 * @property Providers $provider
 * @property TourTypes $type
 * @property TourLocation[] $tourLocations
 * @property TourPhotos[] $tourPhotos
 */
class Tour extends \yii\db\ActiveRecord
{
    /**
     * The additional attributes are needed to create wright itinerary
     */
    public $itineraryTime;
    public $itineraryDesc;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tour';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','descShort','descLong'], 'required'],
            [['name'], 'string', 'max' => 70],
            [['priceAdult', 'deposit', 'priceChild', 'priceInfant', 'priceSenior', 'status','NetpriceAdult','NetpriceChild', 'NetpriceInfant', 'NetpriceSenior','desireCurrency'], 'number'],
            [['descShort'], 'string', 'min' => 160, 'max' => 220],
            [['descLong'], 'string', 'min' => 450, 'max' => 15000],
            [['status'], 'number', 'min' => 0, 'max' => 1],
            [['itinerary'], 'string'],
            [['descExtra', 'inclusion', 'exclusion', 'payMethod','AgeTypePrice','voucherType'], 'string'],
            [['addInfo'], 'string', 'max' => 500],
            [['hotelPickup'], 'string'],
            [['providerId', 'typeId', 'duration', 'status', 'showMain','itsclone'], 'integer'],
            [['providerId'], 'exist', 'skipOnError' => true, 'targetClass' => Providers::className(), 'targetAttribute' => ['providerId' => 'userId']],
            [['typeId'], 'exist', 'skipOnError' => true, 'targetClass' => TourTypes::className(), 'targetAttribute' => ['typeId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Tour name (max 70 characters)',
            'typeId' => 'Tour type',
            'priceAdult' => 'Adult',
            'priceChild' => 'Child',
            'priceInfant' => 'Infant',
            'priceSenior' => 'Senior',
			'NetpriceAdult' => 'Net price Adult',
            'NetpriceChild' => 'Net price Child',
            'NetpriceInfant' => 'Net price Infant',
            'NetpriceSenior' => 'Net price Senior',
			'desireCurrency' => 'desire Currency',
            'descShort' => 'Short description (max 220 characters)',
            'descLong' => 'Full description (max 450 characters)',
            'descExtra' => 'Add some short extra information to your description (max 6 items)',
            'inclusion' => 'Inclusions',
            'exclusion' => 'Exclusions',
            'itinerary' => 'Time period and description',
            'addInfo' => 'Additional information (max 500 characters)',
            'duration' => 'Duration',
            'payMethod' => 'Choose your payment methods',
            'hotelPickup' => 'Hotel Pick-Up and Drop-Off',
			'AgeTypePrice'=>'Age from to',
			'voucherType'=>'voucher Type',
			'itsclone'=>'itsclone'
        ];
    }

    public static function wordsToSeconds($array) {
        $words = "";

        /*** set the days ***/
        /*if(!empty($array[0])) {
            $words .= "$array[0] days";
        }
		*/
		if($array[0]) {$words .= "$array[0] days";}
		
        /*** set the hours ***/
        if(!empty($array[1])) {
            if($array[0]){
				$words .= ", $array[1] hours";
			}
			else
			{
				$words .= "$array[1] hours";
			}
        }

        /*** set the minutes ***/
        if(!empty($array[2])) {
			if($array[1])
			{
				$words .= ", $array[2] minutes";
			}
			else
			{
				$words .= "$array[2] minutes";
			}
        }
        
        $seconds = strtotime($words, 0);

        return $seconds;
    }

    public static function secondsToWords($seconds) {
        $words = "";

        /*** get the days ***/
        $days = intval(intval($seconds) / (3600*24));
		
        if($days > 0)
		{
            $words .= "$days day(s) ";
        }
		
		

        /*** get the hours ***/
        $hours = (intval($seconds) / 3600) % 24;
        if($hours > 0) {
            $words .= "$hours hour(s) ";
        }
		

        /*** get the minutes ***/
        $minutes = (intval($seconds) / 60) % 60;
        if($minutes > 0) {
            $words .= "$minutes minute(s)";
        }

        return $words;
    }

    public static function secondsToValuesArray($seconds) {
        $valuesArray = [];

        /*** get the days ***/
        $days = intval(intval($seconds) / (3600*24));
        if($days > 0) {
            $valuesArray[] = $days;
        }
		else
		{
			$valuesArray[]='';
		}

        /*** get the hours ***/
        $hours = (intval($seconds) / 3600) % 24;
        if($hours > 0) {
            $valuesArray[] = $hours;
        }
		else
		{
			$valuesArray[]='';
		}

        /*** get the minutes ***/
        $minutes = (intval($seconds) / 60) % 60;
        if($minutes > 0) {
            $valuesArray[] = $minutes;
        }

        return $valuesArray;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavorites()
    {
        return $this->hasMany(Favorites::className(), ['tourId' => 'id']);
    }

    /**
     *
     */
    public function isUserFavorites()
    {
        if (!empty(Yii::$app->user->identity)) {
            $userId = Yii::$app->user->identity->getId();
            return $this->getFavorites()
                ->andWhere(['userId' => $userId])
                ->one();
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Groups::className(), ['tourId' => 'id'])->orderBy('price ASC');
    }

    /**
     *
     */
    public function getGroupsList()
    {
        
		return ArrayHelper::map($this->groups, 'id', 'name');
    }
	
	public function getRangeGroupsfromto()
    {
        $arr = array();
		if($this->groups)
		{
			foreach ($this->groups as $key=>$value)
			{
				$arr[$value['id']] = $value['name'].' ('.$value['peopleFrom'].'-'.$value['peopleTo'].' PAX)';
			}
		}
		return $arr;
    }
   
    public function getMessages()
    {
        return $this->hasMany(Messages::className(), ['tourId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['tourId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPickupPoints()
    {
        return $this->hasMany(PickupPoints::className(), ['tourId' => 'id']);
    }

    /**
     *
     */
    public function getPickupPointsList()
    {
        return ArrayHelper::map($this->pickupPoints, 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatings()
    {
        return $this->hasMany(Ratings::className(), ['tourId' => 'id'])->average('value');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), ['tourId' => 'id']);
    }

    /**
     * 
     */
    public function getCountReviews()
    {
        return $this->getReviews()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Providers::className(), ['userId' => 'providerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'userId'])->via('provider');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TourTypes::className(), ['id' => 'typeId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourLocations()
    {
        return $this->hasOne(TourLocation::className(), ['tourId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasOne(Countries::className(), ['id' => 'countryId'])
            ->via('tourLocations');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasOne(Cities::className(), ['id' => 'cityId'])
            ->via('tourLocations');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourPhotos()
    {
        return $this->hasMany(TourPhotos::className(), ['tourId' => 'id'])
            ->where(['reviewId' => null]);
    }

    /**
     * 
     */
    public function getPromoPhoto()
    {
        $promoPhoto = $this->getTourPhotos()
            ->andWhere(['isPromo' => 1])
            ->one();
        
        return $promoPhoto;
    }

    /**
     *
     */
    public function getAllPhotos()
    {
        $allPhotos = $this->getTourPhotos()
            ->andWhere(['isPromo' => 0])
            ->all();

        return $allPhotos;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourDates()
    {
        return $this->hasMany(TourDates::className(), ['tourId' => 'id']);
    }

    /**
     *
     */
    public function getGuideDates($languageId)
    {
        $guideDates = $this->getTourDates()
            ->andWhere(['languageId' => $languageId])
            ->one();
        
        return json_encode(explode('; ', $guideDates->dates));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuides()
    {
        return $this->hasMany(GuideLanguages::className(), ['id' => 'languageId'])
            ->via('tourDates');
    }

    /**
     *
     */
    public function getGuidesList()
    {
        return ArrayHelper::map($this->guides, 'id', 'language');
    }

    /**
     * @return integer
     */
	 public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'desureCurrency']);
    } 
    public function getMinimalPrice()
    {
        
		if (!empty($this->groups[0]->price)) {
            $minimalPrice = $this->groups[0]->price;
        } else {
            if (!empty($this->priceAdult)) {
                $minimalPrice = $this->priceAdult;
            } else if (!empty($this->priceChild)) {
                $minimalPrice = $this->priceChild;
            } else if (!empty($this->priceInfant)) {
                $minimalPrice = $this->priceInfant;
            } else if (!empty($this->priceSenior)) {
                $minimalPrice = $this->priceSenior;
            }
        }

        return $minimalPrice;
    }
	public function getNetMinimalPrice()
    {
        if ($this->groups[0]->Netprice) 
		{
            $NetminimalPrice = $this->groups[0]->Netprice;
        } 
		else
		{
            if (!empty($this->NetpriceAdult))
			{
                $NetminimalPrice = $this->NetpriceAdult;
            } else if (!empty($this->NetpriceChild)) {
                $NetminimalPrice = $this->NetpriceChild;
            } else if (!empty($this->NetpriceInfant)) {
                $NetminimalPrice = $this->NetpriceInfant;
            } else if (!empty($this->NetpriceSenior)) {
                $NetminimalPrice = $this->NetpriceSenior;
            }
        }

        return $NetminimalPrice;
    }
}
