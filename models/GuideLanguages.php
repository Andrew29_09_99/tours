<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "guideLanguages".
 *
 * @property integer $id
 * @property string $language
 * @property integer $sort
 * 
 * @property Orders[] $orders
 * @property TourDates[] $tourDates
 */
class GuideLanguages extends ActiveRecordSort
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guideLanguages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort'], 'integer'],            
            [['language'], 'string', 'max' => 255],
            [['sort'], 'unique'],            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'sort' => 'Sorting',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['languageId' => 'id']);
    }

    public static function getLanguagesList() {
        $guideLanguages = self::find()->orderBy('sort')->all();
        $languagesList = ArrayHelper::map($guideLanguages, 'id', 'language');

        return $languagesList;
    }
   
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPickupPoints()
    {
        return $this->hasMany(PickupPoints::className(), ['languageId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourDates()
    {
        return $this->hasMany(TourDates::className(), ['languageId' => 'id']);
    }    
    
}
