<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "guideLanguages".
 *
 * @property integer $id
 * @property string $language
 * @property integer $sort
 * 
 * @property Orders[] $orders
 * @property TourDates[] $tourDates
 */
class ActiveRecordSort extends \yii\db\ActiveRecord
{
    /* Поднимает, опускает расположение сортировки языка путём обмена значений
     * @param string $dir направление обмена (up - вверх, down - вниз)
     */
    public function swap($dir) {
        switch ($dir) { // Поиск значения с которім нужно меняться
            case 'up':  $element = self::find()->orderBy('sort DESC')->where('sort < ' . $this->sort)->limit(1)->all(); break;
            case 'down': $element = self::find()->orderBy('sort ASC')->where('sort > ' . $this->sort)->limit(1)->all(); break;
        }  
        if (count($element) < 1) return;
        $sort = $this->sort;
        self::updateAll(['sort' => NULL], ['id' => $element[0]->id]);
        $this->sort = $element[0]->sort;
        $this->save();
        self::updateAll(['sort' => $sort], ['id' => $element[0]->id]);

    }
    
}
