<?php
namespace app\models;
use Yii;

class Currency extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'currency';
    }

    public function rules()
    {
        return [
            [['alias', 'name', 'crosskurs'], 'required'],
            [['crosskurs'], 'number'],
            [['alias'], 'string', 'max' => 10],
            [['name'], 'string', 'max' => 20]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'alias' => Yii::t('app', 'Alias'),
            'name' => Yii::t('app', 'Name'),
            'crosskurs' => Yii::t('app', 'Crosskurs'),
        ];
    }
}
