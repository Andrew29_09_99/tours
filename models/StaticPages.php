<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "staticPages".
 *
 * @property integer $id
 * @property string $title_id
 * @property string $title
 * @property string $text
 */
class StaticPages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'staticPages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['title_id', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_id' => 'Title ID',
            'title' => 'Title',
            'text' => 'Text',
        ];
    }
}
