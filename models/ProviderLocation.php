<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "providerLocation".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $countryId
 * @property integer $cityId
 *
 * @property Cities $city
 * @property Countries $country
 * @property Providers $user
 */
class ProviderLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providerLocation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'countryId', 'cityId'], 'required'],
            [['userId', 'cityId'], 'integer'],
            [['countryId'], 'string', 'max' => 255],
            [['cityId'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['cityId' => 'id']],
            [['countryId'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['countryId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => Providers::className(), 'targetAttribute' => ['userId' => 'userId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'countryId' => 'Country ID',
            'cityId' => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'cityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'countryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Providers::className(), ['userId' => 'userId']);
    }

    /**
     * @param $userId - userId Provider
     * @param $supplierCountries - current name Country of Provider
     * @param $supplierCities - current name City of Provider
     * @param $modifiedCountries - change name Country of Provider
     * @param $modifiedCities - change name City of Provider
     */
    public static function editProviderLocation($userId, $supplierCountries, $supplierCities, $modifiedCountries, $modifiedCities){
        $location = ProviderLocation::findOne(['userId' => $userId]);

        if($modifiedCountries['name'] != $supplierCountries['name']){

            $continent = Continents::find()->where(['country' => $modifiedCountries['id']])->one();

            if($country = Countries::findOne(['id' => $modifiedCountries['id']])){
                $location->countryId = $country->id;
            } else {
                $country = new Countries;
                $country->id = $modifiedCountries['id'];
                $country->name = $modifiedCountries['name'];
                $country->continent = $continent['continent'];
                if(!$country->save()){
//                    debug($country->errors);
//                    Exception
                    /* ToDo add Exception */
                }
                $location->countryId = $modifiedCountries['id'];
            }

        }
        if($modifiedCities['name'] != $supplierCities['name']){
            if($city = Cities::findOne(['name' => $modifiedCities['name'], 'countryId' => $modifiedCountries['id']])) {
                $location->cityId = $city->id;
            } else {
                $city = new Cities;
                $city->name = $modifiedCities['name'];
                $city->countryId = $modifiedCountries['id'];
                if(!$city->save()){
//                    Exception
                    /* ToDo add Exception */
                }
                $location->cityId = $modifiedCities['id'];
            }

        }

        $location->save();

        return true;
    }
}
