<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 07.07.2017
 * Time: 16:56
 */

namespace app\models;


use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use Yii;

class UploadAvatarForm extends Model
{

    public $uploadImageUser;

    public function rules(){
        return [
          [['uploadImageUser'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, gif', 'message' => ''],
        ];
    }

    public function upload(){
        if($this->validate()){
            $path_to_file = 'uploads/usr/'. Yii::$app->user->identity->getId();

            /* remove previous avatar */
            $files = glob($path_to_file."/*");
            foreach ($files as $file){
                if(!is_file($file)) continue;
                unlink($file);
            }
            /* if not exist directory for current user - create this directory */
            if(!is_dir($path_to_file)){
                BaseFileHelper::createDirectory($path_to_file);
            }
            $newImageName = md5(rand(0, getrandmax()));
            $this->uploadImageUser->saveAs($path_to_file .'/'. $newImageName . '.' . $this->uploadImageUser->extension);
            return $newImageName . '.' . $this->uploadImageUser->extension;
        }
        else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'uploadImageUser' => ''
        ];
    }

}