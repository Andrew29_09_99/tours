<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "orders".
 *
 * @property integer $orderId
 * @property integer $cartId
 * @property integer $tourId
 * @property integer $userId
 * @property integer $providerId
 * @property double $payPrice
 * @property double $totalPrice
 * @property integer $totalAdult
 * @property integer $totalInfant
 * @property integer $totalSenior
 * @property integer $totalChild
 * @property integer $groupId
 * @property string $dateBuy
 * @property string $languageId
 * @property string $dateStartTour
 * @property string $leadFirstname
 * @property string $leadLastname
 * @property string $leadPhone
 * @property string $leadEmail
 * @property string $pickInfo
 * @property integer $orderStatus
 * @property string $voucher
 * @property integer $payStatus
 * @property string $chargeId
 * @property integer $isReview
 *
 * 0 - not viewed, 1 - viewed by supplier, 2 - viewed by user
 * @property integer $viewedStatus

 * @property Groups $group
 * @property Profile $user
 * @property Providers $provider
 * @property Tour $tour
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderId', 'cartId', 'tourId', 'userId', 'providerId', 'payPrice', 'totalPrice', 'languageId',
                'dateStartTour', 'leadFirstname', 'leadLastname', 'leadPhone', 'leadEmail', 'pickInfo'], 'required'],
            [['orderId', 'cartId', 'tourId', 'providerId', 'totalAdult', 'totalInfant', 'totalSenior',
                'totalChild', 'groupId', 'languageId', 'orderStatus', 'payStatus', 'isReview', 'viewedStatus'], 'integer'],
            [['userId', 'chargeId'], 'string'],
            [['payPrice', 'totalPrice'], 'number'],
            [['dateStartTour'], 'string', 'length' => [8, 10]],
            [['leadFirstname'], 'string', 'max' => 50],
            [['leadLastname'], 'string', 'max' => 75],
            ['voucher', 'string', 'max' => 20],
            ['voucher', 'unique'],
            [['leadPhone'], 'match', 'pattern' => '/^[+\-\(\)0-9]{1,25}$/'],
            [['leadEmail'], 'email'],
            [['leadEmail', 'pickInfo'], 'string', 'max' => 255],
            [['languageId'], 'exist', 'skipOnError' => true, 'targetClass' => GuideLanguages::className(), 'targetAttribute' => ['languageId' => 'id']],
            [['groupId'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['groupId' => 'id']],
            [['providerId'], 'exist', 'skipOnError' => true, 'targetClass' => Providers::className(), 'targetAttribute' => ['providerId' => 'userId']],
            [['tourId'], 'exist', 'skipOnError' => true, 'targetClass' => Tour::className(), 'targetAttribute' => ['tourId' => 'id']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['scenario_formBookNow'] = ['cartId', 'tourId', 'userId', 'providerId', 'payPrice', 'totalPrice', 'totalAdult', 'totalInfant', 'totalSenior', 'totalChild', 'groupId', 'languageId', 'dateStartTour'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cartId' => 'Cart ID',
            'orderId' => 'Order ID',
            'tourId' => 'Tour',
            'userId' => 'User ID',
            'providerId' => 'Provider ID',
            'payPrice' => 'Pay Price',
            'totalPrice' => 'Total Price',
            'totalAdult' => 'Adults',
            'totalInfant' => 'Infants',
            'totalSenior' => 'Seniors',
            'totalChild' => 'Children',
            'groupId' => 'Group ID',
            'dateBuy' => 'Date Buy',
            'dateStartTour' => 'Date',
            'pickInfo' => 'Pick Info',
            'orderStatus' => 'Status',
            'typeId' => 'Tour type',
            'languageId' => 'Guide',
            'voucher'  => 'Voucher',
            'payStatus' => 'Pay Status',
            'isReview' => 'Is Review',
        ];
    }

    /**
     *
     */
    public function setTotalPrice()
    {
        if (empty($this->groupId)) 
		{
            
			$travellersPrices=[];
			
            $this->totalPrice = 0;
            foreach($travellersPrices as $travellers => $price) 
			{
                if (!empty($travellers))
				{
                    $this->totalPrice += $travellers * $price;
				
                }
            }
        }
		else 
		{
            $this->totalPrice = $this->group->price;
        }
        
        return $this->totalPrice;
    }
	
	 public function setTotalPriceNew()
    {
        if (empty($this->groupId)) 
		{
           
			$travellersPrices=[];
			$travellersPrices = array(0=>array($this->totalAdult => $this->tour->priceAdult),1=>array($this->totalChild => $this->tour->priceChild),2=>array($this->totalInfant => $this->tour->priceInfant),3=>array($this->totalSenior => $this->tour->priceSenior));
			
            $this->totalPrice = 0;
            foreach($travellersPrices as $keyArr) 
			{
            	foreach($keyArr as $countTrav=>$price)
				if($countTrav)
				{
                    $this->totalPrice += $countTrav * $price;
			    }
            }
        }
		else 
		{
            //echo '<pre>';print_r($this->group);echo '</pre>';die;
			$this->totalPrice = $this->group->price;
        }
        return $this->totalPrice;
    }
	
	
    /**
     *
     */
    public function setPayPrice()
    {
        return $this->payPrice = (Yii::$app->params['depositPercentage'] / 100) * $this->totalPrice;
    }
	
	public function setPayPriceNew()
    {
        
		$deposit=0;
		if(empty($this->groupId)) 
		{
			$depositAdults = ($this->totalAdult)?($this->tour->priceAdult - $this->tour->NetpriceAdult)*$this->totalAdult:0;
			$depositChild = ($this->totalChild)?($this->tour->priceChild - $this->tour->NetpriceChild)*$this->totalChild:0;
			$depositInfant = ($this->totalInfant)?($this->tour->priceInfant - $this->tour->NetpriceInfant)*$this->totalInfant:0;
			$depositSenior = ($this->totalSenior)?($this->tour->priceSenior - $this->tour->NetpriceSenior)*$this->totalSenior:0;
			$deposit = $depositAdults + $depositChild + $depositInfant + $depositSenior;
		}
		else
		{
			$deposit = ($this->totalPrice)?($this->group->price - $this->group->Netprice):0;
			//echo '<pre>';print_r($this->groups);echo '</pre>';die;
		}
		return $this->payPrice = $deposit;
    }
    /**
     *
     */
    public function setCartId()
    {
        $unpaidOrders = self::find()->where(['userId' => $this->userId, 'payStatus' => '0'])->one();
        if (!empty($unpaidOrders)) {
            $this->cartId = $unpaidOrders->cartId;
        } else {
            $this->cartId = $this->getDb()->createCommand('SELECT(UUID_SHORT())')->queryScalar();
        }
        return $this->cartId;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'groupId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Providers::className(), ['userId' => 'providerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(Tour::className(), ['id' => 'tourId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuide()
    {
        return $this->hasOne(GuideLanguages::className(), ['id' => 'languageId']);
    }

    /**
     *
     */
    public function getPickupPoint()
    {
        if (is_numeric($this->pickInfo)) {
            return $this->hasOne(PickupPoints::className(), ['id' => 'pickInfo']);
        } else {
            return false;
        }
    }

    /**
     *
     */
    public function getTravellers()
    {
        if (!empty($this->groupId)) {
            $travellers = $this->group->name;
        } else {
            $travellers = $this->totalAdult + $this->totalInfant + $this->totalSenior + $this->totalChild;
        }

        return $travellers;
    }

    /**
     *
     */
    public function getTypeId()
    {
        return $this->tour->typeId;
    }

    public function getProviderOrder(){
        return $this->provider->nameProvider;
    }

    public function getCustomer(){
        return $this->user->firstname.' '.$this->user->lastname;
    }

    public function getNameTour(){
        return $this->tour->name;
    }
}
