<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tour;
use yii\db\Query;

/**
 * TourSearch represents the model behind the search form about `app\models\Tour`.
 */
class TourSearch extends Tour
{
    // default value for the range slider init (values from , to)
    public $ratings = '0,5';
    public $continent;
    public $country;
    public $city;
    public $supplier;
    public $type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'providerId', 'typeId', 'priceAdult', 'priceChild', 'priceInfant', 'priceSenior',
                'descShort', 'descLong', 'descExtra', 'inclusion', 'exclusion', 'addInfo', 'itinerary', 'duration',
                'payMethod', 'hotelPickup', 'status', 'showMain', 'ratings', 'continent', 'country', 'city', 'supplier', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tour::find()->where(['status' => 1])->joinWith('countries')->joinWith('cities');
        $subQuery = Ratings::find()
            ->select('tourId, AVG(value) as rating')
            ->groupBy('tourId');
        $query->leftJoin(['ratings' => $subQuery], 'ratings.tourId = tour.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'ratings' => [
                        'asc' => ['ratings.rating' => SORT_ASC],
                        'desc' => ['ratings.rating' => SORT_DESC],
                        'label' => 'RATING',
                    ],
                    'priceAdult' => [
                        'label' => 'PRICE',
                    ],
                    'name' => [
                        'label' => 'NAME',
                    ],
                    'deposit' => [
                        'label' => 'REQUIRED BOOKING DEPOSIT',
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 5
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'typeId' => $this->typeId,
        ]);

        // split ratings by values "from-to"
        $ratings = explode(',', $this->ratings);

        $query->andFilterWhere(['like', 'tour.name', $this->name])
            ->andFilterWhere(['like', 'countries.continent', $this->continent])
            ->andFilterWhere(['like', 'countries.id', $this->country])
            ->andFilterWhere(['like', 'cities.id', $this->city])
            ->andFilterWhere(['between', 'ratings.rating', ($ratings[0] > 0) ? $ratings[0] : null, $ratings[1]]);

        return $dataProvider;
    }

    /**
     * Model search for display all tours (for admin and det. manager)
     *
     * $uid use if we need to display tours of selected supplier
     * $sort use if we need to sorted tours by deposit
     *
     * @param $params
     * @param integer $uid
     * @param integer $sort (parameter for sorting (deposit))
     * @return ActiveDataProvider
     */
    public function adminSearch($params, $uid = null, $sort = null){
//        $query = Tour::find()->joinWith(['profile','cities','countries']);
        $subQuery = (new Query())
            ->select(['tourId', 'src', 'isPromo'])
            ->from(['tourPhotos'])
            ->where(['isPromo' => 1]);

        $countQuery = (new Query())
            ->select(['tourId', 'count(orderId) as countOrders'])
            ->from(['orders'])
            ->groupBy(['tourId']);

        $query = (new Query())
            ->select(['t.*', 't.id as tourId', 't.status as tourStatus', 'tt.*', 'o.countOrders', 'p.*', 'tp.*'])
            ->from(['t' => 'tour'])
            ->leftJoin((['tp' => $subQuery]), 'tp.tourId = t.id')
            ->leftJoin((['tt' => 'tourTypes']), 'tt.id = t.typeId')
            ->leftJoin((['o' => $countQuery]), 'o.tourId = t.id')
            ->leftJoin((['p' => 'providers']), 'p.userId = t.providerId');

        if($uid && !is_null($uid)){
            $query->where(['t.providerId' => $uid]);
        }

        if($sort && !is_null($sort)){
            $query->orderBy($sort->orders);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
//                    'priceAdult' => [
//                        'label' => 'Price',
//                    ],
//                    'name' => [
//                        'label' => 'name',
//                    ],
                    'deposit' => [
                        'label' => 'Deposit',
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],

        ]);

        $dataProvider->setSort([
            'attributes' => [
                'deposit'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'priceAdult', $this->priceAdult])
            ->andFilterWhere(['like', 'deposit', $this->deposit])
            ->andFilterWhere(['like', 'providerId', $this->providerId])
//            ->andFilterWhere(['like', 'typeId', $this->typeId])
            ->andFilterWhere(['like', 'priceChild', $this->priceChild])
            ->andFilterWhere(['like', 'priceInfant', $this->priceInfant])
            ->andFilterWhere(['in', 't.status', $this->status])
            ->andFilterWhere(['in', 'p.userId', $this->supplier])
            ->andFilterWhere(['in', 'typeId', $this->type]);


        return $dataProvider;

    }
}
