<?php

namespace app\models;

use dektrium\user\models\User as UserDektrium;
use yii\helpers\ArrayHelper;

/**
 * User ActiveRecord model.
 *
 * @property bool    $isAdmin
 * @property bool    $isBlocked
 * @property bool    $isConfirmed
 *
 * Database fields:
 * @property integer $id
 * @property string  $username
 * @property string  $firstname
 * @property string  $lastname
 * @property string  $email
 * @property string  $unconfirmed_email
 * @property string  $password_hash
 * @property string  $auth_key
 * @property string  $registration_ip
 * @property integer $confirmed_at
 * @property integer $blocked_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $last_login
 * @property integer $flags
 *
 * Defined relations:
 * @property Account[] $accounts
 * @property Profile   $profile
 *
 * Dependencies:
 * @property-read Finder $finder
 * @property-read Module $module
 * @property-read Mailer $mailer
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class User extends UserDektrium {

//    public static $usernameRegexp = '/^[-a-zA-Z0-9_\.@]+$/';
    /**
     * @var string pattern (avaliable symbols) for first and last name of user
     *
     * Remarks
     * At first time only latin symbol. In future (maybe) we must add other languages symbols
     */
    public static $firstAndLastNameRegexp = '/[\w-]+/';

    public $firstname;

    public $lastname;

    /** @var Profile|null */
    private $_profile;

    const ROLE_USER = 1;
    const ROLE_SUPPLIER = 5;
    const ROLE_ADMIN = 10;

    /** @inheritdoc
     *
     * overriding method rules of class dektrium\user\models\User with first and last name
     */
    public function rules()
    {
        return [
            // username rules
            //'usernameRequired' => ['username', 'required', 'on' => ['register', 'create', 'connect', 'update']],
            //'usernameMatch'    => ['username', 'match', 'pattern' => static::$usernameRegexp],
            'usernameLength'   => ['username', 'string', 'min' => 3, 'max' => 255],
           // 'usernameUnique'   => [
            //    'username',
             //   'unique',
             //   'message' => \Yii::t('user', 'This username has already been taken')
            //],
            'usernameTrim'     => ['username', 'trim'],

            // first name rules
  //          'firstnameTrim'     => ['firstname', 'filter', 'filter' => 'trim'],
//            'firstnameRequired' => ['firstname', 'required'],

            'firstnameLength'   => ['firstname', 'string', 'max' => 50],

            // first name rules
  //          'lastnameTrim'     => ['lastname', 'filter', 'filter' => 'trim'],
//            'lastnameRequired' => ['lastname', 'required'],

            'lastnameLength'   => ['lastname', 'string', 'max' => 75],

            // email rules
            'emailRequired' => ['email', 'required', 'on' => ['register', 'connect', 'create', 'update']],
            'emailPattern'  => ['email', 'email'],
            'emailLength'   => ['email', 'string', 'max' => 255],
            'emailUnique'   => [
                'email',
                'unique',
                'message' => \Yii::t('user', 'This email address has already been taken')
            ],
            'emailTrim'     => ['email', 'trim'],

            // password rules
            'passwordRequired' => ['password', 'required', 'on' => ['register']],
            'passwordLength'   => ['password', 'string', 'min' => 6, 'max' => 72, 'on' => ['register', 'create']],
        ];
    }

    /** @inheritdoc
     * method afterSave - call after saving the data in dektrium\user\models\User for save data in Profile
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            if ($this->_profile == null) {
                Profile::saveNewProfile($this);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /** @inheritdoc */
   /* public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            if ($this->_profile == null) {
                $this->_profile = \Yii::createObject(Profile::className());
            }
//            $this->_profile->link('user', $this);
        }
    }*/


}
