<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 14.07.2017
 * Time: 16:49
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

class TempAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];

}