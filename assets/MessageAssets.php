<?php
/**
 * Created by PhpStorm.
 * User: VisioN
 * Date: 22.05.2015
 * Time: 14:05
 */

namespace vision\messages\assets;


class MessageAssets extends BaseMessageAssets {

    public $js = [
        'js/chat/vision_messages.js',
    ];

    public $depends = [
        'yii\web\PrivateMessPoolingAsset',
        'yii\web\JqueryAsset'
    ];

} 