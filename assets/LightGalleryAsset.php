<?php

namespace app\assets;

use yii\web\AssetBundle;

class LightGalleryAsset extends AssetBundle
{
    public $sourcePath = '@bower/lightgallery';
    public $css = [
        'src/css/lightgallery.css',
    ];
    public $js = [
        'src/js/lightgallery.js',
        'modules/lg-thumbnail.js',
        'modules/lg-fullscreen.js',
    ];
}