<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 04.09.2017
 * Time: 15:30
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;
use Yii;

class WithBootstrapAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/bootstrap.js'
    ];
    public $jsOptions = [
        'position' => View::POS_HEAD
    ];
    public $css = [
        'css/bootstrap.css'
        // more plugin CSS here
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'dmstr\web\AdminLteAsset'
    ];


}