<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 04.09.2017
 * Time: 11:56
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;
use app\assets\WithBootstrapAsset;
use Yii;

class AdminLtePluginAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins';
    public $js = [
//        'datatables/dataTables.bootstrap.min.js',
//        'js/bootstrap.js',
        'ckeditor/ckeditor.js',
        // more plugin Js here
    ];
    public $jsOptions = [
        'position' => View::POS_END
    ];
    public $css = [
//        'datatables/dataTables.bootstrap.css',
//        'css/bootstrap.css',
        'ckeditor/content.css',
        // more plugin CSS here
    ];
//    public $depends = [
//        'yii\bootstrap\BootstrapAsset',
//        'yii\bootstrap\BootstrapPluginAsset',
//        'dmstr\web\AdminLteAsset'
//    ];


}