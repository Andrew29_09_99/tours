<?php

namespace app\assets;

use yii\web\AssetBundle;

class TourComponentAsset extends AssetBundle
{
    public $css = [
        'css/components/c-tour.css',
    ];
    public $js = [
        'js/components/c-tour.js',
    ];
}