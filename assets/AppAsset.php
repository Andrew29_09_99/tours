<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/main/base.css',
        'css/main/header.css',
        'css/main/footer.css',
        'css/main/profileMenu.css',
        'css/main/customSelect2.css',
    ];
    public $js = [
        'js/main/header.js',
        'js/main/profileMenu.js',
        // allows Stripe to detect anomalous behavior that may be indicative of fraud
        'https://js.stripe.com/v3/'
    ];
    public $jsOptions = array(
        'position' => View::POS_HEAD
    );
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
