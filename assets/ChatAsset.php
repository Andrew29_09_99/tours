<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 01.08.2017
 * Time: 15:05
 */

namespace app\assets;

use yii\web\AssetBundle;
//use vision\messages\assets\CloadAsset;


class ChatAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/profileMessages.css'
    ];

    public $js = [
        'js/chat/jquery.tinyscrollbar.min.js',
        'js/chat/private_mess_cload.js',
        'js/chat/private_mess_pooling.js',
        'js/chat/sortElements.js',
        'js/chat/vision_messages.js'
    ];

}