<?php

namespace app\components;

use Yii;

class Email
{
    public static function send($mailTemplate, $mailTo, $params)
    {
        if (!is_array($mailTo)) {
            $email = Yii::$app->mailer->compose($mailTemplate, ['params' => $params['model']])
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo($mailTo);
            switch ($mailTemplate) {
                case 'approvedSupplier':
                    $email->setSubject('You became a supplier')
                        ->send();
                    break;
                case 'addedTour':
                    $email->setSubject('You added a Tour')
                        ->send();
                    break;
                case 'moderatedTour':
                    $email->setSubject('Your Tour was moderated')
                        ->send();
                    break;
                case 'confirmedOrder':
                    $email->setSubject('Your Order was confirmed')
                        ->attach($params['voucher'])
                        ->send();
                    break;
                case 'cancelledOrder':
                    $email->setSubject('Your Order was cancelled')
                        ->send();
                    break;
            }
        } else {
            if ($mailTemplate == 'newOrder') {
                $emails[] = Yii::$app->mailer->compose('newOrderSupplier', ['params' => $params['model']])
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setTo($mailTo[0])
                    ->setSubject('You have a new Order');
                $emails[] = Yii::$app->mailer->compose('newOrderUser', ['params' => $params['model']])
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setTo($mailTo[1])
                    ->setSubject('You made an Order');
                Yii::$app->mailer->sendMultiple($emails);
            }
        }

    }
}