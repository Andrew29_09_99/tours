<?php
/** Вспомагательные функции 
 * @author Rallo Sergey <meta.greenweb@gmail.com>
 * @version 1.0.1 Дата релиза 2017-12-04
 */

namespace app\components;

class Serve 
{
    
    static function text_view($text, $length = 0, $first = true) {
        if ($length != 0) {
            if ($first) $text = mb_substr($text, 0, $length);
            else $text =  mb_substr($text, $length, mb_strlen($text));        
        }
        $text = str_replace("\n", '<br>', $text);
        return $text;
    }
    
    static function _pr($var, $name = '') {
        ?>
        <pre class='pr'><?
        if ($name != '') {
            ?><div style='border: 1px solid #880000'><?=$name?></div><?
        }

        if (!isset($var)) {
            echo 'NOT ISSET';
        } elseif (is_null($var)) {
            echo 'NULL';
        } elseif (is_bool($var)) {
             echo ($var ? 'TRUE' : 'FALSE');
        } elseif (is_string($var) && trim($var) == '') {
            var_dump($var);
        } else {
            print_r($var);
        }    
        ?></pre><?
    }

     

    static function pr_($var, $name = '') {
        self::pr($var, $name);
        exit();
    }

    /** Функция для удобного вывода значений (переменной, массива, объекта...) Используется при дебаге
     * @author Rallo Sergey <meta.greenweb@gmail.com>
     * @version 1.0 Дата релиза 2017-12-04
     * @param type $var Значение
     * @param string $name Название
     * @return void
     */
    static function pr($var, $name = '') {		    
        //_pr($_SERVER['REMOTE_ADDR']);
        $flag_rallo = true;
        $remote_addr_developer = array('77.87.144.50', '176.100.18.24');
        //$flag_rallo = (in_array($_SERVER['REMOTE_ADDR'], $remote_addr_developer) ? true : false);
        if ($flag_rallo) {   
            self::_pr($var, $name);
        }    
    }    
    

}