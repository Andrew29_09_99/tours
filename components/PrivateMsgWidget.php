<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 31.07.2017
 * Time: 15:37
 */

namespace app\components;

use app\models\Messages;
use app\models\Tour;
use vision\messages\widgets\PrivateMessageWidget as BasePrivateMessage;
use yii\helpers\Url;

class PrivateMsgWidget extends BasePrivateMessage {

    public $tourId;
    public $userId;

    public function run(){
        $this->assetJS();
        $this->addJs();
        $this->html = '<div id="' . $this->uniq_id . '" class="main-message-container">';
        $this->html .= '<div class="message-north">';
        $this->html .= $this->getListUsers();
        $this->html .= $this->getBoxMessages();
        $this->html .= '</div></div>';
        return $this->html;
    }

    protected function getListUsers() {
        $users = \Yii::$app->mymessages->getUserTourDialog($this->userId, $this->tourId);

        $html = '<ul class="list_users message-user-list">';

        /* якщо діалог вже містить повідомлення, підтягується наступний блок, з якого буде вибраний ід юзера, кому буде відправлено повідомелення*/
        if($users) {
            foreach ($users as $usr) {
                $html .= '<span class="contact" data-user="' . $usr['id'] . '" data-tour="' . $this->tourId . '">';
                $html .= "</span>";

            }
        }

        /* якщо це новий діалог, то буде підгружений наступний блок, де береться дефолтне значення юзера-отримувача */
        else {
            $html .= '<span class="contact" data-user="' . $this->userId . '" data-tour="' . $this->tourId . '">';
            $html .= "</span>";
        }
        $html .= '</ul>';
        return $html;
    }

    protected function getFormInput() {
//        $html = '<div class="c-profileDialogue__footer">';

//        $html = '<div class="c-profileDialogue__footer message-south"><form action="#" class="c-profileDialogue__footerContent cf message-form" method="POST">';
        $html = '<div class="c-profileDialogue__footer"><form action="#" enctype="multipart/form-data" class="c-profileDialogue__footerContent cf message-form" method="POST">';
        $html .= '<div class="c-profileDialogue__footerLeft">';
        $html .= '<input type="file" id="file" class="c-profileDialogue__linkUpload" multiple>';
        $html .= '<label class="c-profileDialogue__linkLabel" for="file">';
        $html .= '<svg class="c-profileDialogue__iconAttach" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><path d="m14.754,60.041c3.269,2.399 6.476,3.607 9.567,3.607 0.77,0 1.532-0.075 2.287-0.226 5.39-1.072 8.216-5.645 8.233-5.69l15.922-21.676c0.66-0.897 0.467-2.161-0.432-2.821s-2.16-0.466-2.821,0.433l-16.021,21.825c-0.019,0.031-2.06,3.254-5.669,3.972-2.628,0.527-5.548-0.378-8.678-2.677-9.174-6.737-4.389-13.862-3.811-14.656l26.347-35.899c0.012-0.016 1.131-1.557 3.168-1.91 1.865-0.325 4.009,0.392 6.427,2.167 0.942,0.652 3.476,2.885 3.886,5.34 0.2,1.194-0.116,2.337-0.967,3.495l-23.538,32.048c-0.01,0.014-1.062,1.432-2.613,1.689-0.98,0.158-1.99-0.161-3.105-0.979-3.312-2.434-1.593-5.382-1.255-5.89l13.118-17.861c0.659-0.897 0.467-2.161-0.432-2.821-0.899-0.66-2.162-0.468-2.822,0.433l-13.149,17.903c-1.832,2.638-2.77,7.874 2.148,11.488 2.011,1.476 4.078,2.047 6.156,1.708 3.204-0.53 5.061-3.072 5.233-3.321l23.511-32.008c1.512-2.059 2.082-4.271 1.689-6.573-0.791-4.654-5.383-7.844-5.523-7.94-3.286-2.412-6.465-3.37-9.457-2.855-3.715,0.646-5.617,3.328-5.775,3.565l-26.29,35.821c-0.131,0.175-3.201,4.368-2.381,9.863 0.586,3.927 2.957,7.442 7.047,10.446z"/></svg>';
        $html .= '</label></div>';
        $html .= '<div class="c-profileDialogue__footerCenter">';
        $html .= '<textarea class="c-profileDialogue__textarea" name="input_message" placeholder="Write your message..." rows="6"></textarea>';
//        $html .= '<input type="hidden" name="message_id_user" value="">';
        $html .= '<input type="hidden" name="message_id_user" value="">';
        $html .= '<input type="hidden" name="message_id_tour" value="'.$this->tourId.'">';
        $html .= '<div id="image-wrapper" style="position: relative"><img src="" id="icon" style="width: 100px"></div></div>';
        $html .= '<div class="c-profileDialogue__fileName"></div>';
        $html .= '<div class="c-profileDialogue__footerRight">';
        $html .= '<button class="c-profileDialogue__submit" type="submit">' . $this->buttonName;
        $html .= '<svg class="c-profileDialogue__iconSubmit" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 300" ><path d="M232.6,63.7L63.1,152.6c-7,3.6-6.7,9,0.6,11.9l21.6,8.6c7.3,2.9,18.4,1.3,24.6-3.6l94-74.1
			c6.2-4.9,6.8-4.2,1.5,1.6l-74.3,79.6c-5.4,5.7-3.8,12.7,3.6,15.6l2.5,1c7.3,2.8,19.3,7.6,26.6,10.6l24,9.7
			c7.3,3,13.2,5.4,13.3,5.5c0,0,0,0.2,0.1,0.2c0,0,1.7-6.2,3.8-13.8l36.6-134.5C243.6,63.3,239.5,60,232.6,63.7L232.6,63.7z
			 M232.6,63.7"/>
            <path d="M131.8,201.2l-16.6-6.8c-7.3-3-10.8,0.5-7.8,7.8c0,0,15.4,37.2,15,38.5c-0.5,1.3,14.8-22.1,14.8-22.1
			C141.6,212,139.1,204.2,131.8,201.2L131.8,201.2z M131.8,201.2"/></svg>';
        $html .= '</button>';
        $html .= '</div></form></div>';

//        $html .= '</div>';
        return $html;
    }

    protected function getBoxMessages() {
        $title = Tour::find()->select('name')->where(['id' => $this->tourId])->one();
        $html = '';
//        $html .= '<div class="message-container message-thread">';
//        $html .= '<div class="l-profilePage__rightPart">';
//        $html .= '<div class="l-profilePage__head">Messages</div>';
        $html .= '<div class="l-profilePage__block">';
        $html .= '<div class="c-profileDialogue">';
        $html .= '<div class="c-profileDialogue__header cf">';
        $html .= '<div class="c-profileDialogue__leftPart">';
        $html .= '<a href="'. Url::to(['/messages/dialogs']).'" class="c-profileDialogue__linkBack">';
        $html .= '<svg class="c-profileDialogue__iconBack" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.175 477.175">'.
                       '<path d="M145.188,238.575l215.5-215.5c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0l-225.1,225.1c-5.3,5.3-5.3,13.8,0,19.1l225.1,225   c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,0-19.1L145.188,238.575z"/>'.
                  '</svg>';
        $html .= '</a></div>';
        $html .= '<div class="c-profileDialogue__rightPart">';
        $html .= '<a href="'.Url::to(['/tour/view', 'id' => $this->tourId]).'" class="c-profileDialogue__link">';
        $html .= '<div class="c-profileDialogue__headerTextName"> Topic: &nbsp;</div>';
        $html .= '<div class="c-profileDialogue__headerText">'. $title['name'] .'</div>';
        $html .= '</a></div></div>';
        $html .= $this->getFormInput();
        $html .= '<div class="message-container c-profileDialogue__body">';
        $html .= '</div>';
        $html .= '</div></div></div>';
        return $html;
    }


}