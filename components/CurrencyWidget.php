<?php
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
use app\models\Currency;

class CurrencyWidget extends Widget
{
    public function init(){}

    public function run() 
	{
		$tevValuta = (isset($_GET['currency']) && $_GET['currency']>1)?(int) $_GET['currency']:1; 
		return $this->render('currency/view', 
			[
				'current' => Currency::find()->where('id = :tevValuta', [':tevValuta' => $tevValuta])->one(),
				'notcurrent' => Currency::find()->where('id != :tevValuta', [':tevValuta' => $tevValuta])->all(),
			]);
    }
}