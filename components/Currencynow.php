<?php
namespace app\components;

use Yii;
use app\models\Currency;
use yii\web\NotFoundHttpException;


class Currencynow 
{
    public static function index() 
	{
		if(Yii::$app->request->get('currency')!==null)
		{
			if((int) Yii::$app->request->get('currency')>0)
			{
				$currency = (new \yii\db\Query())->select('id,simbol,alias')->from('currency')->where(['id' => (int) $_GET['currency']])->one();
				if($currency)
				{
					return $currency;
				}
				
			}
		}
		else
		{
			$currency = (new \yii\db\Query())->select('id,simbol,alias')->from('currency')->where(['id' =>1])->one();
			if($currency)
			{
				return $currency;
			}
		}
		
		throw new NotFoundHttpException();
		
    }
}