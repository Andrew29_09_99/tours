<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 17.08.2017
 * Time: 14:03
 */

namespace app\components;

use vision\messages\widgets\PrivateMessageWidget as BasePrivateMessage;
use yii\db\Query;
use app\models\Messages;
use yii\db\Expression;
use yii\data\ArrayDataProvider;
use Yii;

class PreviewAllDialogsWidget extends BasePrivateMessage {

    public function run(){
        $users = $this->getListUsers();

        /*$provider = new ArrayDataProvider([
            'allModels' => $users,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
//                'defaultOrder' => [
//                    'created_at' => SORT_DESC,
//                    'title' => SORT_ASC,
//                ]
            ],
        ]);*/


        // приводим параметры в ссылке к ЧПУ
//        $pages->pageSizeParam = false;

        return $users;
    }

    protected function getListUsers() {
        $users = $this->getAllUsersDialog();

//        debug($users);

        $html = '<div class="c-profileMessages">';
        if($users) {
            foreach ($users as $usr) {
                if ($usr['img']) {
                    $usr['img'] = '/uploads/usr/' . $usr['id'] . '/' . $usr['img'];
                } else {
                    $usr['img'] = '/uploads/usr/default.png';
                }
                /* додаємо в розмітку чата ід тура, по якому планується спілкування */
                $html .= '<div class="contact" data-user="' . $usr['id'] . '"';
                if (!empty($usr['tourId'])) {
                    $html .= 'data-tour="' . $usr['tourId'] . '"><a href="/messages/dialogs?ui=' . $usr['id'] . '&ti=' . $usr['tourId'] . '" class="c-profileMessages__message">';
                } else {
                    $html .= '><a href="/messages/dialog?ui=' . $usr['id'] . '" class="c-profileMessages__message">';
                }

                $html .= '<div class="c-profileMessages__wrapper cf">';
                $html .= '<div class="c-profileMessages__photos">';
                $html .= '<img class="c-profileMessages__photo" src="' . $usr['img'] . '"></div>';

                $html .= '<div class="c-profileMessages__contentWrapper">';
                $html .= '<div class="c-profileMessages__content">';
                $html .= '<div class="c-profileMessages__name">';
                $html .= '<span class="user-title c-profileMessages__name">' . $usr['username'] . '</span>';
                $html .= '</div></div></div>';
                $html .= '<div class="c-profileMessages__contentWrapper">';
                $html .= '<div class="c-profileMessages__content">';
                $html .= '<div class="c-profileMessages__topic">';
                $html .= '<div class="c-profileMessages__topicText">' . $usr['theme_topic'] . '</div></div></div></div>';
//            $html .= ' <span id="cnt">';
                if ($usr['cnt_mess']) {
                    $html .= '<div class="c-profileMessages__contentWrapper c-profileMessages__contentWrapper--messages">';
                    $html .= '<div class="c-profileMessages__content">';
                    $html .= '<div class="c-profileMessages__unreadWrapper">';
                    $html .= '<div class="c-profileMessages__unread"> ' . $usr['cnt_mess'] . ' </div>';
                    $html .= '<div class="c-profileMessages__unreadBg"></div>';
                    $html .= '</div></div></div>';
                }

                $html .= "</div></a>";
                $html .= "</div>";
            }
        } else {
            $html .= '<p>You have not ordered a single tour</p>';
        }

        $html .= '</div>';
        return $html;
    }

    public function getAllUsersDialog()
    {
        $user_id = Yii::$app->user->identity->getId();

        $table_name = Messages::tableName();

        $subQuery = (new Query())
            ->select([
                'tourId',
                'cntAllMsg' => new Expression('count(id)'),

                /* наступні 2 рядки юзати для mysql > 5.7 */

//                'ANY_VALUE(whom_id) whom_id',
//                'ANY_VALUE(from_id) from_id',

                /* наступні 2 рядки юзати для mysql < 5.7 і mariaDB */

                'whom_id',
                'from_id',
                'IF(whom_id < from_id, 
                CONCAT(whom_id, \'-\', from_id),
                CONCAT(from_id, \'-\', whom_id)) AS `tagDialog`'
            ])
            ->from($table_name)
            ->where([
                '=', 'whom_id', $user_id
            ])
            ->orWhere([
                '=', 'from_id', $user_id,
            ])
            ->groupBy([
                'tourId', 'tagDialog'
            ]);


        $subQueryCount = (new Query())
            ->select([
                'whom_id',
                'from_id',
                'tourId',
                'cntNotRead' => new Expression('count(id)'),
            ])
            ->from($table_name)
            ->where([
                '=', 'status', 1
            ])
            ->andWhere([
                '=', 'whom_id', $user_id
            ])
            ->groupBy([
                'from_id', 'tourId'
            ]);



        $query = (new Query())
            ->select([
                'usr.id',
                'username' => 'usr.username',
                'cnt_mess' => 'cntmsg.cntNotRead',
                'msg.cntAllMsg',
                'msg.tourId',
                'img' => 'pr.src_avatar',
                'theme_topic' => 'tr.name'
            ])
            ->from(['usr' => 'user'])
//            ->leftJoin(['msg' => $subQuery], '(usr.id = msg.whom_id or usr.id = msg.from_id)')
            ->leftJoin(['msg' => $subQuery], '(usr.id = msg.whom_id or usr.id = msg.from_id)')
            ->leftJoin(['cntmsg' => $subQueryCount], 'usr.id = cntmsg.from_id and msg.tourId = cntmsg.tourId')
            ->join('INNER JOIN',(['pr' => 'profile']), 'usr.id = pr.user_id')
            ->join('LEFT JOIN',(['tr' => 'tour']), 'msg.tourId = tr.id')
            ->where([
                '!=', 'usr.id', $user_id
            ])
            ->andWhere([
                'not', ['msg.cntAllMsg' => null],
            ])
            ->orderBy([
                'cntmsg.cntNotRead' => SORT_DESC,
                'usr.username' => SORT_DESC
            ]);



        return $query->all();

    }

}