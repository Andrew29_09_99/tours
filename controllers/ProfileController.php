<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 26.04.2017
 * Time: 16:14
 */

namespace app\controllers;

use app\components\LayoutParamsTrait;
use app\models\Orders;
use app\models\Ratings;
use app\models\Reviews;
use app\models\TourPhotos;
use Yii;
use yii\filters\AccessControl;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use dektrium\user\controllers\ProfileController as BaseProfileController;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;

class ProfileController extends BaseProfileController
{
    use LayoutParamsTrait;

    public $layout;

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['index'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['show'], 'roles' => ['?', '@']],
                    ['allow' => true, 'actions' => ['bookings'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['reviews'], 'roles' => ['@']],
                ],
            ],
        ];
    }

    /**
     * Page view profile user.
     * Add check permission for edit profile supplier.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionShow($id)
    {
        $this->layout = 'userblock';
        
        $profile = $this->finder->findProfileById($id);
        
        if ($profile === null) {
            throw new NotFoundHttpException();
        }

        if(\Yii::$app->user->identity->getId() == $id) {
//            $this->layout = 'userblock';
            return $this->render('show', [
                'model' => $profile,
            ]);
        }
        elseif (\Yii::$app->user->identity->getId() != $id){
//            $this->layout = 'g_userblock';
            return $this->render('profile', [
                'model' => $profile,
            ]);
        }
    }

    /**
     * Display booked tours current user
     * @return array
     */

    public function actionBookings(){
        $this->layout = 'main';
		
		//noactive=1 - just bookings
		//today = 1 - realised bookings
		//confirmed = 1 comfirmed booking 
		//cancelByMe = 1 canceled by me booking
		//cancelBySup =1 canceled by supplier booking
        $userId = Yii::$app->user->identity->getId();
		$orderStatus = ((int) Yii::$app->request->get('notactive'))?0:1;
		$today = ((int) Yii::$app->request->get('today')>0)?1:0;
		$confirmed = ((int) Yii::$app->request->get('confirmed')>0)?1:0;	
		$cancelByMe = ((int) Yii::$app->request->get('cancelByMe')>0)?1:0;	
		$cancelBySup = ((int) Yii::$app->request->get('cancelBySup')>0)?1:0;	
		//echo $today;
        /*
		$query = (new Query())
            ->select(['t.*', 'o.*', 'tp.tourId', 'tp.src AS promoPhoto', 'tp.isPromo'])
            ->from(['t' => 'tour'])
            ->leftJoin((['tp' => 'tourPhotos']), 'tp.tourId = t.id')
            ->leftJoin((['o' => 'orders']), 'o.tourId = t.id')
            ->orderBy(['o.dateBuy' => SORT_DESC])
            ->where(['o.userId' => $userId, 'o.orderStatus' => 1, 'tp.isPromo' => 1]);
//            ->groupBy(['t.id']);
*/
        if($today)
		{
			//echo date('d/m/Y');
			$query = (new Query())
            ->select(['t.*', 'o.*','tp.tourId', 'tp.src AS promoPhoto', 'tp.isPromo','c.*'])
            ->from(['t' => 'tour'])
            ->leftJoin((['tp' => 'tourPhotos']), 'tp.tourId = t.id')
			->leftJoin((['o' => 'orders']), 'o.tourId = t.id')
			->leftJoin((['c' => 'currency']), 'c.id = t.desireCurrency')
            ->orderBy(['o.dateBuy' => SORT_DESC])
            ->where(['o.userId' => $userId, 'o.orderStatus' => $orderStatus, 'tp.isPromo' => 1])
			->andwhere('o.dateStartTour > :data',['data'=>date('d/m/Y')]);
			//->andwhere('o.dateStartTour > :data',['data'=>'2/12/2018']);
			/*echo '<pre>';
			print_r($query);
			echo '</pre>';
			*/
		}
		elseif($confirmed)
		{
			$query = (new Query())
            ->select(['t.*', 'o.*','tp.tourId', 'tp.src AS promoPhoto', 'tp.isPromo','c.*'])
            ->from(['t' => 'tour'])
            ->leftJoin((['tp' => 'tourPhotos']), 'tp.tourId = t.id')
			->leftJoin((['o' => 'orders']), 'o.tourId = t.id')
			->leftJoin((['c' => 'currency']), 'c.id = t.desireCurrency')
            ->orderBy(['o.dateBuy' => SORT_DESC])
            ->where(['o.userId' => $userId, 'o.orderStatus' => $orderStatus, 'tp.isPromo' => 1])
			->andwhere('o.dateStartTour < :data',['data'=>date('d/m/Y')]);
		}
		elseif($cancelByMe)
		{
			$query ='';
		}
		elseif($cancelBySup)
		{
			$query ='';
		}
		else
		{
		$query = (new Query())
            ->select(['t.*', 'o.*','tp.tourId', 'tp.src AS promoPhoto', 'tp.isPromo','c.*'])
            ->from(['t' => 'tour'])
            ->leftJoin((['tp' => 'tourPhotos']), 'tp.tourId = t.id')
            ->leftJoin((['o' => 'orders']), 'o.tourId = t.id')
			->leftJoin((['c' => 'currency']), 'c.id = t.desireCurrency')
            ->orderBy(['o.dateBuy' => SORT_DESC])
            //->where(['o.userId' => $userId, 'o.orderStatus' => 1, 'tp.isPromo' => 1]);
			->where(['o.userId' => $userId, 'o.orderStatus' => $orderStatus, 'tp.isPromo' => 1]);
		}
		if($query!='')
		{
			$dataProvider = new ActiveDataProvider([
				'query' => $query,
				'sort' => [
					/*'defaultOrder' => [
						'o.dateBuy' => SORT_DESC
					]*/
				],
				'pagination' => [
					'pageSize' => 20,
				],
			]);
		}	
		else
		{
			throw new NotFoundHttpException();
		};

        // make all user's bookings viewed
        Orders::updateAll(['viewedStatus' => 2], ['userId' => $userId]);

        return $this->render('bookings', [
            'dataProvider' => $dataProvider
        ]);
    }

    /*
     * function return data from profile of user and link to avatar
     * param - user id (integer)
     */

//    public function getUser($user_id){
//        if($user_id) {
//            $profile = $this->finder->findProfileById($user_id);
//            $avatar = Profile::getDefaultAvatar($user_id);
//            return array($profile, $avatar);
//        }
//        else{
//            throw new NotFoundHttpException();
//        }
//    }

    /*public function actionSetting(){
        $id = \Yii::$app->user->getId();
        $profile = $this->finder->findProfileById($id);

        if ($profile === null) {
            throw new NotFoundHttpException();
        }

        return $this->render('settings', [
            'profile' => $profile,
        ]);
    }*/

    /**
     *
     */
    public function actionReviews()
    {
        $userId = Yii::$app->user->identity->getId();

        $newReview = new Reviews();
        $reviewPhotos = new TourPhotos();
        $tourRating = new Ratings();

        $reviews = Reviews::findAll(['userId' => $userId]);

        // prepare sql command to choose only orders for tours that have already been finished
        $sql = "SELECT orders.* FROM orders LEFT JOIN tour ON orders.tourId = tour.id 
                WHERE orders.userId = :userId AND orders.orderStatus = 1 AND orders.isReview = 0 AND 
                (UNIX_TIMESTAMP(STR_TO_DATE(orders.dateStartTour, '%e/%c/%Y')) < (UNIX_TIMESTAMP() + tour.duration))";
        $orders = Orders::findBySql($sql, [':userId' => $userId])->all();

        if ($newReview->load(Yii::$app->request->post()) && $tourRating->load(Yii::$app->request->post())) {
            if ($newReview->save() && $tourRating->save()) {
                $images = UploadedFile::getInstances($reviewPhotos, 'images');
                foreach ($images as $image) {
                    $reviewPhoto = new TourPhotos();
                    $reviewPhoto->tourId = $newReview->tourId;
                    $reviewPhoto->userId = $newReview->userId;
                    $reviewPhoto->reviewId = $newReview->id;
                    $imageResource = $reviewPhoto->uploadImage($image);
                    if ($reviewPhoto->save()) {
                        $path = $reviewPhoto->getImageFile($reviewPhoto->tourId);
                        imagejpeg($imageResource, $path);
                    }
                }
                return $this->redirect(['reviews']);
            }
        } else {
            return $this->render('reviews', [
                'newReview' => $newReview,
                'reviewPhotos' => $reviewPhotos,
                'tourRating' => $tourRating,
                'reviews' => $reviews,
                'orders' => $orders,
            ]);
        }
    }
}