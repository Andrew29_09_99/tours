<?php

namespace app\controllers;

use app\components\LayoutParamsTrait;
use app\components\Email;
use app\models\GuideLanguages;
use app\models\PickupPoints;
use app\models\Profile;
use app\models\User;
use Yii;
use app\models\Orders;
use app\models\OrdersSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\base\Model;
use kartik\grid\EditableColumnAction;
use kartik\mpdf\Pdf;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use yii\filters\AccessControl;

/**
 * OrderController implements the CRUD actions for Orders model.
 */
class OrderController extends Controller
{
    use LayoutParamsTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['setstatus'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['setstatus'],
                        'roles' => ['admin', 'destination', 'supplier'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'setstatus' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => Orders::className(),
                'outputValue' => function ($model, $attribute, $key, $index) {
                    if ($model->$attribute == 1) {
                        $voucher = $this->generateVoucher($model->orderId);
                        if(!empty($voucher)){
                            $model->voucher = $voucher;
                            $model->save();
                        }
                        $linkVoucher = 'uploads/voucher/'.$voucher.'.pdf';
                        // Send email to user (template name, mailTo, params for template)
                        Email::send('confirmedOrder', $model->leadEmail, ['model' => $model, 'voucher' => $linkVoucher]);
                        // return html for grid column
                        $value = '<svg class="orders__checked" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 426.667 426.667">
                                    <path d="M213.333,0C95.518,0,0,95.514,0,213.333s95.518,213.333,213.333,213.333
                                        c117.828,0,213.333-95.514,213.333-213.333S331.157,0,213.333,0z M174.199,322.918l-93.935-93.931l31.309-31.309l62.626,62.622
                                        l140.894-140.898l31.309,31.309L174.199,322.918z"></path>
                                  </svg>';
                    } elseif ($model->$attribute == 0) {
                        // Send email to user (template name, mailTo, params for template)
                        Email::send('cancelledOrder', $model->leadEmail, ['model' => $model]);
                        // return html for grid column
                        $value = '<svg class="orders__checked orders__checked--reject" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44 44">
                                    <path d="m22,0c-12.2,0-22,9.8-22,22s9.8,22 22,22 22-9.8 22-22-9.8-22-22-22zm3.2,22.4l7.5,7.5c0.2,0.2 0.3,0.5 0.3,0.7s-0.1,0.5-0.3,0.7l-1.4,1.4c-0.2,0.2-0.5,0.3-0.7,0.3-0.3,0-0.5-0.1-0.7-0.3l-7.5-7.5c-0.2-0.2-0.5-0.2-0.7,0l-7.5,7.5c-0.2,0.2-0.5,0.3-0.7,0.3-0.3,0-0.5-0.1-0.7-0.3l-1.4-1.4c-0.2-0.2-0.3-0.5-0.3-0.7s0.1-0.5 0.3-0.7l7.5-7.5c0.2-0.2 0.2-0.5 0-0.7l-7.5-7.5c-0.2-0.2-0.3-0.5-0.3-0.7s0.1-0.5 0.3-0.7l1.4-1.4c0.2-0.2 0.5-0.3 0.7-0.3s0.5,0.1 0.7,0.3l7.5,7.5c0.2,0.2 0.5,0.2 0.7,0l7.5-7.5c0.2-0.2 0.5-0.3 0.7-0.3 0.3,0 0.5,0.1 0.7,0.3l1.4,1.4c0.2,0.2 0.3,0.5 0.3,0.7s-0.1,0.5-0.3,0.7l-7.5,7.5c-0.2,0.1-0.2,0.5 3.55271e-15,0.7z"/>
                                  </svg>';
                    }
                    return $value;
                },
            ]
        ]);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'Cart' page.
     * @return mixed
     * @throws NotFoundHttpException if creation was failed
     */
    public function actionCreate()
    {
        $newOrder = new Orders(['scenario' => 'scenario_formBookNow']);
        if ($newOrder->load(Yii::$app->request->post())) {
			//print_r($newOrder);
			//echo '<pre>';
			//print_r($_POST);
			//echo '<pre>';
            if (!empty(Yii::$app->user->identity)) {
                $userId = Yii::$app->user->identity->getId();
            } else {
                $session = Yii::$app->session;
                // if session is not open, open session
                if (!$session->isActive) {
                    $session->open();
                }
                $userId = $session->id;
            }
            $newOrder->userId = (string)$userId;
            $newOrder->setCartId();
            //$newOrder->setTotalPrice();метод неправильно считает, ошибка в формировании массива
			$newOrder->setTotalPriceNew();
            //$newOrder->setPayPrice();
			$newOrder->setPayPriceNew();
            $newOrder->save();

            return $this->redirect(['/cart/index', 'userId' => $newOrder->userId]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'Payment' page.
     * @param int $cartId
     * @return mixed
     */
    public function actionUpdate($cartId)
    {
        $orders = Orders::find()
            ->with('tour')
            ->where(['cartId' => $cartId])
            ->orderBy('tourId')
            ->all();
        
        $payPriceSum = array_sum(ArrayHelper::getColumn($orders, 'payPrice'));
        $totalPriceSum = array_sum(ArrayHelper::getColumn($orders, 'totalPrice'));

        $tourIds = ArrayHelper::getColumn($orders, 'tourId');
        $pickupPoints = PickupPoints::find()
            ->where(['tourId' => $tourIds])
            ->orderBy('tourId')
            ->asArray()
            ->all();

        $userData = false;
        $user = Yii::$app->user;
        if(!$user->isGuest){
            $userData = Profile::findOne($user->identity->getId());
        }

        $tourPickupPoints = json_encode(ArrayHelper::index($pickupPoints, null, 'tourId'));

        if (Model::loadMultiple($orders, Yii::$app->request->post()) && Model::validateMultiple($orders)) {
            foreach ($orders as $order) {
                // update sums if it is needed
                //$order->setTotalPrice();
				$order->setTotalPriceNew();
                //$order->setPayPrice();
				$order->setPayPriceNew();
                $order->save(false);
            }
            return $this->redirect(['payment', 'cartId' => $orders[0]->cartId]);
        } else {
            return $this->render('update', [
                'orders' => $orders,
                'payPriceSum' => $payPriceSum,
                'totalPriceSum' => $totalPriceSum,
                'tourPickupPoints' => $tourPickupPoints,
                'userdata' => $userData
            ]);
        }
    }

    /**
     * @param int $cartId
     * @return mixed
     */
    public function actionPayment($cartId)
    {
        $orders = Orders::find()
            ->with('tour', 'guide', 'group')
            ->where(['cartId' => $cartId])
            ->all();
        $payPriceSum = array_sum(ArrayHelper::getColumn($orders, 'payPrice'));
        $totalPriceSum = array_sum(ArrayHelper::getColumn($orders, 'totalPrice'));

        return $this->render('payment', [
            'orders' => $orders,
            'payPriceSum' => $payPriceSum,
            'totalPriceSum' => $totalPriceSum,
        ]);
    }

    /**
     *
     */
    public function actionNewCharge()
    {
        $chargeId = $this->stripeCharge(Yii::$app->request->post());

        if ($chargeId) {
            return $this->redirect(['success',
                'cartId' => Yii::$app->request->post('cartId'),
                'chargeId' => $chargeId
            ]);
        } else {
            return $this->render('failure', [
                'cartId' => Yii::$app->request->post('cartId'),
            ]);
        }
    }

    /**
     * @param array $postData
     * @return mixed
     * @throws NotFoundHttpException if the charge was not created

     */
    private function stripeCharge($postData)
    {
        $stripe = [
            'secret_key' => 'sk_test_MTJxspQZ68rtB7ZagEEqIkRw',
            'publishable_key' => 'pk_test_CuqYghHFpqBvjpQKpMv33h36'
        ];
        Stripe::setApiKey($stripe['secret_key']);

        $customer = Customer::create([
            'email' => $postData['email'],
            'card'  => $postData['stripeToken']
        ]);

        $charge = Charge::create([
            'customer' => $customer->id,
            'amount'   => $postData['amount'],
            'currency' => 'eur',
            'description' => 'Payment for Tour(s) on TripsPoint'
        ]);

        if (empty($charge->failure_code)) {
            return $charge->id;
        } else {
            return false;
        }
    }

    /**
     *
     */
    public function actionSuccess($cartId, $chargeId)
    {
        $orders = Orders::find()
            ->where(['cartId' => $cartId])
            ->all();
        foreach ($orders as $order) {
            $order->payStatus = 1;
            $order->chargeId = $chargeId;
            // define a current timestamp
            $order->dateBuy = time();
            $order->save();
            // Send emails to supplier and user (template name, mailTo(first-supplier, second-user), params for template)
            Email::send('newOrder', [$order->provider->emailProvider, $order->leadEmail], ['model' => $order]);
        }

        return $this->render('success');
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $order = $this->findModel($id);
        $order->delete();

        return $this->redirect(['/cart/index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     *  Create voucher
     *  @param integer $oid (orderId)
     *  @return string $pdf->filename (link for file-voucher)
     */
    private function generateVoucher($oid) {
        // get your HTML raw content without any layouts or scripts
        $orderData = $this->getOrderData($oid);
        $content = $this->renderPartial('_voucher', [
            'order' => $orderData
        ]);

        // generate of name file-voucher
        $str='1234567890ABCDEF';
        $code_length=12;
        $code_separartor=6;
        $str_length=strlen($str)-1;

         $code='';
         for ($i=0; $i<$code_length; $i++){
                if ($i>0 && $code_separartor>0 && $i%$code_separartor==0) { $code.='-';}
                $code.=substr($str, mt_rand(0,$str_length), 1);
         }

        $voucherFilename = $code;
        $path_to_file = 'uploads/voucher/';

        /* check if isset directory for voucher */
        if(!is_dir($path_to_file)){
            FileHelper::createDirectory($path_to_file, 0755);
        }

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
//            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
//            \'destination\' => Pdf::DEST_BROWSER,
//            \'tempPath\' => \'uploads/voucher\',
            'filename' => 'uploads/voucher/'.$voucherFilename.'.pdf',
            /* return in file*/
            'destination' => Pdf::DEST_FILE,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
//            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssFile' => 'css/voucher.css',
            // any css to be embedded if required
//            'cssInline' => '.heading: {border-bottom:none}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Name Voucher'],
            // call mPDF methods on the fly
            'methods' => [
//                'SetHeader'=>['Tripspoint'],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        $pdf->render();
//        return $pdf->filename;
        return $voucherFilename;
    }

    protected function getOrderData($oid){

        if($oid){
            $order = Orders::find()->joinWith(['tour','user','provider'])->join('left join', 'pickupPoints', 'pickupPoints.tourId = `orders`.`tourId`')->where(['orderId' => $oid])->one();

//            debug($order['tour']['addInfo']);
//            die;

            return $order;
        }
        return false;
    }
}
