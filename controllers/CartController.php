<?php

namespace app\controllers;

use app\components\LayoutParamsTrait;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\models\Orders;
use Yii;
use yii\web\NotFoundHttpException;

class CartController extends Controller
{
    use LayoutParamsTrait;

    public function actionIndex()
    {
        if (!empty(Yii::$app->user->identity)) {
            $userId = Yii::$app->user->identity->getId();
        } else {
            $session = Yii::$app->session;
            // if session is not open, open session
            if (!$session->isActive) {
                $session->open();
            }
            $userId = $session->id;
        }

        $orders = Orders::find()
            ->where(['userId' => $userId, 'payStatus' => '0'])
            ->orderBy('orderId')
            ->all();

        $depositSum = array_sum(ArrayHelper::getColumn($orders, 'payPrice'));
        $totalSum = array_sum(ArrayHelper::getColumn($orders, 'totalPrice'));

        return $this->render('index', [
            'orders' => $orders,
            'depositSum' => $depositSum,
            'totalSum' => $totalSum,
        ]);
    }
}
