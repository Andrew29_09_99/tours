<?php   

namespace app\controllers;  

use app\components\LayoutParamsTrait;
use app\components\Email;
use app\models\Cities;
use app\models\Countries;
use app\models\Groups;
use app\models\Orders;
use app\models\PickupPoints;
use app\models\Reviews;
use app\models\TourLocation;
use app\models\TourPhotos;
use app\models\TourDates;
use app\models\GuideLanguages;
use app\models\Tour;
use app\models\TourSearch;
use app\models\TourTypes;
use app\models\Currency;
use Yii;
use yii\web\Controller;
use yii\web\Cookie;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
/**
 * TourController implements the CRUD actions for Tour model.
 */
class TourController extends Controller
{
    use LayoutParamsTrait;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['admin', 'destination', 'supplier'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
			\app\behaviors\Getcurrencynow::className(),
        ];
    }
    /**
     * Lists all Tour models.
     * @return mixed
     */
    public function actionIndex()
    {
        $tourSearch = new TourSearch();
        $dataProvider = $tourSearch->search(Yii::$app->request->queryParams);
        $typesList = TourTypes::getTypesList(true);
        $continentsList = Countries::getContinentsList();
		$currencynow = $this->whatCurrencyNow();
        return $this->render('index', [
            'tourSearch' => $tourSearch,
            'dataProvider' => $dataProvider,
            'typesList' => $typesList,
            'continentsList' => $continentsList,
			'currencynow'=>$currencynow,
        ]);
    }
    /**
     * Get locations list based on the parent location value (for dependent dropdown) for filtering tours
     */
    public function actionChildLocation()
    {
        $parentLocation = Yii::$app->request->post('depdrop_all_params');
        $childLocation = [];
        $outputData = [];
        if (!empty($parentLocation)) {
            if (!empty($parentLocation['toursearch-continent'])) {
                $childLocation = Countries::find()
                    ->joinWith('tourLocations', false, 'RIGHT JOIN')
                    ->where(['countries.continent' => $parentLocation['toursearch-continent']])
                    ->all();
            } elseif (!empty($parentLocation['toursearch-country'])) {
                $childLocation = Cities::find()
                    ->joinWith('tourLocations', false, 'RIGHT JOIN')
                    ->where(['cities.countryId' => $parentLocation['toursearch-country']])
                    ->all();
            }
            if (!empty($childLocation)) {
                foreach ($childLocation as $key => $location) {
                    // Define selected values for the initializing dependent dropdowns after the page was reloaded
                    if ($parentLocation['countryValue'] == $location->id || $parentLocation['cityValue'] == $location->id) {
                        $selected = ['id' => $location->id, 'name' => $location->name];
                    }
                    $outputData[] = ['id' => $location->id, 'name' => $location->name];
                }
                echo json_encode(['output' => $outputData, 'selected' => $selected]);
            }
        } else {
            echo json_encode(['output' => '', 'selected' => '']);
        }
    }
    /**
     * Get dates for the exact guide (for datepicker)
     */
    public function actionGetGuideDates()
    {
        $postData = Yii::$app->request->post();
        if (!empty($postData['tourId']) && !empty($postData['languageId'])) {
            $tour = $this->findModel($postData['tourId']);
            $guideDates = $tour->getGuideDates($postData['languageId']);
            echo $guideDates;
        } else {
            echo json_encode('');
        }
    }
    /**
     * Displays a single Tour model and form for Order creation
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $tour = Tour::findOne(['id' => $id, 'status' => 1]);
		/*echo '<pre>';
		print_r($tour);
		echo '</pre>';
		echo $tour->priceAdult;
		echo $tour->minimalPrice.' ';echo $tour->NetminimalPrice;die;
		*/
        if (!empty($tour)) {
            $guidesArray = [];
            foreach ($tour->guides as $guide) {
                array_push($guidesArray, $guide->language);
            }
            $tourGuides = implode(', ', $guidesArray);
            $order = new Orders(['scenario' => 'scenario_formBookNow']);
            $pickupPoints = PickupPoints::find()
                ->where(['tourId' => $id])
                ->asArray()
                ->all();
            $tourReviews = new Reviews();
            $dataProvider = $tourReviews->search($id, Yii::$app->request->queryParams);
			$currencynow = $this->whatCurrencyNow();
			$currencydesire = '';
			$currencydesire = Currency::find()->where(['id'=>$tour->desireCurrency])->one();
			$output = array();
			$output=array(
				'deposit'=>($tour->minimalPrice && $tour->NetminimalPrice && ($tour->minimalPrice > $tour->NetminimalPrice))?$tour->minimalPrice - $tour->
NetminimalPrice:'',
				'balance'=> ($tour->NetminimalPrice)?$tour->NetminimalPrice:'',
				'total'=> ($tour->minimalPrice)?$tour->minimalPrice:'',
				'depositnow'=>($tour->NetminimalPrice)?$tour->NetminimalPrice:'',
				'childDeposit'=>($tour->priceChild && $tour->NetpriceChild && ($tour->priceChild > $tour->NetpriceChild))?$tour->priceChild - $tour->
NetpriceChild:'',
				'infantDeposit'=>($tour->priceInfant && $tour->NetpriceInfant && ($tour->priceInfant > $tour->NetpriceInfant))?$tour->priceInfant - $tour->
NetpriceInfant:'',
				'seniorDeposit'=>($tour->priceSenior && $tour->NetpriceSenior && ($tour->priceSenior > $tour->NetpriceSenior))?$tour->priceSenior - $tour->
NetpriceSenior:'',
				'childPrice'=>($tour->priceChild)?$tour->priceChild:'',
				'infantPrice'=>($tour->priceInfant)?$tour->priceInfant:'',
				'seniorPrice'=>($tour->priceSenior)?$tour->priceSenior:'',
			);	
			if($tour->desireCurrency!=$currencynow->id)
			{
				//need to call converter and for valuta new 
				$output = array();
				//$currencydesire = Currency::find()->where(['id'=>$tour->desireCurrency])->one();
				$output=array(
					'deposit'=>$this->converter($tour->minimalPrice - $tour->NetminimalPrice, $currencydesire->alias, $currencynow->alias),
					'balance'=>$this->converter($tour->NetminimalPrice, $currencydesire->alias, $currencynow->alias),
					'total'=>$this->converter($tour->minimalPrice, $currencydesire->alias, $currencynow->alias),
					'depositnow'=>$this->converter($tour->NetminimalPrice, $currencydesire->alias, $currencynow->alias),
					'childDeposit'=>($tour->priceChild && $tour->NetpriceChild && ($tour->priceChild > $tour->NetpriceChild))?$this->converter($tour->
priceChild - $tour->NetpriceChild, $currencydesire->alias, $currencynow->alias):'',
					'infantDeposit'=>($tour->priceInfant && $tour->NetpriceInfant && ($tour->priceInfant > $tour->NetpriceInfant))?$this->converter($tour->
priceInfant - $tour->NetpriceInfant, $currencydesire->alias, $currencynow->alias):'',
					'seniorDeposit'=>($tour->priceSenior && $tour->NetpriceSenior && ($tour->priceSenior > $tour->NetpriceSenior))?$this->converter($tour->
priceSenior - $tour->NetpriceSenior, $currencydesire->alias, $currencynow->alias):'',
					'childPrice'=>($tour->priceChild)?$this->converter($tour->priceChild, $currencydesire->alias, $currencynow->alias):'',
					'infantPrice'=>($tour->priceInfant)?$this->converter($tour->priceInfant, $currencydesire->alias, $currencynow->alias):'',
					'seniorPrice'=>($tour->priceSenior)?$this->converter($tour->priceSenior, $currencydesire->alias, $currencynow->alias):'',
				);
			}
            // Add id of the recently viewed tour to the cookies
            if (Yii::$app->request->cookies->has('tourIds')) {
                $tourIdsString = Yii::$app->request->cookies->getValue('tourIds');
                if (strpos($tourIdsString, $id) === false) {
                    $tourIdsArray = explode(',', $tourIdsString);
                    if (count($tourIdsArray) > 50) {
                        array_shift($tourIdsArray);
                        $tourIdsString = implode(',', $tourIdsArray);
                    }
                    Yii::$app->response->cookies->add(new Cookie([
                        'name' => 'tourIds',
                        'value' => $tourIdsString.','.$id,
                    ]));
                }
            } else {
                Yii::$app->response->cookies->add(new Cookie([
                    'name' => 'tourIds',
                    'value' => $id,
                ]));
            }
            return $this->render('view', [
                'tour' => $tour,
                'tourGuides' => $tourGuides,
                'order' => $order,
                'tourPickupPoints' => json_encode($pickupPoints),
                'dataProvider' => $dataProvider,
				'currencynow'=>$currencynow,
				'currencydesire'=>$currencydesire,
				'output'=>$output,
            ]);
        } else {
            throw new NotFoundHttpException();
        }
    }
	public function converter($amount,$from,$to)
	{
		if($amount > 0 && $from && $to)
		{
			$data = file_get_contents("https://finance.google.com/finance/converter?a=$amount&from=$from&to=$to");
			if($data)
			{
				preg_match("/<span class=bld>(.*)<\/span>/",$data, $converted);
				$converted = preg_replace("/[^0-9.]/", "", $converted[1]);
				$convert=$converted + $converted*0.02;
				return number_format(round($convert, 3),2);
			}
			else {return ;}
		}
		else {return ;}
	}
    /**
     * Displays 50 recently viewed Tours
     * @return mixed
     */
    public function actionRecentlyViewed()
    {
        // Get tours ids from the cookies
        $currencynow = $this->whatCurrencyNow();
		if (Yii::$app->request->cookies->has('tourIds')) {
            $error = false;
            $tourIds = explode(',', Yii::$app->request->cookies['tourIds']->value);
            $tours = Tour::findAll(['id' => $tourIds, 'status' => 1]);
        } else {
            $error = true;
            $tours = false;
        }
        return $this->render('recentlyViewed', [
            'error' => $error,
            'tours' => $tours,
			'currencynow'=>$currencynow,
        ]);
    }
    /**
     * Creates a new Tour model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$tour = new Tour();
        $newCountry = new Countries();
        $newCity = new Cities();
        $tourLocation = new TourLocation();
        $tourPhotos = new TourPhotos();
        $tourDates = new TourDates();
        $groups = new Groups();
        $pickupPoints = new PickupPoints();
		$currencynow = $this->whatCurrencyNow();
        $languagesList = GuideLanguages::getLanguagesList();
        $typesList = TourTypes::getTypesList();
		$currency = Currency::find()->all();
        if ($tour->load(Yii::$app->request->post()) &&
            $newCountry->load(Yii::$app->request->post()) &&
            $newCity->load(Yii::$app->request->post()) &&
            $tourDates->load(Yii::$app->request->post()) &&
            $tourPhotos->load(Yii::$app->request->post()) &&
            $pickupPoints->load(Yii::$app->request->post()) &&
            $groups->load(Yii::$app->request->post()))
		{
			//echo '<pre>';
			//print_r($_POST);
			//echo '</pre>';
			$country = Countries::findOne(['name' => $newCountry->name]);
            if (!$country) {
                $newCountry->setContinent();
                $newCountry->save();
                $country = $newCountry;
            }
            $city = Cities::findOne(['name' => $newCity->name, 'countryId' => $newCountry->id]);
            if (!$city) {
                $newCity->countryId = $newCountry->id;
                $newCity->save();
                $city = $newCity;
            }
            $tour->providerId = Yii::$app->user->identity->getId();
            $tour->descExtra = json_encode($tour->descExtra);
            $tour->inclusion = json_encode($tour->inclusion);
            $tour->exclusion = json_encode($tour->exclusion);
			$tour->AgeTypePrice = json_encode($tour->AgeTypePrice);
			$tour->hotelPickup = json_encode($tour->hotelPickup);
//            $itinerary = [];
//            foreach ($tour->itinerary as $key => $values) {
//            	$itinerary[$values['itineraryTime'].';'.$values['itineraryTimeEnd']] = $values['itineraryDesc'];
//            }
            //$tour->itinerary = json_encode($tour->itinerary);
            $tour->duration = Tour::wordsToSeconds($tour->duration);
            $tour->payMethod = json_encode($tour->payMethod);
            $tour->voucherType = json_encode($tour->voucherType);
            if ($tour->save()) {
                $tourLocation->tourId = $tour->id;
                $tourLocation->countryId = $country->id;
                $tourLocation->cityId = $city->id;
                $tourLocation->save();
                foreach ($tourDates->allDates as $key => $values) {
                    $tourDates = new TourDates();
                    $tourDates->tourId = $tour->id;
                    $tourDates->attributes = $values;
                    $tourDates->save();
                }
                $tourPhotos->tourId = $tour->id;
                $tourPhotos->userId = Yii::$app->user->identity->getId();
                $promoImage = $tourPhotos->uploadPromoImage($tourPhotos->promoImage);
                if ($tourPhotos->save()) {
                    $path = $tourPhotos->getImageFile($tour->id);
                    file_put_contents($path, $promoImage);
                }
                $images = FileHelper::findFiles(Yii::$app->basePath . '/web/uploads/temp/' . Yii::$app->user->identity->getId());
                if (!empty($images)) {
                    foreach ($images as $image) {
                        $tourPhoto = new TourPhotos();
                        $tourPhoto->tourId = $tour->id;
                        $tourPhoto->userId = Yii::$app->user->identity->getId();
                        $tourPhoto->src = basename($image);
                        if ($tourPhoto->save()) {
                            // move image from temporary directory
                            $path = $tourPhoto->getImageFile($tour->id);
                            rename($image, $path);
                        }
                    }
                }
                $allPoints = json_decode($pickupPoints->allPoints, true);
				if($allPoints)
				{
					foreach ($allPoints as $point)
					{
						$pickupPoint = new PickupPoints();
						$pickupPoint->tourId = $tour->id;
						$pickupPoint->attributes = $point;
						$pickupPoint->save();
					}
				}
                if (!empty($groups->newGroups)) {
                    foreach ($groups->newGroups as $key => $values) {
                        $group = new Groups();
                        $group->tourId = $tour->id;
                        $group->attributes = $values;
                        $group->save();
                    }
                }
                // Send email to supplier (template name, mailTo, params for template)
                Email::send('addedTour', $tour->provider->emailProvider, ['model' => $tour]);
                return $this->redirect(['/supplier/added-tours','currency'=>$currencynow['id']]);
            } else {
                \app\components\Serve::pr($tour->errors);
            }
        }
		else
		{
            // remove images from temporary directory
            FileHelper::removeDirectory(Yii::$app->basePath . '/web/uploads/temp/' . Yii::$app->user->identity->getId());
            return $this->render('create', [
                'tour' => $tour,
                'country' => $newCountry,
                'city' => $newCity,
                'tourPhotos' => $tourPhotos,
                'tourDates' => $tourDates,
                'languagesList' => $languagesList,
                'typesList' => $typesList,
                'groups' => $groups,
                'pickupPoints' => $pickupPoints,
				'currency'=>$currency,
				'currencynow'=>$currencynow,
            ]);
        }
    }
    /**
     * !!! NEEDS REFACTORING AND REWRITING CODE !!!
     * Updates an existing Tour model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the user hasn't rights
     */
    public function actionUpdate($id)
    {
        $tour = $this->findModel($id);
        $userId = Yii::$app->user->identity->getId();
		$currency = Currency::find()->all();
		$currencynow = $this->whatCurrencyNow();
        if ($userId == $tour->providerId || Yii::$app->authManager->getRolesByUser($userId)['admin']) {
            $tour->duration = $tour->secondsToValuesArray($tour->duration);
            $tour->descExtra = json_decode($tour->descExtra);
            $tour->inclusion = json_decode($tour->inclusion);
			$tour->AgeTypePrice = json_decode($tour->AgeTypePrice);
            $tour->exclusion = json_decode($tour->exclusion);
			$tour->hotelPickup = json_decode($tour->hotelPickup);
//            $itinerary = [];
//            foreach (json_decode($tour->itinerary) as $key => $value) {
//				$masDate=[];$itinerTimeStart='';$itinerTimeEnd='';
//				if($key)
//				{
//					$masDate = explode(';',$key);
//					$itinerTimeStart =(isset($masDate[0]))?$masDate[0]:''; 
//					$itinerTimeEnd =(isset($masDate[1]))?$masDate[1]:''; 
//				}
//                $itinerary[] = [
//                    'itineraryTime' => $itinerTimeStart,
//					'itineraryTimeEnd' => $itinerTimeEnd,
//                    'itineraryDesc' => $value,
//                ];
//            }
//            $tour->itinerary = $itinerary;
            $tour->payMethod = json_decode($tour->payMethod);
			$tour->voucherType = json_decode($tour->voucherType);
            $tourLocation = $tour->tourLocations;
            $country = $tour->countries;
            $city = $tour->cities;
            $tourPhotos = new TourPhotos();
            $existingPhotos = [];
            foreach ($tour->allPhotos as $photo) {
                $existingPhotos[] = Yii::$app->request->baseUrl.'/uploads/photos/'.$photo->tourId.'/'.$photo->src;
            }
            $tourDates = new TourDates();
            $tourDates->allDates = $tour->tourDates;
            $groups = new Groups();
            $groups->newGroups = $tour->groups;
            $pickupPoints = new PickupPoints();
           
		   $allPoints = [];
            foreach ($tour->pickupPoints as $pickupPoint) {
                $allPoints[] = $pickupPoint->attributes;
            }
            $pickupPoints->allPoints = json_encode($allPoints);
            $typesList = TourTypes::getTypesList();
            $languagesList = GuideLanguages::getLanguagesList();
            // echo '<pre>';
			//	print_r($_POST);
			//echo '</pre>';
			if ($tour->load(Yii::$app->request->post()) &&
                $country->load(Yii::$app->request->post()) &&
                $city->load(Yii::$app->request->post()) &&
                $tourDates->load(Yii::$app->request->post()) &&
                $tourPhotos->load(Yii::$app->request->post()) &&
                $pickupPoints->load(Yii::$app->request->post())
				//&& $groups->load(Yii::$app->request->post())
				) 
			{
				$existingCountry = Countries::findOne(['name' => $country->name]);
                if (!$existingCountry) {
                    $country->setContinent();
                    $country->save();
                }
                $exisitngCity = Cities::findOne(['name' => $city->name, 'countryId' => $city->id]);
                if (!$exisitngCity) {
                    $city->countryId = $country->id;
                    $city->save();
                }
                $tour->providerId = Yii::$app->user->identity->getId();
                $tour->descExtra = json_encode($tour->descExtra);
                $tour->inclusion = json_encode($tour->inclusion);
                $tour->exclusion = json_encode($tour->exclusion);
				$tour->AgeTypePrice = json_encode($tour->AgeTypePrice);
				$tour->hotelPickup = json_encode($tour->hotelPickup);
				/*$itinerary = [];
                foreach ($tour->itinerary as $key => $values) {
                    $itinerary[$values['itineraryTime'].';'.$values['itineraryTimeEnd']] = $values['itineraryDesc'];
                }
                $tour->itinerary = json_encode($itinerary);
				*/
                $tour->duration = Tour::wordsToSeconds($tour->duration);
                $tour->payMethod = json_encode($tour->payMethod);
				$tour->voucherType = json_encode($tour->voucherType);
                $tour->status = 0;
				$tour->itsclone = 0;
                if ($tour->save()) {
                    $tourLocation->tourId = $tour->id;
                    $tourLocation->countryId = $country->id;
                    $tourLocation->cityId = $city->id;
                    $tourLocation->save();
                    foreach ($tour->tourDates as $tourDate) {
                        $tourDate->delete();
                    }
                    foreach ($tourDates->allDates as $key => $values) {
                        $tourDates->tourId = $tour->id;
                        $tourDates->attributes = $values;
                        $tourDates->save();
                    }
                    if (!empty($tourPhotos->promoImage)) 
					{
                        if(Yii::$app->request->get('cloneRec'))
						{
							Yii::$app->db->createCommand("DELETE FROM TourPhotos WHERE tourId = ".$id." AND isPromo = 1")->execute();
						}
						else
						{
							$tour->promoPhoto->delete();
						}
                        $tourPhotos->tourId = $tour->id;
                        $tourPhotos->userId = Yii::$app->user->identity->getId();
                        $promoImage = $tourPhotos->uploadPromoImage($tourPhotos->promoImage);
                        if ($tourPhotos->save()) {
                            $path = $tourPhotos->getImageFile($tour->id);
                            file_put_contents($path, $promoImage);
                        }
                    }
					if(is_dir(Yii::$app->basePath . '/web/uploads/temp/'. Yii::$app->user->identity->getId()))
					{
						$images = FileHelper::findFiles(Yii::$app->basePath . '/web/uploads/temp/' . Yii::$app->user->identity->getId());
						if (!empty($images)) {
							if(Yii::$app->request->get('cloneRec'))
							{	
								Yii::$app->db->createCommand("DELETE FROM TourPhotos WHERE tourId = ".$id." AND isPromo = 0")->execute();
							}
							foreach ($images as $image) {
								$tourPhoto = new TourPhotos();
								$tourPhoto->tourId = $tour->id;
								$tourPhoto->userId = Yii::$app->user->identity->getId();
								$tourPhoto->src = basename($image);
								if ($tourPhoto->save()) {
									// move image from temporary directory
									$path = $tourPhoto->getImageFile($tour->id);
									rename($image, $path);
								}
							}
						}
					}
					//$allPoints = json_decode($pickupPoints->allPoints, true);
					if(!empty($pickupPoints->allPoints ))
					{
						foreach ($tour->pickupPoints as $pickupPoint) {
							$pickupPoint->delete();
						}
						$allPoints = json_decode($pickupPoints->allPoints, true);
						foreach ($allPoints as $point) {
							$pickupPoint = new PickupPoints();
							$pickupPoint->tourId = $tour->id;
							$pickupPoint->attributes = $point;
							$pickupPoint->save();
						}
					}
                    if (!empty($tour->groups)) {
                        foreach ($tour->groups as $group) {
                            $group->delete();
                        }
                    }
                    if (!empty($tour->groups)) {
                        foreach ($groups->newGroups as $key => $values) {
                            $group = new Groups();
                            $group->tourId = $tour->id;
                            $group->attributes = $values;
                            $group->save();
                        }
                    }
                    return $this->redirect(['/supplier/added-tours','currency'=>$currencynow['id']]);
                }
				else 
				{
                    \app\components\Serve::pr($tour->errors);
                }
            } 
			else
			{
                // remove images from temporary directory
                FileHelper::removeDirectory(Yii::$app->basePath . '/web/uploads/temp/' . Yii::$app->user->identity->getId());
                return $this->render('update', [
                    'tour' => $tour,
                    'country' => $country,
                    'city' => $city,
                    'tourPhotos' => $tourPhotos,
                    'existingPhotos' => $existingPhotos,
                    'tourDates' => $tourDates,
                    'groups' => $groups,
                    'pickupPoints' => $pickupPoints,
                    'typesList' => $typesList,
                    'languagesList' => $languagesList,
					'currencynow'=>$currencynow,
					'currency'=>$currency,
                ]);
            }
        } else {
            throw new NotFoundHttpException();
        }
    }
    // clone record
	public function actionClone($id)
    {
        $tour = $this->findModel($id);
		$currencynow = $this->whatCurrencyNow();
        $tour->id = null;
		$tour->status = 0;
        $tour->setIsNewRecord(true);
        if($tour->insert(false))
		{
			$lastId = $tour->id;
			$groupsFind = Groups::find()->where(['tourId' => $id])->all();
			if($groupsFind)
			{
				foreach($groupsFind as $key)
				{
					$key->id=null; // PK
					$key->tourId= $lastId;
					$key->setIsNewRecord(true);
					$key->insert(false);
				}
			}
			//die;
			$TourDatesFind = TourDates::find()->where(['tourId' => $id])->all();
			if($TourDatesFind)
			{
				foreach($TourDatesFind as $key)
				{
					$key->id=null; // PK
					$key->tourId= $lastId;
					$key->setIsNewRecord(true);
					$key->insert(false);
				}
			}
			$TourLocationFind = TourLocation::find()->where(['tourId' => $id])->all();
			if($TourLocationFind)
			{
				foreach($TourLocationFind as $key)
				{
					$key->id=null; // PK
					$key->tourId= $lastId;
					$key->setIsNewRecord(true);
					$key->insert(false);
				}
			}
			$PickupPointsFind = PickupPoints::find()->where(['tourId' => $id])->all();
			if($PickupPointsFind)
			{
				foreach($PickupPointsFind as $key)
				{
					$key->id=null; // PK
					$key->tourId= $lastId;
					$key->setIsNewRecord(true);
					$key->insert(false);
				}
			}
			$TourPhotosFind = TourPhotos::find()->where(['tourId' => $id])->all();
			if($TourPhotosFind)
			{
				FileHelper::createDirectory(Yii::$app->basePath . '/web/uploads/photos/' . $lastId);
				$this->copyFiles(Yii::$app->basePath . '/web/uploads/photos/' . $id,Yii::$app->basePath . '/web/uploads/photos/' . $lastId);
				foreach($TourPhotosFind as $key)
				{
					$key->id=null; // PK
					$key->tourId= $lastId;
					$key->setIsNewRecord(true);
					$key->insert(false);
				}
			}
			return $this->redirect(['/tour/update','id'=>$lastId,'currency'=>$currencynow['id'],'cloneRec'=>1]);
        }  
		else
		{
            throw new NotFoundHttpException();
        }
    }
	//end clone
  public function copyFiles($dirname,$newDirname)
  {
    $dir = opendir($dirname);
    while (($file = readdir($dir)) !== false)
    {
      if(is_file($dirname."/".$file))
      {
        copy($dirname."/".$file, $newDirname."/".$file);
      }
    }
    closedir($dir);
  }
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index','currency'=>$currencynow['id']]);
    }
    /**
     *
     */
    public function actionUploadImages()
    {
        // get the files posted
        $tourPhotos = new TourPhotos();
        $images = UploadedFile::getInstances($tourPhotos, 'images');
        // 'images' refers to your file input name attribute
        if (empty($images)) {
            echo json_encode(['error' => 'No files found for upload.']);
            return; // terminate
        }
        // a flag to see if everything is ok
        $success = null;
        // file paths to store
        $paths = [];
        // loop and process files
        foreach ($images as $image) {
            $imageNameParts = explode(".", $image->name);
            $extension = end($imageNameParts);
            $src = Yii::$app->security->generateRandomString().".{$extension}";
            $imageResource = imagecreatefromjpeg($image->tempName);
            $imageResource = imagerotate($imageResource, array_values([0, 0, 0, 180, 0, 0, -90, 0, 90])[@exif_read_data($image->tempName)['Orientation'] ?: 0
], 0);
            FileHelper::createDirectory(Yii::$app->basePath . '/web/uploads/temp/' . Yii::$app->user->identity->getId());
            $target = Yii::$app->basePath . '/web/uploads/temp/' . Yii::$app->user->identity->getId() . '/' . $src;
            if(imagejpeg($imageResource, $target)) {
                $success = true;
                $paths[] = $target;
            } else {
                $success = false;
                break;
            }
        }
        // check and process based on successful status
        if ($success === true) {
            // store a successful response (default at least an empty array). You
            // could return any additional response info you need to the plugin for
            // advanced implementations.
            $output = [];
        } elseif ($success === false) {
            $output = ['error' => 'Error while uploading images. Contact the system administrator'];
            // delete any uploaded files
            foreach ($paths as $file) {
                unlink($file);
            }
        } else {
            $output = ['error' => 'No files were processed.'];
        }
        // return a json encoded response for plugin to process successfully
        echo json_encode($output);
    }
	public function actionDeleteImages()
    {
       	$id = Yii::$app->request->get('id');
		$src = Yii::$app->request->post('key');
		$success = false;
		if($id>0 && $src)
		{
			$target = Yii::$app->basePath . '/web/uploads/photos/' . $id . '/' . $src;
			if (file_exists($target)) 
			{
				unlink($target);
				$model = TourPhotos::find()->where(['src' => $_GET['key']])->one();
				$model->delete();
				$success = true;
				$output=['success' => 'Delete processed.'];
			}
			else
			{
				$output=['error' => 'No files '];
			}
		}
			echo json_encode($output);
    }
	public function actionChecknetprice()
    {
        if ($postData = Yii::$app->request->post())
		{
            $tourTypeId = (int) $postData['tourtypeid'];
			$price = (int) $postData['price'];
			$Netprice = (int) $postData['netprice'];
			if($tourTypeId>0 && $price>0 && $Netprice>0)
			{
				$supervisiorCommission = TourTypes::find('commisssion')->where(['id' => $tourTypeId])->one();
				if($supervisiorCommission)
				{
					if($price - $Netprice < $price*$supervisiorCommission->commission/100)
					{
						return '<span class="text-danger">Your Net price is too high! Please, offer lower Net price.</span>';
					}
				}
			}
			elseif($price <= $Netprice && ($tourTypeId<=0 || $tourTypeId==''))
			{
				return '<span class="text-danger">Your Net price is too high! Please, offer lower Net price.</span>';
			}
			elseif($price < $Netprice)
			{
				return '<span class="text-danger">Your Net price is too high! Please, offer lower Net price.</span>';
			}	
        } 
        return ;
    }
	public function actionStepssave()
    {
        if((int) Yii::$app->request->get('pid')>0)
		{	
			$id = (int) Yii::$app->request->get('pid');
			$tour = $this->findModel($id);
			$userId = Yii::$app->user->identity->getId();
			$currency = Currency::find()->all();
			$currencynow = $this->whatCurrencyNow();
			if ($tour)
			{
				$tourPhotos = new TourPhotos();
				$existingPhotos = [];
				$tourDates = new TourDates();
				$groups = new Groups();
				$groups->newGroups = $tour->groups;
				$pickupPoints = new PickupPoints();
				$allPoints = [];
				$tourLocation = $tour->tourLocations;
				$country = $tour->countries;
				$city = $tour->cities;
				
				if ($tour->load(Yii::$app->request->post())
					 && $country->load(Yii::$app->request->post()) &&
					$city->load(Yii::$app->request->post()) &&
					$tourDates->load(Yii::$app->request->post()) &&
					$tourPhotos->load(Yii::$app->request->post()) &&
					$pickupPoints->load(Yii::$app->request->post())
					//&& $groups->load(Yii::$app->request->post())
					) 
				{
					//echo '<pre>';
					//print_r($_POST);
					//echo '</pre>';
					$tour->status = 0;
					$existingCountry = Countries::findOne(['name' => $country->name]);
					if (!$existingCountry) {
						$country->setContinent();
						$country->save();
					}
					$exisitngCity = Cities::findOne(['name' => $city->name, 'countryId' => $city->id]);
					if (!$exisitngCity) {
						$city->countryId = $country->id;
						$city->save();
					}
					$tour->providerId = Yii::$app->user->identity->getId();
					$tour->descExtra = json_encode($tour->descExtra);
					$tour->inclusion = json_encode($tour->inclusion);
					$tour->exclusion = json_encode($tour->exclusion);
					$tour->AgeTypePrice = json_encode($tour->AgeTypePrice);
					$tour->hotelPickup = json_encode($tour->hotelPickup);
				    $tour->duration = Tour::wordsToSeconds($tour->duration);
					$tour->payMethod = json_encode($tour->payMethod);
					$tour->voucherType = json_encode($tour->voucherType);
					if((int) Yii::$app->request->get('stepNum')==1 || (int) Yii::$app->request->get('stepNum')==2 || (int) Yii::$app->request->get('stepNum')==3 || (int) Yii::$app->request->get('stepNum')==4)
					{
						$tour->itsclone = 1;
					}
					else
					{
						$tour->itsclone = 0;
					}
					if ($tour->save()) 
					{
						$tourLocation->tourId = $tour->id;
						$tourLocation->countryId = $country->id;
						$tourLocation->cityId = $city->id;
						$tourLocation->save();
						foreach ($tour->tourDates as $tourDate) 
						{
							$tourDate->delete();
						}
						foreach ($tourDates->allDates as $key => $values) {
							$tourDates->tourId = $tour->id;
							$tourDates->attributes = $values;
							$tourDates->save();
						}
						
							if (!empty($tourPhotos->promoImage)) 
							{
									
									//TourPhotos::deleteAll('AND',['tourId'=>$id,'isPromo'=>1]);
									Yii::$app->db->createCommand("DELETE FROM TourPhotos WHERE tourId = ".$id." AND isPromo = 1")->execute();
									$tour->promoPhoto->delete();
									$tourPhotos->tourId = $tour->id;
									$tourPhotos->userId = Yii::$app->user->identity->getId();
									$promoImage = $tourPhotos->uploadPromoImage($tourPhotos->promoImage);
									//$tourPhotos->isPromo = 1;
									if ($tourPhotos->save()) {
										$path = $tourPhotos->getImageFile($tour->id);
										file_put_contents($path, $promoImage);
									}
							}
							
							if((int) Yii::$app->request->get('stepNum')==1)
							{
								if(is_dir(Yii::$app->basePath . '/web/uploads/temp/'. Yii::$app->user->identity->getId()))
								{
									
									$images = FileHelper::findFiles(Yii::$app->basePath . '/web/uploads/temp/' . Yii::$app->user->identity->getId());
									if (!empty($images)) {
									Yii::$app->db->createCommand("DELETE FROM TourPhotos WHERE tourId = ".$id." AND isPromo = 0")->execute();
										foreach ($images as $image) 
										{
											$tourPhoto = new TourPhotos();
											$tourPhoto->tourId = $tour->id;
											$tourPhoto->userId = Yii::$app->user->identity->getId();
											$tourPhoto->src = basename($image);
											if ($tourPhoto->save()) {
												// move image from temporary directory
												$path = $tourPhoto->getImageFile($tour->id);
												rename($image, $path);
											}
										
										}
									}
								}
							}
						//$allPoints = json_decode($pickupPoints->allPoints, true);
						if(!empty($pickupPoints->allPoints ))
						{
							foreach ($tour->pickupPoints as $pickupPoint) {
								$pickupPoint->delete();
							}
							$allPoints = json_decode($pickupPoints->allPoints, true);
							foreach ($allPoints as $point) {
								$pickupPoint = new PickupPoints();
								$pickupPoint->tourId = $tour->id;
								$pickupPoint->attributes = $point;
								$pickupPoint->save();
							}
						}
						if (!empty($tour->groups)) {
							foreach ($tour->groups as $group) {
								$group->delete();
							}
						}
						if (!empty($tour->groups)) {
							foreach ($groups->newGroups as $key => $values) {
								$group = new Groups();
								$group->tourId = $tour->id;
								$group->attributes = $values;
								$group->save();
							}
						}
						//return $this->redirect(['/supplier/added-tours','currency'=>$currencynow['id']]);
						return 'step is saving';
					}
					else 
					{
						return;
					}
				} 
				else
				{
					// remove images from temporary directory
					FileHelper::removeDirectory(Yii::$app->basePath . '/web/uploads/temp/' . Yii::$app->user->identity->getId());
					return ;
				}
			} 
			else 
			{
				throw new NotFoundHttpException();
			}
		}
        return ;
    }
	public function actionChangegroupprice()
    {
        if ($postData = Yii::$app->request->post())
		{
            //$json = array();
			$groupId = (int) $postData['groupId'];
			$currencydesire = (int) $postData['currencydesire'];
			$currency = (int) $postData['currency'];
			if($currencydesire>0 && $groupId>0 && $currency>0)
			{
				$prices = Groups::find('price,Netprice')->where(['id' => $groupId])->one();
				//echo '<pre>';
				//print_r($prices);
				//echo '</pre>';
				if($prices)
				{
					$currency_sim = Currency::find('simbol,alias')->where(['id'=>$currency])->one();
					if($currency_sim)
					{
						if($currencydesire!=$currency)
						{
							//need to call converter and for valuta new 
							$currencydesire_sim = Currency::find('simbol,alias')->where(['id'=>$currencydesire])->one();
							if($currencydesire_sim)
							{
								$priceTotal = $this->converter($prices['price'], $currencydesire_sim->alias, $currency_sim->alias);
								$pricesNet = $this->converter($prices['Netprice'], $currencydesire_sim->alias, $currency_sim->alias);
								$pricesBook = $this->converter($prices['price'] - $prices['Netprice'], $currencydesire_sim->alias, $currency_sim->alias);
								if($priceTotal && $pricesNet)
								{
									return $currency_sim['simbol'].';'.$priceTotal.';'.$pricesNet.';'. $pricesBook; 
								}
							}
						}
						else
						{
							$pricesBook =$prices['price']-$prices['Netprice'];
							return $currency_sim['simbol'].';'.$prices['price'].';'.$prices['Netprice'].';'. $pricesBook; 
						}
					}	
				}
			}
		} 
        return ;
    }
    protected function findModel($id)
    {
        if (($model = Tour::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException();
        }
    }
}