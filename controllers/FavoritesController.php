<?php

namespace app\controllers;

use app\components\LayoutParamsTrait;
use Yii;
use app\models\Favorites;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * FavoritesController implements the CRUD actions for Favorites model.
 */
class FavoritesController extends Controller
{
    use LayoutParamsTrait;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Favorites models.
     * @return mixed
     * @throws NotFoundHttpException if the user is not logged in
     */
    public function actionIndex()
    {
        if (!empty(Yii::$app->user->identity)) {
            $userId = Yii::$app->user->identity->getId();
            $favorites = new Favorites();
            $dataProvider = $favorites->search($userId, Yii::$app->request->queryParams);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * Creates a new Favorites model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $favorites = Favorites::findOne(['userId' => Yii::$app->request->post('userId'), 'tourId' => Yii::$app->request->post('tourId')]);
        if (empty($favorites)) {
            $newFavorites = new Favorites();
            if ($newFavorites->load(Yii::$app->request->post(), '') && $newFavorites->save()) {
                echo 'created';
            }
        } else {
            $favorites->delete();
            echo 'deleted';
        }
    }

    /**
     * Deletes an existing Favorites model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Favorites model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Favorites the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Favorites::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException();
        }
    }
}
