<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 20.09.2017
 * Time: 18:00
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use app\assets\WithBootstrapAsset;

WithBootstrapAsset::register($this);

$gridColumns = [
    ['class' => 'kartik\grid\DataColumn', 'attribute' => 'user_id', 'mergeHeader' => true],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'firstname',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $firstnameList,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'Firstname'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'lastname',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $lastnameList,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'Lastname'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'phone',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $phoneList,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'Phone'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'public_email',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $emailList,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'Email'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($dataProvider, $key, $index, $column) {
            return Yii::$app->controller->renderPartial('_expand-row-admin-user', ['model'=>$dataProvider]);
//            return 1;
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true,
        'expandTitle' => 'Open',
        'collapseTitle' => 'Close',
        'expandIcon' => '<span class="adminSupplier__details">Open<span>',
        'collapseIcon' => '<span class="adminSupplier__details">Close<span>',
//        'options' => ['style' => 'width: 3%'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'item_name',
        'header' => 'Current Role',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $rolesList,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'Role'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '200px',
        'header' => "Assign Role<br/>(click on proper icon to assign exact role)",
        'template' => '<div class="adminUsers__roles">{admin}{destination}{user}</div>',
        'buttons' => [
            'admin' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-font"></span>', $url, ['title' => 'Assign as Administrator']);
            },
            'destination' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-briefcase"></span>', $url, ['title' => 'Assign as Destination Manager']);
            },
            'user' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-user"></span>', $url, ['title' => 'Assign as User']);
            },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
            if ($action === 'admin') {
                $url = Url::to(['/admin/role-admin', 'uid' => $model->user_id]);
                return $url;
            } else if ($action === 'destination') {
                $url = Url::to(['/admin/role-destination', 'uid' => $model->user_id]);
                return $url;
            } else if ($action === 'user') {
                $url = Url::to(['/admin/role-user', 'uid' => $model->user_id]);
                return $url;
            }
        },
    ],
];

echo GridView::widget([
    'dataProvider'=> $dataProvider,
    'filterModel' => $searchModel,
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'columns' => $gridColumns,
    'responsive'=>true,
    'hover'=>true,
    'resizableColumns'=>true,
]);