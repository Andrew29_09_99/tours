<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TourSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tour-search">

<?php $js = <<< JS
        $('#toursearch-supplier, #toursearch-type, #toursearch-status').change(function(){
             $('#submit-selected-tour').trigger('click');
        });
JS;
$this->registerJS($js);
?>

    <?php $form = ActiveForm::begin([
        'action' => ['tours'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select (not) approved']); ?>

    <?= $form->field($model, 'type')->dropDownList($category, ['prompt'=>'Select type of tour']); ?>

    <?= $form->field($model, 'supplier')->dropDownList($supplier, ['prompt'=>'Select supplier']); ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['id' => 'submit-selected-tour','class' => 'btn btn-primary', 'style' => ['display' => 'none']]) ?>
<!--        --><?//= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
