<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 06.09.2017
 * Time: 11:09
 */

use yii\widgets\DetailView;

//debug($model->providerDocs);
//die;


echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'regNumberProvider',
        'descProvider',
        'addressProvider',
        'zipProvider',
        'phoneProvider',
        'emailProvider'
    ]
]);

foreach ($model->providerDocs as $docs){
    echo "<img class='adminSupplier__img' src='/uploads/docs/".$model->userId."/".$docs->srcDoc."' weight='300px'>";
}