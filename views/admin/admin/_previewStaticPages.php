<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
//use app\assets\AdminLtePluginAsset;
use yii\helpers\Url;

//AdminLtePluginAsset::register($this);

?>

<div class="l-admin-static_pages">
    <div class="container">
        <div class="col-md-6 adminPages__block">
            <div class="adminPages__text">
                <p><b><?= $model['title'] ?></b><p>
            </div>
            <div class="adminPages__buttonWrap">
                <?= Html::a('Edit', Url::to(['/admin/edit-page', 'pid'=>$model['id']]), ['class' => 'btn btn-warning']) ?>
            </div>
        </div>
<!--        <div class="col-md-10">
            <div class="text-static_pages">
                <?/*= $model['text'] */?>
            </div>
        </div>-->
    </div>
</div>
