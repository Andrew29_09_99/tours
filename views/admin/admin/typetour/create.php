<?php


use app\assets\WithBootstrapAsset;
WithBootstrapAsset::register($this);
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

//$this->title = Yii::t('app', 'Create Guide Languages');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Guide Languages'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">

    <h1><?= Html::encode(Yii::t('app', 'Create tour types')) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
