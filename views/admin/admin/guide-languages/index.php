<?php

use app\assets\WithBootstrapAsset;
WithBootstrapAsset::register($this);
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
?>
<div class="">

        <?php echo Html::a(Yii::t('app', 'Create a language'), ['create-language'], ['class' => 'btn btn-success']) ?>   
		<h1><?php echo Html::encode($this->title) ?></h1>

    <p>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'language',
            'sort',

            
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {up} {down} {update} {delete}',
                'buttons' => [
                    'up' => function ($url, $model, $key) {
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-arrow-up', 'title'=>'Up']), $url, ['class' => 'arrow-up']);;
                    },
                    'down' => function ($url, $model, $key) {
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-arrow-down', 'title'=>'Down']), $url, ['class' => 'arrow-down']);;
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {   
                        case 'update': return Url::to(['update-language', 'id' => $model->id]);     break;
                        case 'view': return Url::to(['view-language', 'id' => $model->id]);         break;
                        case 'delete': return Url::to(['delete-language', 'id' => $model->id]);     break;
                        case 'up': return Url::to(['up-language', 'id' => $model->id]);             break;
                        case 'down': return Url::to(['down-language', 'id' => $model->id]);         break;
                    }
                }
            ],
        ],
    ]); ?>
</div>
