<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 04.09.2017
 * Time: 17:39
 */


use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\WithBootstrapAsset;
WithBootstrapAsset::register($this);

$gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'nameProvider',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(\app\models\Providers::find()->select(['userId','nameProvider'])->asArray()->all(), 'nameProvider', 'nameProvider'),
        'filterWidgetOptions'=>[
             'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'Name of supplier'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'city',
        'filterType' => GridView::FILTER_SELECT2,
//        'filter' => ArrayHelper::map(\app\models\Providers::find()->select(['providers.userId','providerLocation.cityId','cities.name'])->joinWith(['providerLocations', 'cities'])->asArray()->all(), 'providers.userId', 'cities.names'),
        'filter' => $citiesList,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'City'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'country',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $countriesList,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'Country'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
            return Yii::$app->controller->renderPartial('_expand-row-admin-supplier', ['model'=>$model]);
//            return 1;
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true,
        'expandTitle' => 'Details',
        'collapseTitle' => 'Details',
        'expandIcon' => '<span class="adminSupplier__details">Open<span>',
        'collapseIcon' => '<span class="adminSupplier__details">Close<span>',
//        'options' => ['style' => 'width: 3%'],
        'vAlign'=>'middle',
    ],
    [
        'class'=>'kartik\grid\BooleanColumn',
        'attribute' => 'status',
        'vAlign'=>'middle',
    ],
//    [
//        'class' => 'kartik\grid\EditableColumn',
//        'attribute' => 'regNumberProvider',
//        'filterType' => GridView::FILTER_SELECT2,
//        'filter' => ArrayHelper::map(\app\models\Providers::find()->with(['providerLocation'])->select(['userId','cities'])->asArray()->all(), 'userId', 'cities'),
//        'filterWidgetOptions'=>[
//            'pluginOptions'=>['allowClear'=>false],
//        ],
//        'filterInputOptions'=>['placeholder'=>'City'],
//        'vAlign'=>'middle',
//    ],

    ['class' => 'kartik\grid\ActionColumn',
        'template' => '{approve}{block}',
        'vAlign'=>'middle',
        'header' => 'Actions',
        'buttons' => [

            'approve' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-ok-sign"></span>', $url, [
                    'title' => Yii::t('app', 'approve'),
                ]);
            },
            'block' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-remove-sign"></span>', $url, [
                    'title' => Yii::t('app', 'block'),
                ]);
            },
//            'message' => function ($url, $model) {
//                return Html::a('<span class="glyphicon glyphicon-envelope"></span>', $url, [
//                    'title' => Yii::t('app', 'message'),
//                ]);
//            },
//            'tours' => function ($url, $model) {
//                return Html::a('<span class="glyphicon glyphicon-list-alt"></span>', $url, [
//                    'title' => Yii::t('app', 'All tours of supplier'),
//                ]);
//            }

        ],
        'urlCreator' => function ($action, $model, $key, $index) {

            if ($action === 'approve') {
                $url = Url::to(['admin/approve-supplier', 'uid' => $model->userId]);
                return $url;
            }
            if ($action === 'block') {
                $url = Url::to(['admin/block-supplier', 'uid' => $model->userId]);
                return $url;
            }
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'vAlign'=>'middle',
        'header' => 'Orders',
        'template' => '{orders}',
        'buttons' => [
            'orders' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-briefcase"></span>', $url, [
                    'title' => Yii::t('app', 'Display all orders'),
                ]);
            },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {

            if ($action === 'orders') {
                $url = Url::to(['admin/orders', 'uid' => $model->userId]);
                return $url;
            }
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'vAlign'=>'middle',
        'header' => 'Tours',
        'template' => '{tours}',
        'buttons' => [
            'tours' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-list-alt"></span>', $url, [
                    'title' => Yii::t('app', 'View all tours of supplier'),
                ]);
            }
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
            if ($action === 'tours') {
                $url = Url::to(['admin/tours', 'uid' => $model->userId]);
                return $url;
            }
        }
    ]
];


// Generate a bootstrap responsive striped table with row highlighted on hover
echo GridView::widget([
    'dataProvider'=> $model,
    'filterModel' => $searchModel,
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'columns' => $gridColumns,
    'responsive'=>true,
    'hover'=>true,
    'resizableColumns'=>true,
]);