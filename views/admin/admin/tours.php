<?php
use app\components\OwnToursWidget;
use app\assets\WithBootstrapAsset;

WithBootstrapAsset::register($this);

echo $this->render('_search', ['model' => $searchModel, 'supplier' => $supplierList, 'category' => $categoryList, 'status' => $statusList]);

echo OwnToursWidget::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{sorter}\n{summary}\n{items}\n{pager}",
    'sorter' => [
        'attributes' => ['deposit'],
    ],
    'itemOptions' => ['class' => 'item'],
    'itemView' => '/_list',
    'page'  => 'admin'
]);
