<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\web\JsExpression;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $order app\models\Orders */
/* @var $form ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'action' => '/order/create',
    'options' => ['class' => 'c-bookingForm'],
]) ?>
<?= $form->field($order, 'tourId')->hiddenInput(['value' => $tour->id])->label(false) ?>
<?= $form->field($order, 'providerId')->hiddenInput(['value' => $tour->providerId])->label(false) ?>


<?php 
echo '<pre>';
//print_r($tour->groups);
echo '</pre>';

//echo 'min'.$tour->minimalPrice;
?>
    <div class="c-bookingForm__content">
        <div class="c-bookingForm__header">
            <div class="c-bookingForm__wrapper">
                <div class="c-bookingForm__text">Book now just with Booking Deposit</div>
            </div>
            <div class="c-bookingForm__number"><?php echo ($currencynow->simbol)?$currencynow->simbol:$currencydesire->simbol;?><span><?php echo $output['deposit']; ?></span></div>
            <div class="c-bookingForm__text--span">per <?= (empty($tour->groups)) ? '<span></span> traveller' : 'group' ?></div>
        </div>
        <div class="c-bookingForm__info">The balance of <?php echo ($currencynow->simbol)?$currencynow->simbol:$currencydesire->simbol;?><span><?php echo $output['balance'];?></span>  you will pay on the day of the activity</div>
        <div class="c-bookingForm__body">
            <? if (!empty($tour->groups)) { ?>
            <div class="c-bookingForm__tool cf">
                <div style="margin-top:15px;" class="c-bookingForm__head">Price option</div>
                <?= $form->field($order, 'groupId')->widget(Select2::classname(), [
                    //'data' => $tour->groupsList,
					'data' => $tour->rangeGroupsfromto,
                    'theme' => 'default',
                    'options' => [
                        'placeholder' => 'Choose group',
                        'class' => 'c-bookingForm__select',
						'data-currency'=>((int) Yii::$app->request->get('currency')>0)?(int) $_GET['currency']:1,
						'data-currencydesire'=>($currencydesire->id)?$currencydesire->id:1,
						
                    ],
                ])->label(false) ?>
            </div>
            <? } else { ?>
			
            <div class="c-bookingForm__tool c-bookingForm__modal cf">
                <div style="margin-top:15px;" class="c-bookingForm__head">Travellers</div>
                <button type="button" class="c-bookingForm__select js-bookingForm__selection">
                    <div class="c-bookingForm__selectedText">
                        Choose travellers
                    </div>
                    <div class="c-bookingForm__selectedIcon c-bookingForm__selectedIcon--rotated">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 455 455">
                            <path d="M0,0v455h455V0H0z M334.411,296.683L227.5,190.12L120.589,296.683l-21.179-21.248L227.5,147.763l128.089,127.672 L334.411,296.683z" fill="#00bcd4"/>
                        </svg>
                    </div>
                </button>
                <div class="c-bookingForm__modalWindow hide">
                    <? if (!empty($tour->priceAdult)) { ?>
                    <div class="c-bookingForm__row">
                        <div class="c-bookingForm__modalHead">Adults</div>
                        <div class="c-bookingForm__numberBlock">
                            <input type='button' value='–' class='c-bookingForm__quantity--minus' field='Orders[totalAdult]' data='<?php echo $output['deposit']?>' data-price='<?php echo $output['total']?>'/>
                            <?= $form->field($order, 'totalAdult')->textInput(['value' => '1', 'class' => 'c-bookingForm__quantity', 'readonly' => true])->label(false) ?>
                            <input type='button' value='+' class='c-bookingForm__quantity--plus' field='Orders[totalAdult]' data='<?php echo $output['deposit']?>' data-price='<?php echo $output['total']?>'/>
                        </div>
                    </div>
                    <? } ?>
                    <? if (!empty($tour->priceChild)) { ?>
                    <div class="c-bookingForm__row">
                        <div class="c-bookingForm__modalHead">Children <?php $age = json_decode($tour->AgeTypePrice); echo ($age[0] && $age[1])?'('.$age[0].'-'.$age[1].'yo)':'';?></div>
                        <div class="c-bookingForm__numberBlock">
                            <input type='button' value='–' class='c-bookingForm__quantity--minus' field='Orders[totalChild]' data='<?php echo $output['childDeposit']?>' data-price='<?php echo $output['childPrice']?>'/>
                            <?= $form->field($order, 'totalChild')->textInput(['value' => '0', 'class' => 'c-bookingForm__quantity', 'readonly' => true])->label(false) ?>
                            <input type='button' value='+' class='c-bookingForm__quantity--plus' field='Orders[totalChild]' data='<?php echo $output['childDeposit']?>' data-price='<?php echo $output['childPrice']?>'/>
                        </div>
                    </div>
                    <? } ?>
                    <? if (!empty($tour->priceInfant)) { ?>
                    <div class="c-bookingForm__row">
                        <div class="c-bookingForm__modalHead">Infants <?php $age = json_decode($tour->AgeTypePrice); echo ($age[3])?'('.$age[2].'-'.$age[3].'yo)':'';?></div>
                        <div class="c-bookingForm__numberBlock">
                            <input type='button' value='–' class='c-bookingForm__quantity--minus' field='Orders[totalInfant]' data='<?php echo $output['infantDeposit']?>' data-price='<?php echo $output['infantPrice']?>'/>
                            <?= $form->field($order, 'totalInfant')->textInput(['value' => '0', 'class' => 'c-bookingForm__quantity', 'readonly' => true])->label(false) ?>
                            <input type='button' value='+' class='c-bookingForm__quantity--plus' field='Orders[totalInfant]' data='<?php echo $output['infantDeposit']?>' data-price='<?php echo $output['infantPrice']?>'/>
                        </div>
                    </div>
                    <? } ?>
                    <? if (!empty($tour->priceSenior)) { ?>
                    <div class="c-bookingForm__row">
                        <div class="c-bookingForm__modalHead">Seniors <?php $age = json_decode($tour->AgeTypePrice); echo ($age[4] && $age[5])?'('.$age[4].'-'.$age[5].'yo)':'';?></div>
                        <div class="c-bookingForm__numberBlock">
                            <input type='button' value='–' class='c-bookingForm__quantity--minus' field='Orders[totalSenior]' data='<?php echo $output['seniorDeposit']?>' data-price='<?php echo $output['seniorPrice']?>'/>
                            <?= $form->field($order, 'totalSenior')->textInput(['value' => '0', 'class' => 'c-bookingForm__quantity', 'readonly' => true])->label(false) ?>
                            <input type='button' value='+' class='c-bookingForm__quantity--plus' field='Orders[totalSenior]' data='<?php echo $output['seniorDeposit']?>' data-price='<?php echo $output['seniorPrice']?>'/>
                        </div>
                    </div>
                    <? } ?>
                </div>
            </div>
            <? } ?>
            <div class="js-bookingForm__wrapper">
                <div class="c-bookingForm__tool cf">
                    <div style="margin-top:15px;" class="c-bookingForm__head">Language options</div>
                </div>
                <div class="c-bookingForm__tool">
                    <?= $form->field($order, 'languageId')->widget(Select2::classname(), [
                        'data' => $tour->guidesList,
                        'theme' => 'default',
                        'options' => [
                            'placeholder' => 'Choose guide',
                            'class' => 'c-bookingForm__select',
                            'id' => 'chosenGuide',
                        ],
                    ])->label(false) ?>
                </div>
                <div class="c-bookingForm__tool c-bookingForm__tool--disabled cf">
                    <div style="" class="c-bookingForm__head">Select available date</div>
                </div>
                <div class="c-bookingForm__tool c-bookingForm__tool--disabled">
                    <?= $form->field($order, "dateStartTour")->widget(DatePicker::classname(), [
                        'type' => DatePicker::TYPE_INPUT,
                        'options' => [
                            'class' => 'c-bookingForm__select c-bookingForm__select--date',
                            'id' => 'guideDatepicker',
                            'placeholder' => 'Available date',
                        ],
                        'pluginOptions' => [
                            'format' => 'd/m/yyyy',
                            'beforeShowDay' => new JsExpression("
                            function(date) {
                                if ($.inArray(date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear(), guideDates) !== -1) {
                                    return;
                                } else {
                                    return false;
                                }
                            }")
                        ],
                    ])->label(false) ?>
                </div>
            </div>
			<!-- new design -->
			<div style="margin-top:10px;" class="c-bookingForm__tool cf">
				<div class="c-bookingForm__tool--leftPart">
					<div style="color:#005096;text-align:left;" class="c-bookingForm__head--blue">SUMMARY:</div>
				</div>
			</div>
			<div class="c-bookingForm__tool cf">
				
                    <div style="color:#005096;" class="cf">
                        <div style="width:70%;" class="c-bookingForm__tool--leftPart">
							<div style="color:#005096;" class="c-bookingForm__head--blue">
                                Required Booking Deposit:
                            </div>
                        </div>
                        <div style="width:30%;" class="c-bookingForm__tool--rightPart">
                            <div style="font-size:14px;text-align:left;color:#005096;margin-top:5px;" class="c-bookingForm__number--rightBlue">
							    <?php echo ($currencynow->simbol)?$currencynow->simbol:$currencydesire->simbol;?><span style=""><?php echo $output['deposit'];?></span> 
								<div style="display:inline;" class="c-bookingForm__tooltip">(?)
									<div class="c-bookingForm__tooltipText">
										<div class="c-bookingForm__tool--leftPart">
											<div class="c-bookingForm__head">
												Head1
											</div>
											<div class="c-bookingForm__head--small">
												some text1 goes here
											</div>
										</div>
                                    </div>
								</div>
                            </div>
							
                        </div>
                    </div>
            </div>
			<div class="c-bookingForm__tool cf">
                    <div class="cf">
                        <div class="c-bookingForm__tool--leftPart">
                            <div style="font-size:13px;color:#005096;font-weight:normal;" class="c-bookingForm__head--blue">
                                Remaining Balance to pay:
                            </div>
                        </div>
                        <div class="c-bookingForm__tool--rightPart">
                            <div style="font-size:13px;text-align:left;color:#005096;margin-top:5px;font-weight:normal;" class="c-bookingForm__number--rightBlue">
							    <?php echo ($currencynow->simbol)?$currencynow->simbol:$currencydesire->simbol;?><p style="display:inline;"><?php echo $output['depositnow'];?></p>
								<div style="display:inline;" class="c-bookingForm__tooltip">(?)
									<div class="c-bookingForm__tooltipText">
										<div class="c-bookingForm__tool--leftPart">
											<div class="c-bookingForm__head">
												Head2
											</div>
											<div class="c-bookingForm__head--small">
												some text2 goes here
											</div>
										</div>
                                    </div>
								</div>	
                            </div>
                        </div>
                    </div>
            </div>
			<div class="c-bookingForm__tool cf">
                    <div style="width:20%;" class="c-bookingForm__tool--leftPart">
                        <div class="c-bookingForm__head">
                            TOTAL:
                        </div>
                    </div>
                    <div style="width:80%;" class="c-bookingForm__tool--rightPart">
                        <div style="font-size:14px;text-align:left;color:#005096;margin-top:4px;" class="c-bookingForm__number--right">
                           <?php echo ($currencynow->simbol)?$currencynow->simbol:$currencydesire->simbol;?><span><?php echo $output['total']; ?></span>
							<div style="display:inline;" class="c-bookingForm__tooltip">(?)
									<div class="c-bookingForm__tooltipText">
										<div class="c-bookingForm__tool--leftPart">
											<div class="c-bookingForm__head">
												Head3
											</div>
											<div class="c-bookingForm__head--small">
												some text3 goes here
											</div>
										</div>
                                    </div>
								</div>
                        </div>
                    </div>
            </div>
			<!--
			<div style="border-top:none;border-bottom:1px solid #00bcd4;" class="cf">
                    <div class="c-bookingForm__tool--wrapperBlue cf">
                        <div style="width:60%" class="c-bookingForm__tool--leftPart">
                            <div style="font-size:16px;" class="c-bookingForm__head--blue">
                                Book now just with Booking Deposit of
                            </div>
                           
                        </div>
                        <div style="width:40%" class="c-bookingForm__tool--rightPart">
                            <div style="text-align:left;" class="c-bookingForm__number--rightBlue">
							    <span><?php //echo $output['deposit'];?></span> <?php //echo ($currencynow->simbol)?$currencynow->simbol:$currencydesire->simbol;?>
                            </div>
                        </div>
                    </div>
            </div>
			-->
			<!-- end new des -->
				<!--
			   <div class="c-bookingForm__tool cf">
                    <div class="c-bookingForm__tool--wrapperBlue cf">
                        <div class="c-bookingForm__tool--leftPart">
                            <div class="c-bookingForm__head--blue">
                                Required Booking Deposit
                            </div>
                            <span class="c-bookingForm__tooltip">
                                ?
                                <div class="c-bookingForm__tooltipText">
                                    <div class="c-bookingForm__tool--leftPart">
                                        <div class="c-bookingForm__head">
                                            Remaining Balance
                                        </div>
                                        <div class="c-bookingForm__head--small">
                                            is that what you pay on the day of the Activity
                                        </div>
                                    </div>
                                    <div class="c-bookingForm__tool--rightPart">
                                        <div class="c-bookingForm__number--right">
										
                                            <?php //echo ($currencynow->simbol)?$currencynow->simbol:$currencydesire->simbol;?>
											<p><?php //echo $output['balance'];?></p>
											
                                        </div>
                                    </div>
                                </div>
                            </span>
                            <div class="c-bookingForm__head--smallBlue">
                                is that what you pay now
                            </div>
                        </div>
                        <div class="c-bookingForm__tool--rightPart">
                            <div class="c-bookingForm__number--rightBlue">
							
                                <span><?php //echo $output['deposit'];?></span> <?php //echo ($currencynow->simbol)?$currencynow->simbol:$currencydesire->simbol;?>
                            </div>
                        </div>
                    </div>
                </div>
			
				
				<div class="c-bookingForm__tool cf">
                    <div class="c-bookingForm__tool--wrapperBlue cf">
                        <div class="c-bookingForm__tool--leftPart">
                            <div class="c-bookingForm__head--blue">
                                REMAINING BALANCE
                            </div>
                         
                            <div class="c-bookingForm__head--smallBlue">
                                to pay on the day of the Tour
                            </div>
                        </div>
                        <div class="c-bookingForm__tool--rightPart">
                            <div class="c-bookingForm__number--rightBlue">
							    <p><?php //echo $output['depositnow'];?></p> <?php //echo ($currencynow->simbol)?$currencynow->simbol:$currencydesire->simbol;?>
                            </div>
                        </div>
                    </div>
                </div>
				
			
                <div class="c-bookingForm__tool cf">
                    <div class="c-bookingForm__tool--leftPart">
                        <div class="c-bookingForm__head">
                            <?php if(false):?>TOTAL per <?= (empty($tour->groups)) ? 'traveller' : 'group' ?><?php endif;?>TOTAL
                        </div>
                    </div>
                    <div class="c-bookingForm__tool--rightPart">
                        <div class="c-bookingForm__number--right">
                            <span><?php //echo $output['total']; ?></span> <?php //echo ($currencynow->simbol)?$currencynow->simbol:$currencydesire->simbol;?>
                        </div>
                    </div>
                </div>
				-->
                <div class="c-bookingForm__buttonWrap">
                    <?= Html::submitButton('BOOK NOW', ['class' => 'c-button']) ?>
                </div>
        </div>
    </div>

<?php ActiveForm::end() ?>