<?php

use kartik\select2\Select2;
use yii\helpers\Url;
use app\components\CurrencyWidget;
?>

<header class="l-header">
    <div class="l-header__content cf">
        <div class="l-header__leftPart">
            <div class="l-header__logo">
                <div class="c-logo">
                    <a href="<?= Url::to(['/site/index','currency'=>$currency['id']]) ?>" class="c-logo__link c-logo__link--header">
                        <img src="/img/icon/logo.svg" alt="" class="c-logo__img c-logo__img--header">
                    </a>
                </div>
            </div>
        </div>
        <div class="l-header__rightPart">
            <div class="l-header__tools">
                <div class="l-header__tool">
                    <div class="c-headerLink">
                        <a href="<?=Url::to(['/user/register','currency'=>$currency['id']])?>" class="c-headerLink__link cf">
                            <div class="c-headerLink__img">
                                <svg class="c-headerLink__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                                    <circle class="c-headerLink__icon__circle" cx="16" cy="16" r="14.583"/>
                                    <path class="c-headerLink__icon__content" d="M19.108 12.406c0 1.717-1.392 3.108-3.108 3.108s-3.108-1.392-3.108-3.108c0-1.717 1.392-3.108 3.108-3.108s3.108 1.391 3.108 3.108zM16 17.069c-3.111 0-5.634 2.523-5.634 5.634h11.268c0-3.112-2.523-5.634-5.634-5.634zm0 0"/>
                                </svg>
                            </div>
                            <div class="c-headerLink__content">
                                <div class="c-headerLink__text">Sign Up</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="l-header__tool">
                    <div class="c-headerLink">
                        <a href="<?=Url::to(['/user/login','currency'=>$currency['id']])?>" class="c-headerLink__link cf">
                            <div class="c-headerLink__img">
                                <svg class="c-headerLink__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                                    <circle class="c-headerLink__icon__circle" cx="16" cy="16" r="14.583"/>
                                    <path class="c-headerLink__icon__content" d="M19.108 12.406c0 1.717-1.392 3.108-3.108 3.108s-3.108-1.392-3.108-3.108c0-1.717 1.392-3.108 3.108-3.108s3.108 1.391 3.108 3.108zM16 17.069c-3.111 0-5.634 2.523-5.634 5.634h11.268c0-3.112-2.523-5.634-5.634-5.634zm0 0"/>
                                </svg>
                            </div>
                            <div class="c-headerLink__content">
                                <div class="c-headerLink__text">Log In</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="l-header__tool">
                    <div class="c-headerLink">
                        <a href="<?=Url::to(['/supplier/index','currency'=>$currency['id']])?>" class="c-headerLink__link cf">
                            <div class="c-headerLink__content">
                                <div class="c-headerLink__text--simpleText">Add your listing</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="l-header__imgs">
                <div class="l-header__img">
                    <div class="c-headerIcon c-headerIcon--watch">
                        <a href="<?= Url::to(['/tour/recently-viewed','currency'=>$currency['id']]) ?>" class="c-headerIcon__link">
                            <div class="c-headerIcon__img">
                                <svg class="c-headerIcon__icon c-headerIcon__icon--watch" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 488.85 488.85">
                                    <path d="M244.425 98.725c-93.4 0-178.1 51.1-240.6 134.1-5.1 6.8-5.1 16.3 0 23.1 62.5 83.1 147.2 134.2 240.6 134.2s178.1-51.1 240.6-134.1c5.1-6.8 5.1-16.3 0-23.1-62.5-83.1-147.2-134.2-240.6-134.2zm6.7 248.3c-62 3.9-113.2-47.2-109.3-109.3 3.2-51.2 44.7-92.7 95.9-95.9 62-3.9 113.2 47.2 109.3 109.3-3.3 51.1-44.8 92.6-95.9 95.9zm-3.1-47.4c-33.4 2.1-61-25.4-58.8-58.8 1.7-27.6 24.1-49.9 51.7-51.7 33.4-2.1 61 25.4 58.8 58.8-1.8 27.7-24.2 50-51.7 51.7z"/>
                                </svg>
                            </div>
                            <? if (!empty($this->params['recentlyViewedCounter'])) { ?>
                                <div class="c-headerIcon__counter"><?= $this->params['recentlyViewedCounter'] ?></div>
                            <? } ?>
                        </a>
                    </div>
                </div>
            </div>
			<div class="l-header__currency">
				<?= CurrencyWidget::widget([]) ?>
			</div>
<!--            <div class="l-header__currency">
                <?/*= Select2::widget([
                    'name' => 'currency',
                    'data' => ['EUR', 'USD', 'UAH', 'RUB'],
                    'theme' => 'default',
                    'options' => [
                        'class' => 'c-currency',
                    ],
                    'pluginOptions' => [
                        // hide the search box
                        'minimumResultsForSearch' => -1
                    ],
                ]) */?>
            </div>-->
            <div class="l-header__basket">
                <a href="<?= Url::to(['/cart/index','currency'=>$currency['id']]) ?>" class="c-basket">
                    <div class="c-basket__img">
                        <svg class="c-basket__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 510">
                            <path d="M153 408c-28.05 0-51 22.95-51 51s22.95 51 51 51 51-22.95 51-51-22.95-51-51-51zM0 0v51h51l91.8 193.8-35.7 61.2c-2.55 7.65-5.1 17.85-5.1 25.5 0 28.05 22.95 51 51 51h306v-51H163.2c-2.55 0-5.1-2.55-5.1-5.1v-2.551l22.95-43.35h188.7c20.4 0 35.7-10.2 43.35-25.5L504.9 89.25c5.1-5.1 5.1-7.65 5.1-12.75 0-15.3-10.2-25.5-25.5-25.5H107.1L84.15 0H0zm408 408c-28.05 0-51 22.95-51 51s22.95 51 51 51 51-22.95 51-51-22.95-51-51-51z"/>
                        </svg>
                    </div>
                    <div class="c-basket__counter">
                        <div class="c-basket__number"><?= $this->params['cartCounter'] ?></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</header>