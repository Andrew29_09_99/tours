<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $upload app\models\UploadAvatarForm */

$js = <<< JS
    $('.c-user__bg').click(function(){
        $('#upload_avatar').trigger('click');
    });
    $('#upload_avatar').change(function(){
        var upload = new FormData();
        upload.append('file', $('input[id="upload_avatar"]')[0].files[0]);
        $.ajax({
            cache: false,
            processData: false,
            contentType: false,
            url: '/settings/update-avatar',
            data: upload,
            type: 'POST',
            success: function(data){
                $('#usr_img').attr('src', data);
            },
            error: function(e){
                // alert('Error');
            }
        });
    });
JS;
$this->registerJS($js);

?>

<div class="l-profilePage__leftPart">
    <div class="c-user">
        <div class="c-user__info">
            <div class="c-user__photo">
                <?= Html::img($this->params['avatar'], [
                    'class' => 'img-rounded c-user_img',
                    'id' => 'usr_img',
                    'alt' => $this->params['profile']->username,
                    'width' => 180
                ]) ?>
                <?php if(!empty($upload)) : ?>
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
                        <?= $form->field($upload, 'uploadImageUser', ['template' => "{label}\n{input}"])->fileInput(['id' => 'upload_avatar']) ?>
                    <?php ActiveForm::end() ?>
                <?php endif ?>
                <div class="c-user__bg"></div>
            </div>
            <div class="c-user__name">
                <?= "{$this->params['profile']->firstname} {$this->params['profile']->lastname}" ?>
            </div>
            <div class="c-user__list list">
                <div class="c-user__item item">
                    <div class="c-user__counter"><?= $this->params['profile']->bookingsCount ?></div>
                    <div class="c-user__description">bookings</div>
                </div>
                <div class="c-user__item item">
                    <div class="c-user__counter"><?= $this->params['profile']->reviewsCount ?></div>
                    <div class="c-user__description">reviews</div>
                </div>
                <div class="c-user__item item">
                    <div class="c-user__counter"><?= $this->params['profile']->photosCount ?></div>
                    <div class="c-user__description">photos</div>
                </div>
            </div>
        </div>
      
        <div class="c-user__controllers">
<!--            <a href="#" class="c-user__controller caption_settings">
                <svg class="c-user__icon" xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 51.997 51.997">
                    <path d="M51.911 16.242c-.759-8.354-6.672-14.415-14.072-14.415-4.93 0-9.444 2.653-11.984 6.905-2.517-4.307-6.846-6.906-11.697-6.906C6.759 1.826.845 7.887.087 16.241c-.06.369-.306 2.311.442 5.478 1.078 4.568 3.568 8.723 7.199 12.013l18.115 16.439 18.426-16.438c3.631-3.291 6.121-7.445 7.199-12.014.748-3.166.502-5.108.443-5.477z"/>
                </svg>

                <div class="c-user__text" >Wishlist</div>
                <div class="c-user__counter c-user__counter--small">(<span
                            class="c-user__index">2</span>)
                </div>
            </a>-->
<!--            <a href="#" class="c-user__controller caption_settings">
                <svg class="c-user__icon" xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 488.85 488.85">
                    <path d="M244.425 98.725c-93.4 0-178.1 51.1-240.6 134.1-5.1 6.8-5.1 16.3 0 23.1 62.5 83.1 147.2 134.2 240.6 134.2s178.1-51.1 240.6-134.1c5.1-6.8 5.1-16.3 0-23.1-62.5-83.1-147.2-134.2-240.6-134.2zm6.7 248.3c-62 3.9-113.2-47.2-109.3-109.3 3.2-51.2 44.7-92.7 95.9-95.9 62-3.9 113.2 47.2 109.3 109.3-3.3 51.1-44.8 92.6-95.9 95.9zm-3.1-47.4c-33.4 2.1-61-25.4-58.8-58.8 1.7-27.6 24.1-49.9 51.7-51.7 33.4-2.1 61 25.4 58.8 58.8-1.8 27.7-24.2 50-51.7 51.7z"/>
                </svg>

                <div class="c-user__text" >Recently viewed</div>
                <div class="c-user__counter c-user__counter--small">(<span
                            class="c-user__index">2</span>)
                </div>
            </a>-->
			<a href="javascript:void(0);" class="c-user__controller js-addActiveClass caption_settings ">
                <svg class="c-user__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">
                    <path d="M7 9L5.268 7.484.316 11.729c.18.167.423.271.691.271h11.986c.267 0 .509-.104.688-.271L8.732 7.484 7 9z"/>
                    <path d="M13.684 2.271c-.18-.168-.422-.271-.691-.271H1.007c-.267 0-.509.104-.689.273L7 8l6.684-5.729zM0 2.878v8.308l4.833-4.107m4.334 0L14 11.186V2.875"/>
                </svg>
                <div class="c-user__text">
                    TripsPoint Messenger
                </div>
            </a>
			<a href="<?= Url::to(['/messages/dialogs']) ?>" class="c-user__controller">
				<div class="c-user__text">Inbox</div>
                <? if (!empty($this->params['profile']->newMessagesCount)) { ?>
                    <div class="c-user__counter c-user__counter--small">
                        <?= $this->params['profile']->newMessagesCount ?>
                        <div class="c-user__counterBg"></div>
                    </div>
                <? } ?>
            </a>
			<a href="<?= Url::to(['/messages/dialogs']) ?>" class="c-user__controller">
				<div class="c-user__text">Sent Messages</div>
                <? if (!empty($this->params['profile']->newMessagesCount)) { ?>
                    <div class="c-user__counter c-user__counter--small">
                        <?= $this->params['profile']->newMessagesCount ?>
                        <div class="c-user__counterBg"></div>
                    </div>
                <? } ?>
            </a>
			<a href="<?= Url::to(['/messages/dialogs']) ?>" class="c-user__controller">
				<div class="c-user__text">Write New Message</div>
                <? if (!empty($this->params['profile']->newMessagesCount)) { ?>
                    <div class="c-user__counter c-user__counter--small">
                        <?= $this->params['profile']->newMessagesCount ?>
                        <div class="c-user__counterBg"></div>
                    </div>
                <? } ?>
            </a>
			
            <?php if($this->params['isSupplier']) : ?>
				<!-- order -->
				<a href="javascript:void(0);" class="c-user__controller caption_settings">
					<svg class="c-user__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 526 594">
						<path d="M122.7 320.8c-5.3-4.9-6.2-12.9-2-18.7 21.9-31.2 69.2-50.3 100.5-58.6v-13.8c-32.2-23.6-51.2-69.5-51.2-103 0-51.1 41.6-92.6 92.7-92.6 51 0 92.6 41.5 92.6 92.6 0 33.5-19 79.4-51.1 103v13.8c31.3 8.3 78.6 27.4 100.6 58.6 4.1 5.8 3.2 13.8-2.1 18.7-16.2 14.7-69 67.9-69 132.7 0 5.6.2 11.2.6 16.6.3 3.9-1.1 7.8-3.8 10.7-2.7 2.9-6.5 4.5-10.4 4.5H205.2c-3.9 0-7.7-1.6-10.4-4.5s-4-6.8-3.8-10.7c.4-5.5.6-11 .6-16.6.1-64.9-52.8-118-68.9-132.7zm275.7 178H126.9c-7.9 0-14.2 6.3-14.2 14.2v26.5c0 7.9 6.3 14.2 14.2 14.2h271.6c7.8 0 14.2-6.4 14.2-14.2V513c0-7.9-6.4-14.2-14.3-14.2zm0 0"/>
					</svg>
					<div class="c-user__text">
						Orders
					</div>
				</a>
				<a href="<?= Url::to(['/supplier/orders','OrdersSearch[orderStatus]'=>0]) ?>" class="c-user__controller ">
					<div class="c-user__text">
						New Orders to Confirm
					</div>
					<? if (!empty($this->params['profile']->newOrdersCount)) { ?>
						<div class="c-user__counter c-user__counter--small">
							<?= $this->params['profile']->newOrdersCount ?>
							<div class="c-user__counterBg"></div>
						</div>
					<? } ?>
				</a>
				<a href="<?= Url::to(['/supplier/orders','OrdersSearch[orderStatus]'=>1,'OrdersSearch[datePendingOrder]'=>date('d/m/Y')]) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Confirmed Pending Orders
					</div>
					<? if (!empty($this->params['profile']->newOrdersCount)) { ?>
						<div class="c-user__counter c-user__counter--small">
							<?= $this->params['profile']->newOrdersCount ?>
							<div class="c-user__counterBg"></div>
						</div>
					<? } ?>
				</a>
				<a href="<?= Url::to(['/supplier/orders','OrdersSearch[orderStatus]'=>1,'OrdersSearch[dateRealizedOrder]'=>date('d/m/Y')]) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Confirmed Realized Orders
					</div>
					<? if (!empty($this->params['profile']->newOrdersCount)) { ?>
						<div class="c-user__counter c-user__counter--small">
							<?= $this->params['profile']->newOrdersCount ?>
							<div class="c-user__counterBg"></div>
						</div>
					<? } ?>
				</a>
				<a href="<?= Url::to(['/supplier/orders']) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Cancelled by me Orders
					</div>
					<? if (!empty($this->params['profile']->newOrdersCount)) { ?>
						<div class="c-user__counter c-user__counter--small">
							<?= $this->params['profile']->newOrdersCount ?>
							<div class="c-user__counterBg"></div>
						</div>
					<? } ?>
				</a>
				<a href="<?= Url::to(['/supplier/orders']) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Cancelled by Customer Orders
					</div>
					<? if (!empty($this->params['profile']->newOrdersCount)) { ?>
						<div class="c-user__counter c-user__counter--small">
							<?= $this->params['profile']->newOrdersCount ?>
							<div class="c-user__counterBg"></div>
						</div>
					<? } ?>
				</a>
				<!-- end order -->
				
				<!-- Listings -->
				<a href="javascript:void(0);" class="c-user__controller caption_settings">
					<svg class="c-user__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 526 594">
						<path d="M122.7 320.8c-5.3-4.9-6.2-12.9-2-18.7 21.9-31.2 69.2-50.3 100.5-58.6v-13.8c-32.2-23.6-51.2-69.5-51.2-103 0-51.1 41.6-92.6 92.7-92.6 51 0 92.6 41.5 92.6 92.6 0 33.5-19 79.4-51.1 103v13.8c31.3 8.3 78.6 27.4 100.6 58.6 4.1 5.8 3.2 13.8-2.1 18.7-16.2 14.7-69 67.9-69 132.7 0 5.6.2 11.2.6 16.6.3 3.9-1.1 7.8-3.8 10.7-2.7 2.9-6.5 4.5-10.4 4.5H205.2c-3.9 0-7.7-1.6-10.4-4.5s-4-6.8-3.8-10.7c.4-5.5.6-11 .6-16.6.1-64.9-52.8-118-68.9-132.7zm275.7 178H126.9c-7.9 0-14.2 6.3-14.2 14.2v26.5c0 7.9 6.3 14.2 14.2 14.2h271.6c7.8 0 14.2-6.4 14.2-14.2V513c0-7.9-6.4-14.2-14.3-14.2zm0 0"/>
					</svg>
					<div class="c-user__text">
						Listings
					</div>
				</a>
				<a href="<?= Url::to(['/tour/create']) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Add New Listings 
					</div>
				</a>
				<a href="<?= Url::to(['/supplier/added-tours','active'=>1,'clone'=>0]) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Active Listings 
					</div>
				</a>
				<a href="<?= Url::to(['/supplier/added-tours','clone'=>0]) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Listings to Publish
					</div>
				</a>
				<a href="<?= Url::to(['/supplier/added-tours','active'=>0,'clone'=>1]) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Awaiting Approval Listings 
					</div>
				</a>
				<!-- End Listings -->
				
				<!-- Availability  -->
				<a href="javascript:void(0);" class="c-user__controller caption_settings">
					<svg class="c-user__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 526 594">
						<path d="M122.7 320.8c-5.3-4.9-6.2-12.9-2-18.7 21.9-31.2 69.2-50.3 100.5-58.6v-13.8c-32.2-23.6-51.2-69.5-51.2-103 0-51.1 41.6-92.6 92.7-92.6 51 0 92.6 41.5 92.6 92.6 0 33.5-19 79.4-51.1 103v13.8c31.3 8.3 78.6 27.4 100.6 58.6 4.1 5.8 3.2 13.8-2.1 18.7-16.2 14.7-69 67.9-69 132.7 0 5.6.2 11.2.6 16.6.3 3.9-1.1 7.8-3.8 10.7-2.7 2.9-6.5 4.5-10.4 4.5H205.2c-3.9 0-7.7-1.6-10.4-4.5s-4-6.8-3.8-10.7c.4-5.5.6-11 .6-16.6.1-64.9-52.8-118-68.9-132.7zm275.7 178H126.9c-7.9 0-14.2 6.3-14.2 14.2v26.5c0 7.9 6.3 14.2 14.2 14.2h271.6c7.8 0 14.2-6.4 14.2-14.2V513c0-7.9-6.4-14.2-14.3-14.2zm0 0"/>
					</svg>
					<div class="c-user__text">
						Availability
					</div>
				</a>
				<a href="<?= Url::to(['/supplier/added-tours','availbe'=>1]) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Availability Alert
					</div>
					
				</a>
				<a href="<?= Url::to(['/supplier/added-tours','availbe'=>2]) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Inactive Listings to Renew
					</div>
					
				</a>
				<a href="<?= Url::to(['/supplier/orders']) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Manage Listings Availability
					</div>
					
				</a>
				<!-- End Availability  -->
				
				<!-- Reviews -->
				<a href="javascript:void(0);" class="c-user__controller caption_settings">
					<svg class="c-user__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 386 348">
						<path d="M338.7 195.6H60.1c-2.9 0-5.4 1.1-7.5 3.2-2.1 2.1-3.2 4.6-3.2 7.5v21.4c0 2.9 1.1 5.4 3.2 7.5 2.1 2.1 4.6 3.2 7.5 3.2h278.6c2.9 0 5.4-1.1 7.5-3.2 2.1-2.1 3.2-4.6 3.2-7.5v-21.4c0-2.9-1.1-5.4-3.2-7.5-2.1-2.1-4.6-3.2-7.5-3.2zm0 64.3H60.1c-2.9 0-5.4 1.1-7.5 3.2-2.1 2.1-3.2 4.6-3.2 7.5V292c0 2.9 1.1 5.4 3.2 7.5 2.1 2.1 4.6 3.2 7.5 3.2h278.6c2.9 0 5.4-1.1 7.5-3.2 2.1-2.1 3.2-4.6 3.2-7.5v-21.4c0-2.9-1.1-5.4-3.2-7.5-2.1-2.2-4.6-3.2-7.5-3.2zm7.5-189.7c-2.1-2.1-4.6-3.2-7.5-3.2H60.1c-2.9 0-5.4 1.1-7.5 3.2-2.1 2.1-3.2 4.6-3.2 7.5v21.4c0 2.9 1.1 5.4 3.2 7.5 2.1 2.1 4.6 3.2 7.5 3.2h278.6c2.9 0 5.4-1.1 7.5-3.2 2.1-2.1 3.2-4.6 3.2-7.5V77.7c0-2.9-1.1-5.4-3.2-7.5zm-7.5 61.1H60.1c-2.9 0-5.4 1.1-7.5 3.2-2.1 2.1-3.2 4.6-3.2 7.5v21.4c0 2.9 1.1 5.4 3.2 7.5 2.1 2.1 4.6 3.2 7.5 3.2h278.6c2.9 0 5.4-1.1 7.5-3.2 2.1-2.1 3.2-4.6 3.2-7.5V142c0-2.9-1.1-5.4-3.2-7.5-2.1-2.1-4.6-3.2-7.5-3.2zm0 0"/>
					</svg>
					<div class="c-user__text">
						Reviews
					</div>
				</a>
				<a href="<?= Url::to(['/profile/reviews','todo'=>1]) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Received Reviews
					</div>
				</a>
				<a href="<?= Url::to(['/profile/reviews','todo'=>1]) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Reviews to Answer
					</div>
				</a>
				<a href="<?= Url::to(['/profile/reviews','todo'=>0]) ?>" class="c-user__controller ">
					<div class="c-user__text">
						Answered Reviews 
					</div>
				</a>
				<!-- End Reviews -->
				
				<!-- Setting Supplier -->
				<a href="javascript:void(0);" class="c-user__controller caption_settings">
					<svg class="c-user__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 268.765 268.765">
						<path d="M267.92 119.461c-.425-3.778-4.83-6.617-8.639-6.617-12.315 0-23.243-7.231-27.826-18.414-4.682-11.454-1.663-24.812 7.515-33.231 2.889-2.641 3.24-7.062.817-10.133-6.303-8.004-13.467-15.234-21.289-21.5-3.063-2.458-7.557-2.116-10.213.825-8.01 8.871-22.398 12.168-33.516 7.529-11.57-4.867-18.866-16.591-18.152-29.176.235-3.953-2.654-7.39-6.595-7.849-10.038-1.161-20.164-1.197-30.232-.08-3.896.43-6.785 3.786-6.654 7.689.438 12.461-6.946 23.98-18.401 28.672-10.985 4.487-25.272 1.218-33.266-7.574-2.642-2.896-7.063-3.252-10.141-.853-8.054 6.319-15.379 13.555-21.74 21.493-2.481 3.086-2.116 7.559.802 10.214 9.353 8.47 12.373 21.944 7.514 33.53-4.639 11.046-16.109 18.165-29.24 18.165-4.261-.137-7.296 2.723-7.762 6.597-1.182 10.096-1.196 20.383-.058 30.561.422 3.794 4.961 6.608 8.812 6.608 11.702-.299 22.937 6.946 27.65 18.415 4.698 11.454 1.678 24.804-7.514 33.23-2.875 2.641-3.24 7.055-.817 10.126 6.244 7.953 13.409 15.19 21.259 21.508 3.079 2.481 7.559 2.131 10.228-.81 8.04-8.893 22.427-12.184 33.501-7.536 11.599 4.852 18.895 16.575 18.181 29.167-.233 3.955 2.67 7.398 6.595 7.85 5.135.599 10.301.898 15.481.898 4.917 0 9.835-.27 14.752-.817 3.897-.43 6.784-3.786 6.653-7.696-.451-12.454 6.946-23.973 18.386-28.657 11.059-4.517 25.286-1.211 33.281 7.572 2.657 2.89 7.047 3.239 10.142.848 8.039-6.304 15.349-13.534 21.74-21.494 2.48-3.079 2.13-7.559-.803-10.213-9.353-8.47-12.388-21.946-7.529-33.524 4.568-10.899 15.612-18.217 27.491-18.217l1.662.043c3.853.313 7.398-2.655 7.865-6.588 1.184-10.105 1.198-20.383.06-30.561zm-133.325 60.03c-24.718 0-44.824-20.106-44.824-44.824 0-24.717 20.106-44.824 44.824-44.824 24.717 0 44.823 20.107 44.823 44.824 0 24.718-20.106 44.824-44.823 44.824z" clip-rule="evenodd"/>
					</svg>
					<div class="c-user__text">
						Supplier Settings
					</div>
				</a>
				<a href="<?= Url::to(['/user/settings','setting'=>1]) ?>" class="c-user__controller ">
					<div class="c-user__text">
					   Supplier Account Settings
					</div>
				</a>
				<a href="<?= Url::to(['/user/settings']) ?>" class="c-user__controller ">
					<div class="c-user__text">
					   Upload New Document
					</div>
				</a>
				<!-- end Setting Supplier -->
           <?php endif ?>
            <a href="javascript:void(0);" class="c-user__controller caption_settings">
                <svg class="c-user__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 526 594">
                    <path d="M122.7 320.8c-5.3-4.9-6.2-12.9-2-18.7 21.9-31.2 69.2-50.3 100.5-58.6v-13.8c-32.2-23.6-51.2-69.5-51.2-103 0-51.1 41.6-92.6 92.7-92.6 51 0 92.6 41.5 92.6 92.6 0 33.5-19 79.4-51.1 103v13.8c31.3 8.3 78.6 27.4 100.6 58.6 4.1 5.8 3.2 13.8-2.1 18.7-16.2 14.7-69 67.9-69 132.7 0 5.6.2 11.2.6 16.6.3 3.9-1.1 7.8-3.8 10.7-2.7 2.9-6.5 4.5-10.4 4.5H205.2c-3.9 0-7.7-1.6-10.4-4.5s-4-6.8-3.8-10.7c.4-5.5.6-11 .6-16.6.1-64.9-52.8-118-68.9-132.7zm275.7 178H126.9c-7.9 0-14.2 6.3-14.2 14.2v26.5c0 7.9 6.3 14.2 14.2 14.2h271.6c7.8 0 14.2-6.4 14.2-14.2V513c0-7.9-6.4-14.2-14.3-14.2zm0 0"/>
                </svg>
                <div class="c-user__text">
                    Bookings
                </div>
                <? if (!empty($this->params['profile']->newBookingsCount)) { ?>
                  
                <? } ?>
            </a>
			 <a href="<?= Url::to(['/profile/bookings','notactive'=>1]) ?>" class="c-user__controller ">
                <div class="c-user__text">
                    Just Booked - Awaiting Voucher
                </div>
                <? if (!empty($this->params['profile']->newBookingsCount)) { ?>
                    <div class="c-user__counter c-user__counter--small">
                        <?= $this->params['profile']->newBookingsCount ?>
                        <div class="c-user__counterBg"></div>
                    </div>
                <? } ?>
            </a>
			<a href="<?= Url::to(['/profile/bookings','confirmed'=>1]) ?>" class="c-user__controller ">
                <div class="c-user__text">
                    Confirmed Bookings
				</div>
                <? if (!empty($this->params['profile']->newBookingsCount)) { ?>
                    <div class="c-user__counter c-user__counter--small">
                        <?= $this->params['profile']->newBookingsCount ?>
                        <div class="c-user__counterBg"></div>
                    </div>
                <? } ?>
            </a>
			<a href="<?= Url::to(['/profile/bookings','today'=>1]) ?>" class="c-user__controller ">
                <div class="c-user__text">
                    Realised Bookings
				</div>
                <? if (!empty($this->params['profile']->newBookingsCount)) { ?>
                    <div class="c-user__counter c-user__counter--small">
                        <?= $this->params['profile']->newBookingsCount ?>
                        <div class="c-user__counterBg"></div>
                    </div>
                <? } ?>
            </a>
			<a href="<?= Url::to(['/profile/bookings','cancelByMe'=>1]) ?>" class="c-user__controller ">
                <div class="c-user__text">
                    Cancelled by me Bookings
				</div>
                <? if (!empty($this->params['profile']->newBookingsCount)) { ?>
                    <div class="c-user__counter c-user__counter--small">
                        <?= $this->params['profile']->newBookingsCount ?>
                        <div class="c-user__counterBg"></div>
                    </div>
                <? } ?>
            </a>
			<a href="<?= Url::to(['/profile/bookings','cancelBySup'=>1]) ?>" class="c-user__controller ">
                <div class="c-user__text">
                    Cancelled by Supplier Bookings
				</div>
                <? if (!empty($this->params['profile']->newBookingsCount)) { ?>
                    <div class="c-user__counter c-user__counter--small">
                        <?= $this->params['profile']->newBookingsCount ?>
                        <div class="c-user__counterBg"></div>
                    </div>
                <? } ?>
            </a>
			
            <a href="javascript:void(0);" class="c-user__controller caption_settings">
                <svg class="c-user__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 386 348">
                    <path d="M338.7 195.6H60.1c-2.9 0-5.4 1.1-7.5 3.2-2.1 2.1-3.2 4.6-3.2 7.5v21.4c0 2.9 1.1 5.4 3.2 7.5 2.1 2.1 4.6 3.2 7.5 3.2h278.6c2.9 0 5.4-1.1 7.5-3.2 2.1-2.1 3.2-4.6 3.2-7.5v-21.4c0-2.9-1.1-5.4-3.2-7.5-2.1-2.1-4.6-3.2-7.5-3.2zm0 64.3H60.1c-2.9 0-5.4 1.1-7.5 3.2-2.1 2.1-3.2 4.6-3.2 7.5V292c0 2.9 1.1 5.4 3.2 7.5 2.1 2.1 4.6 3.2 7.5 3.2h278.6c2.9 0 5.4-1.1 7.5-3.2 2.1-2.1 3.2-4.6 3.2-7.5v-21.4c0-2.9-1.1-5.4-3.2-7.5-2.1-2.2-4.6-3.2-7.5-3.2zm7.5-189.7c-2.1-2.1-4.6-3.2-7.5-3.2H60.1c-2.9 0-5.4 1.1-7.5 3.2-2.1 2.1-3.2 4.6-3.2 7.5v21.4c0 2.9 1.1 5.4 3.2 7.5 2.1 2.1 4.6 3.2 7.5 3.2h278.6c2.9 0 5.4-1.1 7.5-3.2 2.1-2.1 3.2-4.6 3.2-7.5V77.7c0-2.9-1.1-5.4-3.2-7.5zm-7.5 61.1H60.1c-2.9 0-5.4 1.1-7.5 3.2-2.1 2.1-3.2 4.6-3.2 7.5v21.4c0 2.9 1.1 5.4 3.2 7.5 2.1 2.1 4.6 3.2 7.5 3.2h278.6c2.9 0 5.4-1.1 7.5-3.2 2.1-2.1 3.2-4.6 3.2-7.5V142c0-2.9-1.1-5.4-3.2-7.5-2.1-2.1-4.6-3.2-7.5-3.2zm0 0"/>
                </svg>
                <div class="c-user__text">
                    Reviews
                </div>
            </a>
			<a href="<?= Url::to(['/profile/reviews','todo'=>1]) ?>" class="c-user__controller ">
                <div class="c-user__text">
                    Reviews to do
                </div>
            </a>
			<a href="<?= Url::to(['/profile/reviews','todo'=>0]) ?>" class="c-user__controller ">
                <div class="c-user__text">
                    Reviews I left
                </div>
            </a>
			
			<!-- Blog -->
			<a href="javascript:void(0);" class="c-user__controller caption_settings">
                <svg class="c-user__icon" xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 51.997 51.997">
                    <path d="M51.911 16.242c-.759-8.354-6.672-14.415-14.072-14.415-4.93 0-9.444 2.653-11.984 6.905-2.517-4.307-6.846-6.906-11.697-6.906C6.759 1.826.845 7.887.087 16.241c-.06.369-.306 2.311.442 5.478 1.078 4.568 3.568 8.723 7.199 12.013l18.115 16.439 18.426-16.438c3.631-3.291 6.121-7.445 7.199-12.014.748-3.166.502-5.108.443-5.477z"/>
                </svg>

                <div class="c-user__text" >My Travell Blog</div>
            </a>
			<a href="javascript:void(0);" class="c-user__controller ">
                <div class="c-user__text" >Write New Story</div>
            </a>
			<a href="javascript:void(0);" class="c-user__controller ">
                <div class="c-user__text" >My Stories</div>
            </a>
			<a href="javascript:void(0);" class="c-user__controller ">
                <div class="c-user__text" >My Photos</div>
            </a>
			<a href="javascript:void(0);" class="c-user__controller ">
                <div class="c-user__text" >Visit My Blog</div>
            </a>
			<a href="javascript:void(0);" class="c-user__controller ">
                <div class="c-user__text" >Blog Settings</div>
            </a>
			<!-- end blog -->
            <a href="javascript:void(0);" class="c-user__controller caption_settings">
                <svg class="c-user__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 268.765 268.765">
                    <path d="M267.92 119.461c-.425-3.778-4.83-6.617-8.639-6.617-12.315 0-23.243-7.231-27.826-18.414-4.682-11.454-1.663-24.812 7.515-33.231 2.889-2.641 3.24-7.062.817-10.133-6.303-8.004-13.467-15.234-21.289-21.5-3.063-2.458-7.557-2.116-10.213.825-8.01 8.871-22.398 12.168-33.516 7.529-11.57-4.867-18.866-16.591-18.152-29.176.235-3.953-2.654-7.39-6.595-7.849-10.038-1.161-20.164-1.197-30.232-.08-3.896.43-6.785 3.786-6.654 7.689.438 12.461-6.946 23.98-18.401 28.672-10.985 4.487-25.272 1.218-33.266-7.574-2.642-2.896-7.063-3.252-10.141-.853-8.054 6.319-15.379 13.555-21.74 21.493-2.481 3.086-2.116 7.559.802 10.214 9.353 8.47 12.373 21.944 7.514 33.53-4.639 11.046-16.109 18.165-29.24 18.165-4.261-.137-7.296 2.723-7.762 6.597-1.182 10.096-1.196 20.383-.058 30.561.422 3.794 4.961 6.608 8.812 6.608 11.702-.299 22.937 6.946 27.65 18.415 4.698 11.454 1.678 24.804-7.514 33.23-2.875 2.641-3.24 7.055-.817 10.126 6.244 7.953 13.409 15.19 21.259 21.508 3.079 2.481 7.559 2.131 10.228-.81 8.04-8.893 22.427-12.184 33.501-7.536 11.599 4.852 18.895 16.575 18.181 29.167-.233 3.955 2.67 7.398 6.595 7.85 5.135.599 10.301.898 15.481.898 4.917 0 9.835-.27 14.752-.817 3.897-.43 6.784-3.786 6.653-7.696-.451-12.454 6.946-23.973 18.386-28.657 11.059-4.517 25.286-1.211 33.281 7.572 2.657 2.89 7.047 3.239 10.142.848 8.039-6.304 15.349-13.534 21.74-21.494 2.48-3.079 2.13-7.559-.803-10.213-9.353-8.47-12.388-21.946-7.529-33.524 4.568-10.899 15.612-18.217 27.491-18.217l1.662.043c3.853.313 7.398-2.655 7.865-6.588 1.184-10.105 1.198-20.383.06-30.561zm-133.325 60.03c-24.718 0-44.824-20.106-44.824-44.824 0-24.717 20.106-44.824 44.824-44.824 24.717 0 44.823 20.107 44.823 44.824 0 24.718-20.106 44.824-44.823 44.824z" clip-rule="evenodd"/>
                </svg>
                <div class="c-user__text">
                    Account Settings
                </div>
            </a>
			<a href="<?= Url::to(['/user/settings','setting'=>1]) ?>" class="c-user__controller ">
                <div class="c-user__text">
                   Personal Information
                </div>
            </a>
			<a href="<?= Url::to(['/user/settings','setting'=>0]) ?>" class="c-user__controller ">
                <div class="c-user__text">
                   Change Password
                </div>
            </a>
			
            <?= Html::beginTag('a', ['class' => 'c-user__controller log_out', 'href' => Url::to('/user/logout'), 'data-method' => 'post']) ?>
                <svg class="c-user__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 896 896">
                    <path d="M884.927 356.016L738.364 209.453c-14.656-14.656-65.078-14.656-65.078 15.861v335.562c0 30.627 50.422 30.627 65.078 15.859L884.927 430.28c14.763-14.764 14.763-59.608 0-74.264zM448 0H0v672l336 224V784h112c61.795 0 112.002-50.203 112.002-112V112C560.003 50.094 509.794 0 448 0zm-.042 672.002h-112v-448L167.958 112h280v560.002z"/>
                </svg>
                <div class="c-user__text ">
                    Log out
                </div>
            <?= Html::endTag('a')?>
        </div>
	</div>
</div>