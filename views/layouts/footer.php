<?php
use yii\helpers\Url;
use yii\helpers\Html;

?>
<footer class="l-footer">
    <div class="l-footer__body">
        <div class="l-mainContent">
            <div class="l-footer__list list">
                <div class="l-footer__item item">
                    <div class="c-footer">
                        <div class="c-footer__head">24/7 Support</div>
                        <div class="c-footer__content">
                            <?= Html::a('E-mail or Call Us', Url::to(['/site/page', 'page' => 'email','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                            <?= Html::a('Frequently Asked Questions (FAQ)', Url::to(['/site/page', 'page' => 'faq','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                            <?= Html::a('Privacy Policy', Url::to(['/site/page', 'page' => 'privacy_policy','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                            <?= Html::a('Use of Cookies', Url::to(['/site/page', 'page' => 'cookies','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                            <?= Html::a('Cancellation Policy', Url::to(['/site/page', 'page' => 'cancellation_policy','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                            <?= Html::a('Low Price Guarantee', Url::to(['/site/page', 'page' => 'guarantee','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                            <?= Html::a('Terms & Contditions', Url::to(['/site/page', 'page' => 'terms_conditions','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                            <?php if (Yii::$app->user->isGuest) : ?>
                                <?= Html::a('Log-In', Url::to(['/user/login','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                                <?= Html::a('Sign-Up', Url::to(['/user/register','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="l-footer__item item">
                    <div class="c-footer">
                        <div class="c-footer__head">Destinations</div>
                        <div class="c-footer__content">
                            <? foreach ($this->params['continents'] as $continent) { ?>
                                <?= Html::a($continent, ['/tour/index', 'TourSearch' => ['continent' => $continent],'currency'=>$currency['id']], ['class' => 'c-footer__link']) ?>
                            <? } ?>
                            <?= Html::a('All Destinations', Url::to(['/tour/index','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                        </div>
                    </div>
                </div>
                <div class="l-footer__item item">
                    <div class="c-footer">
                        <div class="c-footer__head">Trips, tours & activities</div>
                        <div class="c-footer__content">
                            <? foreach ($this->params['tourTypes'] as $typeId => $tourType) { ?>
                                <?= Html::a($tourType, ['/tour/index', 'TourSearch' => ['typeId' => $typeId],'currency'=>$currency['id']], ['class' => 'c-footer__link']) ?>
                            <? } ?>
                            <?= Html::a('All the Trips, Tours & Activities', Url::to(['/tour/index','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                        </div>
                    </div>
                </div>
                <!--<div class="l-footer__item item"> 
                    <div class="c-footer">
                        <div class="c-footer__head">Accommodations</div>
                        <div class="c-footer__content">
                            <a class="c-footer__link" href='#'>Apartments</a>
                            <a class="c-footer__link" href='#'>Villas</a>
                            <a class="c-footer__link" href='#'>Bed & Breakfast</a>
                            <a class="c-footer__link" href='#'>Hotels</a>
                            <a class="c-footer__link" href='#'>Hostels</a>
                            <a class="c-footer__link" href='#'>Design Hotels</a>
                            <a class="c-footer__link" href='#'>Guesthouses</a>
                            <a class="c-footer__link" href='#'>Castles & Palaces</a>
                            <a class="c-footer__link" href='#'>Motels</a>
                            <a class="c-footer__link" href='#'>Penthouses</a>
                            <a class="c-footer__link" href='#'>Farmhouses & Eco Hotels</a>
                            <a class="c-footer__link" href='#'>RVs, Motorhomes</a>
                            <a class="c-footer__link" href='#'>All Accommodations</a>
                        </div>
                    </div>
                </div>
                <div class="l-footer__item item">
                    <div class="c-footer">
                        <div class="c-footer__head">Rental & services</div>
                        <div class="c-footer__content">
                            <a class="c-footer__link" href='#'>Rent a Car</a>
                            <a class="c-footer__link" href='#'>Rent a Car with Driver</a>
                            <a class="c-footer__link" href='#'>Rent a Motor Bike or Scooter</a>
                            <a class="c-footer__link" href='#'>Rent a Bike, MTB, Electric Bike</a>
                            <a class="c-footer__link" href='#'>Rent a Kayak or Canoe</a>
                            <a class="c-footer__link" href='#'>Rent a Yacht</a>
                            <a class="c-footer__link" href='#'>Rent a Motor Boat</a>
                            <a class="c-footer__link" href='#'>Rent a Surfing Equipment</a>
                            <a class="c-footer__link" href='#'>Rent a Diving Equipment</a>
                            <a class="c-footer__link" href='#'>Rent a Camp Equipment</a>
                            <a class="c-footer__link" href='#'>Home Help Services</a>
                            <a class="c-footer__link" href='#'>Holiday Childcare Services</a>
                            <a class="c-footer__link" href='#'>All the Rentals & Services</a>
                        </div>
                    </div>
                </div>-->
                <div class="l-footer__item item">
                    <!--<div class="c-footer">
                        <div class="c-footer__head">Tripspoint team</div>
                        <div class="c-footer__content">
                            <a class="c-footer__link" href='#'>About Us</a>
                            <a class="c-footer__link" href='#'>Why Book with us</a>
                            <a class="c-footer__link" href='#'>Our Team</a>
                            <a class="c-footer__link" href='#'>Join our TeamCarriers</a>
                            <a class="c-footer__link" href='#'>Destination Manager Sign-Up</a>
                            <a class="c-footer__link" href='#'>Destination Manager Log-In</a>
                        </div>
                    </div>-->
                    <div class="c-footer">
                        <div class="c-footer__head">Become a supplier</div>
                        <div class="c-footer__content">
                            <?= Html::a('Who can be our Supplier', Url::to(['/supplier/index','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                            <?= Html::a('Supplier Sign-Up', Url::to(['/supplier/create','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                            <?= Html::a('Supplier Log-In', Url::to(['/user/login','currency'=>$currency['id']]), ['class' => 'c-footer__link']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="l-footer__footer">
        <div class="l-mainContent cf">
            <div class="l-footer__logoPart">
                <div class="c-logo">
                    <a href="<?= Url::to(['/site/index','currency'=>$currency['id']]) ?>" class="c-logo__link c-logo__link--footer">
                        <img class="c-logo__img c-logo__img--footer" src="/img/footer/logo--footer.png" alt="">
                    </a>
                </div>
            </div>
            <div class="l-footer__socialPart">
                <div class="l-footer__social item">
                    <div class="c-social">
                        <a class="c-social__link" href="#">
                            <svg class="c-social__icon c-social__icon--pinterest" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 310.05 310.05">
                                <path d="M245.265 31.772C223.923 11.284 194.388 0 162.101 0c-49.32 0-79.654 20.217-96.416 37.176-20.658 20.9-32.504 48.651-32.504 76.139 0 34.513 14.436 61.003 38.611 70.858 1.623.665 3.256 1 4.857 1 5.1 0 9.141-3.337 10.541-8.69.816-3.071 2.707-10.647 3.529-13.936 1.76-6.495.338-9.619-3.5-14.142-6.992-8.273-10.248-18.056-10.248-30.788 0-37.818 28.16-78.011 80.352-78.011 41.412 0 67.137 23.537 67.137 61.425 0 23.909-5.15 46.051-14.504 62.35-6.5 11.325-17.93 24.825-35.477 24.825-7.588 0-14.404-3.117-18.705-8.551-4.063-5.137-5.402-11.773-3.768-18.689 1.846-7.814 4.363-15.965 6.799-23.845 4.443-14.392 8.643-27.985 8.643-38.83 0-18.55-11.404-31.014-28.375-31.014-21.568 0-38.465 21.906-38.465 49.871 0 13.715 3.645 23.973 5.295 27.912-2.717 11.512-18.865 79.953-21.928 92.859-1.771 7.534-12.44 67.039 5.219 71.784 19.841 5.331 37.576-52.623 39.381-59.172 1.463-5.326 6.582-25.465 9.719-37.845 9.578 9.226 25 15.463 40.006 15.463 28.289 0 53.73-12.73 71.637-35.843 17.367-22.418 26.932-53.664 26.932-87.978 0-26.826-11.52-53.272-31.604-72.556z"/>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="l-footer__social item">
                    <div class="c-social">
                        <a class="c-social__link" href="#">
                            <svg class="c-social__icon c-social__icon--youTube" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96.875 96.875 ">
                                <path d="M95.201 25.538c-1.186-5.152-5.4-8.953-10.473-9.52-12.013-1.341-24.172-1.348-36.275-1.341-12.105-.007-24.266 0-36.279 1.341-5.07.567-9.281 4.368-10.467 9.52C.019 32.875 0 40.884 0 48.438 0 55.992 0 64 1.688 71.336c1.184 5.151 5.396 8.952 10.469 9.52 12.012 1.342 24.172 1.349 36.277 1.342 12.107.007 24.264 0 36.275-1.342 5.07-.567 9.285-4.368 10.471-9.52 1.689-7.337 1.695-15.345 1.695-22.898 0-7.554.014-15.563-1.674-22.9zM35.936 63.474V31.437c10.267 5.357 20.466 10.678 30.798 16.068-10.3 5.342-20.504 10.631-30.798 15.969z"/>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="l-footer__social item">
                    <div class="c-social">
                        <a class="c-social__link" href="#">
                            <svg class="c-social__icon c-social__icon--googlePlus" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 491.858 491.858">
                                <path d="M377.472 224.957H201.319v58.718H308.79c-16.032 51.048-63.714 88.077-120.055 88.077-69.492 0-125.823-56.335-125.823-125.824 0-69.492 56.333-125.823 125.823-125.823 34.994 0 66.645 14.289 89.452 37.346l42.622-46.328c-34.04-33.355-80.65-53.929-132.074-53.929C84.5 57.193 0 141.693 0 245.928s84.5 188.737 188.736 188.737c91.307 0 171.248-64.844 188.737-150.989v-58.718l-.001-.001zM491.858 224.857h-36.031v-36.031h-30.886v36.031H388.91v30.883h36.031v36.032h30.886V255.74h36.031"/>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="l-footer__social item">
                    <div class="c-social">
                        <a class="c-social__link" href="#">
                            <svg class="c-social__icon c-social__icon--facebook" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 90 ">
                                <path d="M90 15.001C90 7.119 82.884 0 75 0H15C7.116 0 0 7.119 0 15.001v59.998C0 82.881 7.116 90 15.001 90H45V56H34V41h11v-5.844C45 25.077 52.568 16 61.875 16H74v15H61.875C60.548 31 59 32.611 59 35.024V41h15v15H59v34h16c7.884 0 15-7.119 15-15.001V15.001z "/>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="l-footer__social item">
                    <div class="c-social">
                        <a class="c-social__link" href="#">
                            <svg class="c-social__icon c-social__icon--twitter" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612 ">
                                <path d="M612 116.258c-22.525 9.981-46.694 16.75-72.088 19.772 25.929-15.527 45.777-40.155 55.184-69.411-24.322 14.379-51.169 24.82-79.775 30.48-22.907-24.437-55.49-39.658-91.63-39.658-69.334 0-125.551 56.217-125.551 125.513 0 9.828 1.109 19.427 3.251 28.606-104.326-5.24-196.835-55.223-258.75-131.174-10.823 18.51-16.98 40.078-16.98 63.101 0 43.559 22.181 81.993 55.835 104.479-20.575-.688-39.926-6.348-56.867-15.756v1.568c0 60.806 43.291 111.554 100.693 123.104-10.517 2.83-21.607 4.398-33.08 4.398-8.107 0-15.947-.803-23.634-2.333 15.985 49.907 62.336 86.199 117.253 87.194-42.947 33.654-97.099 53.655-155.916 53.655-10.134 0-20.116-.612-29.944-1.721 55.567 35.681 121.536 56.485 192.438 56.485 230.948 0 357.188-191.291 357.188-357.188l-.421-16.253c24.666-17.593 46.005-39.697 62.794-64.861z "/>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="l-footer__social item">
                    <div class="c-social">
                        <a class="c-social__link" href="#">
                            <svg class="c-social__icon c-social__icon--instagram" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 510">
                                <path d="M459 0H51C22.95 0 0 22.95 0 51v408c0 28.05 22.95 51 51 51h408c28.05 0 51-22.95 51-51V51c0-28.05-22.95-51-51-51zM255 153c56.1 0 102 45.9 102 102s-45.9 102-102 102-102-45.9-102-102 45.9-102 102-102zM63.75 459C56.1 459 51 453.9 51 446.25V229.5h53.55C102 237.15 102 247.35 102 255c0 84.15 68.85 153 153 153s153-68.85 153-153c0-7.65 0-17.85-2.55-25.5H459v216.75c0 7.65-5.1 12.75-12.75 12.75H63.75zM459 114.75c0 7.65-5.1 12.75-12.75 12.75h-51c-7.65 0-12.75-5.1-12.75-12.75v-51c0-7.65 5.1-12.75 12.75-12.75h51C453.9 51 459 56.1 459 63.75v51z"/>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="l-footer__social item">
                    <div class="c-social">
                        <a class="c-social__link" href="#">
                            <svg class="c-social__icon c-social__icon--vimeo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 430.118 430.118 ">
                                <path d="M367.243 28.754c-59.795-1.951-100.259 31.591-121.447 100.664 10.912-4.494 21.516-6.762 31.858-6.762 21.804 0 31.455 12.237 28.879 36.776-1.278 14.86-10.911 36.482-28.879 64.858-18.039 28.423-31.513 42.61-40.464 42.61-11.621 0-22.199-21.958-31.857-65.82-3.239-12.918-9.031-45.812-17.324-98.765-7.775-49.046-28.32-71.962-61.727-68.741-14.132 1.299-35.302 14.241-63.556 38.734C42.113 91.032 21.228 109.761 0 128.471l20.225 26.112c19.303-13.562 30.595-20.311 33.731-20.311 14.802 0 28.625 23.219 41.488 69.651 11.53 42.644 23.158 85.23 34.744 127.812 17.256 46.466 38.529 69.708 63.552 69.708 40.473 0 90.028-38.065 148.469-114.223 56.537-72.909 85.725-130.352 87.694-172.341 2.595-56.115-18.29-84.851-62.66-86.125z "/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>