<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $newReview app\models\Reviews */
/* @var $reviewPhotos app\models\TourPhotos */
/* @var $tourRating app\models\Ratings */
/* @var $form yii\widgets\ActiveForm */
/* @var $tourId app\models\Orders->tourId */
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'c-reviews cf',
        'style' => 'display:none'
    ],
]) ?>
    <div class="c-reviews__leftSide">
        <?= $form->field($tourRating, 'userId')->hiddenInput(['value' => Yii::$app->user->identity->getId()])->label(false) ?>
        <?= $form->field($tourRating, 'tourId')->hiddenInput(['value' => $tourId])->label(false) ?>
        <?= $form->field($tourRating, 'value')->widget(StarRating::classname(), [
            'options' => [
                'id' => "ratings-value-$tourId"
            ],
            'pluginOptions' => [
                'showClear' => false,
                'emptyStar' => '<img class="c-rating__icon" src="/img/icon/starGrey.svg">',
                'filledStar' => '<img class="c-rating__icon" src="/img/icon/starLightBlue.svg">',
                'step' => 1,
            ]
        ])->label(false) ?>
        
        <?= $form->field($newReview, 'userId')->hiddenInput(['value' => Yii::$app->user->identity->getId()])->label(false) ?>
        <?= $form->field($newReview, 'tourId')->hiddenInput(['value' => $tourId])->label(false) ?>
        <?= $form->field($newReview, 'title')->textarea(['class' => 'c-reviews__input', 'maxlength' => true, 'placeholder' => 'Enter the title of your review'])->label(false) ?>
        <?= $form->field($newReview, 'text')->textarea(['class' => 'c-reviews__input c-reviews__textarea', 'maxlength' => true, 'placeholder' => 'Share your impression'])->label(false) ?>
    </div>
    <div class="c-reviews__rightSide">
        <div class="c-reviews__imageTitle">
            Upload pictures you took
        </div>
        <div class="c-reviews__imageTooltip">
            (max 15 pictures)
        </div>
        <div class="c-reviews__uploadWrapper">
            <?= $form->field($reviewPhotos, 'images[]')->widget(FileInput::classname(), [
                'options' => [
                    'id' => "tourphotos-images-$tourId",
                    'accept' => 'image/*',
                    'multiple' => true
                ],
                'pluginOptions' => [
                    'allowedFileExtensions' => ['jpg'],
                    'showCaption' => false,
                    'showUpload' => false,
                    'showCancel' => false,
                    'previewClass' => 'reviewsPictures',
                    'frameClass' => 'reviewsPictures__frame',
                    'fileActionSettings' => [
                        'showZoom' => false,
                        'indicatorNew' => '',
                    ],
                    'previewSettings' => [
                        'image' => [
                            'width' => '98px',
                            'height' => '70px',
                        ],
                    ],
                    'layoutTemplates' => [
                        // hide modal window (for file content preview zooming)
                        'modalMain' => '',
                        'footer' => '
                            <div class="reviewsPictures__footer">
                            <div class="reviewsPictures__name" style="width:{width}">{caption}</div>
                            <div class="reviewsPictures__size">{size}</div>
                            </div>',
                        // custom template for the "browse" button
                        'btnBrowse' => '
                            <button class="c-reviews__linkLabel btn-file">
                                <svg class="c-reviews__iconAttach" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                    <path d="m14.754,60.041c3.269,2.399 6.476,3.607 9.567,3.607 0.77,0 1.532-0.075 2.287-0.226 5.39-1.072 8.216-5.645 8.233-5.69l15.922-21.676c0.66-0.897 0.467-2.161-0.432-2.821s-2.16-0.466-2.821,0.433l-16.021,21.825c-0.019,0.031-2.06,3.254-5.669,3.972-2.628,0.527-5.548-0.378-8.678-2.677-9.174-6.737-4.389-13.862-3.811-14.656l26.347-35.899c0.012-0.016 1.131-1.557 3.168-1.91 1.865-0.325 4.009,0.392 6.427,2.167 0.942,0.652 3.476,2.885 3.886,5.34 0.2,1.194-0.116,2.337-0.967,3.495l-23.538,32.048c-0.01,0.014-1.062,1.432-2.613,1.689-0.98,0.158-1.99-0.161-3.105-0.979-3.312-2.434-1.593-5.382-1.255-5.89l13.118-17.861c0.659-0.897 0.467-2.161-0.432-2.821-0.899-0.66-2.162-0.468-2.822,0.433l-13.149,17.903c-1.832,2.638-2.77,7.874 2.148,11.488 2.011,1.476 4.078,2.047 6.156,1.708 3.204-0.53 5.061-3.072 5.233-3.321l23.511-32.008c1.512-2.059 2.082-4.271 1.689-6.573-0.791-4.654-5.383-7.844-5.523-7.94-3.286-2.412-6.465-3.37-9.457-2.855-3.715,0.646-5.617,3.328-5.775,3.565l-26.29,35.821c-0.131,0.175-3.201,4.368-2.381,9.863 0.586,3.927 2.957,7.442 7.047,10.446z"></path>
                                </svg>
                                Upload pictures
                            </button>',
                        // custom template for the "remove" button
                        'btnDefault' => '
                            <button class="c-reviews__linkLabel fileinput-remove fileinput-remove-button">
                                <svg class="c-reviews__iconAttach" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                                    <path d="M25.834,10.825H6.165c-2.133,0-3.868-1.736-3.868-3.869s1.735-3.869,3.868-3.869h19.669c2.134,0,3.869,1.736,3.869,3.869
                                    S27.968,10.825,25.834,10.825z M6.165,5.087c-1.03,0-1.868,0.838-1.868,1.869s0.838,1.869,1.868,1.869h19.669
                                    c1.03,0,1.869-0.838,1.869-1.869s-0.839-1.869-1.869-1.869H6.165z"/>
                                    <path d="M22.867,32H9.132c-1.431,0-2.615-1.073-2.757-2.496L4.417,9.925l1.99-0.199l1.958,19.58C8.404,29.702,8.734,30,9.132,30
                                    h13.735c0.397,0,0.728-0.299,0.768-0.694l1.957-19.58l1.99,0.199l-1.957,19.58C25.482,30.927,24.297,32,22.867,32z"/>
                                    <path d="M20.235,4.027c-0.553,0-1-0.448-1-1C19.235,2.461,18.774,2,18.208,2h-4.417c-0.566,0-1.027,0.461-1.027,1.027
                                    c0,0.552-0.447,1-1,1s-1-0.448-1-1C10.764,1.358,12.122,0,13.791,0h4.417c1.669,0,3.027,1.358,3.027,3.027
                                    C21.235,3.58,20.788,4.027,20.235,4.027z"/>
                                </svg>
                                Remove
                            </button>',
                    ],
                ],
            ])->label(false) ?>
        </div>
        <?= Html::submitButton('+ Add review', ['class' => 'c-reviews__button']) ?>
    </div>
<?php ActiveForm::end(); ?>
