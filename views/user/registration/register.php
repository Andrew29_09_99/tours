
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
//use yii\authclient\widgets\AuthChoice;
use app\components\AuthChoice;

/**
 * @var yii\web\View              $this
 * @var yii\widgets\ActiveForm    $form
 * @var dektrium\user\models\User $user
 */
$this->registerCssFile('/css/signUp.css');
$this->title = Yii::t('user', 'Sign up');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="l-action--empty"></div>
<div class="l-tools">
    <div class="l-tools__header  l-mainContent">
        <? if (\Yii::$app->session->hasFlash('info')) { ?>
            <p class="c-signUp__text"><?= \Yii::$app->session->getFlash('info') ?></p>
            <?= Html::a('Back to main', ['/site/index'], ['class' => 'c-button']) ?>
        <? } else { ?>
            <div class="l-tools__form">
                <div class="c-signUp">
                    <div class="c-signUp__header">
                        <div class="c-signUp__question">Have an account?</div>
                        <div class="c-signUp__question"><a class="c-signUp__question--span" href="<?=Url::to(['/user/login'])?>">Log In!</a>
                        </div>
                        <div class="c-signUp__body">
                            <?php $form = ActiveForm::begin([
                                'id' => 'registration-form',
                                'options' => ['class' => 'c-signUp__form'],
                                'fieldConfig' => [
                                    'template' => "<div>{input}</div><div class='help-block'>{error}\n{hint}</div>",
                                ],
                            ]); ?>
                            <div class="c-signUp__inputBlock">
                                <?= $form->field($model, 'username')->HiddenInput(['value' => '', 'class' => 'c-signUp__input']) ?>
                                <svg class="c-signUp__icon"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11.8 14"  xml:space="preserve">
                                    <path d="M9.1,3.2c0,1.8-1.5,3.2-3.2,3.2S2.6,5,2.6,3.2S4.1,0,5.9,0S9.1,1.5,9.1,3.2L9.1,3.2z M9.1,3.2"/>
                                    <path d="M5.9,8.1C2.6,8.1,0,10.8,0,14h11.8C11.8,10.8,9.1,8.1,5.9,8.1L5.9,8.1z M5.9,8.1"/>
                                </svg>
                            </div>
                            <div class="c-signUp__inputBlock">
                                <?= $form->field($model, 'firstname')->HiddenInput(['value' => '', 'class' => 'c-signUp__input']) ?>
                                <svg class="c-signUp__icon"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11.8 14"  xml:space="preserve">
                                    <path d="M9.1,3.2c0,1.8-1.5,3.2-3.2,3.2S2.6,5,2.6,3.2S4.1,0,5.9,0S9.1,1.5,9.1,3.2L9.1,3.2z M9.1,3.2"/>
                                    <path d="M5.9,8.1C2.6,8.1,0,10.8,0,14h11.8C11.8,10.8,9.1,8.1,5.9,8.1L5.9,8.1z M5.9,8.1"/>
                                </svg>
                            </div>
                            <div class="c-signUp__inputBlock">
                                <?= $form->field($model, 'lastname')->HiddenInput(['value' => '', 'class' => 'c-signUp__input']) ?>
                                <svg class="c-signUp__icon"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11.8 14"  xml:space="preserve">
                                    <path d="M9.1,3.2c0,1.8-1.5,3.2-3.2,3.2S2.6,5,2.6,3.2S4.1,0,5.9,0S9.1,1.5,9.1,3.2L9.1,3.2z M9.1,3.2"/>
                                    <path d="M5.9,8.1C2.6,8.1,0,10.8,0,14h11.8C11.8,10.8,9.1,8.1,5.9,8.1L5.9,8.1z M5.9,8.1"/>
                                </svg>
                            </div>
                            <div class="c-signUp__inputBlock">
                                <?= $form->field($model, 'email')->input('email', ['placeholder' => 'E-mail', 'class' => 'c-signUp__input']) ?>
                                <svg class="c-signUp__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">
                                    <path d="M7 9L5.268 7.484.316 11.729c.18.167.423.271.691.271h11.986c.267 0 .509-.104.688-.271L8.732 7.484 7 9z"/>
                                    <path d="M13.684 2.271c-.18-.168-.422-.271-.691-.271H1.007c-.267 0-.509.104-.689.273L7 8l6.684-5.729zM0 2.878v8.308l4.833-4.107m4.334 0L14 11.186V2.875"/>
                                </svg>
                            </div>
                            <div class="c-signUp__inputBlock">
                                <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password', 'class' => 'c-signUp__input']) ?>
                                <svg class="c-signUp__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 486.733 486.733">
                                    <path d="M403.88 196.563h-9.484v-44.388c0-82.099-65.151-150.681-146.582-152.145-2.225-.04-6.671-.04-8.895 0C157.486 1.494 92.336 70.076 92.336 152.175v44.388h-9.485c-14.616 0-26.538 15.082-26.538 33.709v222.632c0 18.606 11.922 33.829 26.539 33.829H403.88c14.616 0 26.539-15.223 26.539-33.829V230.272c0-18.626-11.922-33.709-26.539-33.709zM273.442 341.362v67.271c0 7.703-6.449 14.222-14.158 14.222H227.45c-7.71 0-14.159-6.519-14.159-14.222v-67.271c-7.477-7.36-11.83-17.537-11.83-28.795 0-21.334 16.491-39.666 37.459-40.513 2.222-.09 6.673-.09 8.895 0 20.968.847 37.459 19.179 37.459 40.513-.002 11.258-4.355 21.435-11.832 28.795zm58.444-144.799H154.847v-44.388c0-48.905 39.744-89.342 88.519-89.342 48.775 0 88.521 40.437 88.521 89.342v44.388z"/>
                                </svg>
                            </div>
                            <div class="c-signUp__inputBlock">
                                <?= $form->field($model, 'confirm')->passwordInput(['placeholder' => 'Confirm Password', 'class' => 'c-signUp__input']) ?>
                                <svg class="c-signUp__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 486.733 486.733">
                                    <path d="M403.88 196.563h-9.484v-44.388c0-82.099-65.151-150.681-146.582-152.145-2.225-.04-6.671-.04-8.895 0C157.486 1.494 92.336 70.076 92.336 152.175v44.388h-9.485c-14.616 0-26.538 15.082-26.538 33.709v222.632c0 18.606 11.922 33.829 26.539 33.829H403.88c14.616 0 26.539-15.223 26.539-33.829V230.272c0-18.626-11.922-33.709-26.539-33.709zM273.442 341.362v67.271c0 7.703-6.449 14.222-14.158 14.222H227.45c-7.71 0-14.159-6.519-14.159-14.222v-67.271c-7.477-7.36-11.83-17.537-11.83-28.795 0-21.334 16.491-39.666 37.459-40.513 2.222-.09 6.673-.09 8.895 0 20.968.847 37.459 19.179 37.459 40.513-.002 11.258-4.355 21.435-11.832 28.795zm58.444-144.799H154.847v-44.388c0-48.905 39.744-89.342 88.519-89.342 48.775 0 88.521 40.437 88.521 89.342v44.388z"/>
                                </svg>
                            </div>
                            <div class="c-signUp__buttonWrap">
                                <?= Html::submitButton(Yii::t('user', strtoupper('Create Account')), ['class' => 'c-button']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                            <div class="c-signUp__policyBlock">
                                <div class="c-signUp__policy">Creating an account means you're okay with our</div>
                                <a href="" class="c-signUp__policy c-signUp__policy--link" hreft="#">Terms of
                                    Service</a>
                                <div class="c-signUp__policy">and</div>
                                <a href="" class="c-signUp__policy c-signUp__policy--link" hreft="#">Privacy
                                    Statement.</a>
                            </div>
                            <div class="c-signUp__separator">or</div>

                            <!-- Віджет реєстрації через соцмережі, верстка знаходиться всередині віджета
                                 Якщо додається соцмережа у віджеті в методі clientLink() додати іконку соцмережі-->
                            <?= AuthChoice::widget([
                                'baseAuthUrl' => ['/user/security/auth']
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        <? } ?>
    </div>
</div>
