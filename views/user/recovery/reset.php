<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\RecoveryForm $model
 */

$this->title = Yii::t('user', 'Reset your password');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/resetPassword.css')

?>
<div class="l-action--empty"></div>
<div class="l-tools">
    <div class="l-tools__header  l-mainContent">
        <div class="l-tools__form">
            <div class="c-resetPassword">
                <div class="c-resetPassword__header">Reset your password</div>
                <? if (\Yii::$app->session->hasFlash('success')) { ?>
                    <p class="c-resetPassword__text"><?= \Yii::$app->session->getFlash('success') ?></p>
                    <?= Html::a('Log In', ['/user/login'], ['class' => 'c-button']) ?>
                <? } else if (\Yii::$app->session->hasFlash('danger')) { ?>
                    <p class="c-resetPassword__text"><?= \Yii::$app->session->getFlash('danger') ?></p>
                    <?= Html::a('Back to main', ['/site/index'], ['class' => 'c-button']) ?>
                <? } else { ?>
                    <div class="c-resetPassword__body">
                        <?php $form = ActiveForm::begin([
                            'id' => 'password-recovery-form',
                            'fieldConfig' => [
                                'options' => [
                                    'class' => 'c-resetPassword__form'
                                ],
                                'template' => "<div>{input}</div><div class=\"help-block\">{error}\n{hint}</div>"
                            ],
                            'enableAjaxValidation' => true,
                            //                    'enableClientValidation' => false,
                        ]); ?>
                        <div class="c-resetPassword__inputBlock">
                            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'New password', 'class' => 'c-resetPassword__input', 'autofocus' => true]) ?>
                            <svg class="c-resetPassword__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 486.733 486.733">
                                <path d="M403.88 196.563h-9.484v-44.388c0-82.099-65.151-150.681-146.582-152.145-2.225-.04-6.671-.04-8.895 0C157.486 1.494 92.336 70.076 92.336 152.175v44.388h-9.485c-14.616 0-26.538 15.082-26.538 33.709v222.632c0 18.606 11.922 33.829 26.539 33.829H403.88c14.616 0 26.539-15.223 26.539-33.829V230.272c0-18.626-11.922-33.709-26.539-33.709zM273.442 341.362v67.271c0 7.703-6.449 14.222-14.158 14.222H227.45c-7.71 0-14.159-6.519-14.159-14.222v-67.271c-7.477-7.36-11.83-17.537-11.83-28.795 0-21.334 16.491-39.666 37.459-40.513 2.222-.09 6.673-.09 8.895 0 20.968.847 37.459 19.179 37.459 40.513-.002 11.258-4.355 21.435-11.832 28.795zm58.444-144.799H154.847v-44.388c0-48.905 39.744-89.342 88.519-89.342 48.775 0 88.521 40.437 88.521 89.342v44.388z"></path>
                            </svg>
                        </div>
                        <div class="c-resetPassword__buttonWrap">
                            <?= Html::submitButton(Yii::t('user', 'Finish'), ['class' => 'c-button']) ?><br>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                <? } ?>
            </div>
        </div>
    </div>
</div>






