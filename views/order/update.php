<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\web\JsExpression;
use kartik\select2\Select2;
use app\components\Converter;
/* @var $this yii\web\View */
/* @var $orders app\models\Orders */

$this->title = 'Order';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/order.css');
$this->registerCssFile('/css/components/bootstrap-datetimepicker-standalone.css', ['depends' => kartik\date\DatePickerAsset::className()]);
$this->registerJs("var pickupPoints = $tourPickupPoints; var guideDates = [];", $this::POS_HEAD);
$this->registerJsFile('/js/order.js');
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyB5sXmmhv29RSYXsyobgNq4gks1OT-zLr0&libraries=places&language=en');
$this->registerJsFile('/js/googleMaps/infobox.js');
?>

<div class="l-action l-action--textBlock">
    <div class="l-mainContent">
        <div class="l-action__content--textBlock">
            <div class="l-action__textBlock">
                <p class="l-action__text--white l-action__text--bold">
                    Order!
                </p>
            </div>
        </div>
    </div>
</div>
<!--<div class="l-tools">
    <div class="l-tools__content l-mainContent cf">
        <div class="l-tools__leftPart">
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Main
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Order
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>-->
<div class="l-mainContent cf">
    <div class="c-fakeSteps">
        <ul class="c-fakeSteps__list">
            <li class="c-fakeSteps__item">
                <span class="c-fakeSteps__text active">
                    Step 1
                </span>
            </li>
            <li class="c-fakeSteps__item">
                <span class="c-fakeSteps__text">
                    Step 2
                </span>
            </li>
            <li class="c-fakeSteps__item">
                <svg class="c-fakeSteps__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 426.667 426.667">
                    <path d="M213.333,0C95.518,0,0,95.514,0,213.333s95.518,213.333,213.333,213.333
	c117.828,0,213.333-95.514,213.333-213.333S331.157,0,213.333,0z M174.199,322.918l-93.935-93.931l31.309-31.309l62.626,62.622
	l140.894-140.898l31.309,31.309L174.199,322.918z"/>
                </svg>
            </li>
        </ul>
    </div>
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'l-order'],
    ]) ?>
        <? foreach ($orders as $index => $order) { ?>
        <? $this->registerJs("guideDates[$order->tourId] = {$order->tour->getGuideDates($order->languageId)};", $this::POS_HEAD) ?>
		<?php 
			$currency = (new \yii\db\Query())->select('id,simbol,alias')->from('currency')->where(['id' => ((int) Yii::$app->request->get('currency')>0)?(int) $_GET['currency']:1])->one();
			$currency_alias = (new \yii\db\Query())->select('id,alias')->from('currency')->all();
		?>
        <div class="c-orderItem">
            <div class="c-orderItem__wrapper">
                <div class="c-orderItem__title">
                    <?= $order->tour->name ?>
                </div>
                <div class="c-orderItem__body">
                    <div class="c-orderItem__label--black">
                        1. Travellers
                    </div>
                    <div class="c-orderItem__inputBlock">
                        <div class="c-orderItem__chooseGroup" <?= (empty($order->tour->groups)) ? 'style="display:none"' : '' ?> >
                            <div class="c-orderItem__label--normal">
                                Price option:
                            </div>
                            <?= $form->field($order, "[$index]groupId")->widget(Select2::classname(), [
                                'data' => $order->tour->groupsList,
                                'theme' => 'default',
                                'options' => [
                                    'placeholder' => 'Choose group',
                                    'class' => 'c-orderItem__select',
                                ],
                            ])->label(false) ?>
                        </div>
                        <div class="c-orderItem__chooseTravellers" <?= (empty($order->tour->groups)) ? '' : 'style="display:none"' ?> >
                            <? if (!empty($order->tour->priceAdult)) { ?>
                            <div class="c-orderItem__inputColumn">
                                <div class="c-orderItem__inputLabel">
                                    Adults
                                </div>
                                <?= $form->field($order, "[$index]totalAdult")->textInput(['class' => 'c-orderItem__input--small', 'placeholder' => '- Adult -'])->label(false) ?>
                            </div>
                            <? } ?>
                            <? if (!empty($order->tour->priceChild)) { ?>
                            <div class="c-orderItem__inputColumn">
                                <div class="c-orderItem__inputLabel">
                                    Children
                                </div>
                                <?= $form->field($order, "[$index]totalChild")->textInput(['class' => 'c-orderItem__input--small', 'placeholder' => '- Child -'])->label(false) ?>
                            </div>
                            <? } ?>
                            <? if (!empty($order->tour->priceInfant)) { ?>
                            <div class="c-orderItem__inputColumn">
                                <div class="c-orderItem__inputLabel">
                                    Infants
                                </div>
                                <?= $form->field($order, "[$index]totalInfant")->textInput(['class' => 'c-orderItem__input--small', 'placeholder' => '- Infant -'])->label(false) ?>
                            </div>
                            <? } ?>
                            <? if (!empty($order->tour->priceSenior)) { ?>
                            <div class="c-orderItem__inputColumn">
                                <div class="c-orderItem__inputLabel">
                                    Seniors
                                </div>
                                <?= $form->field($order, "[$index]totalSenior")->textInput(['class' => 'c-orderItem__input--small', 'placeholder' => '- Senior -'])->label(false) ?>
                            </div>
                            <? } ?>
                        </div>
                    </div>
                    <div class="c-orderItem__columnBlock cf">
                        <div class="c-orderItem__column--left">
                            <div class="c-orderItem__label--black">
                                2. Choose guide
                            </div>
                            <?= $form->field($order, "[$index]languageId")->widget(Select2::classname(), [
                                'data' => $order->tour->guidesList,
                                'theme' => 'default',
                                'options' => [
                                    'class' => 'c-orderItem__select',
                                    'id' => "chosenGuide-$order->tourId",
                                    'placeholder' => 'Guide',
                                ],
                            ])->label(false) ?>
                        </div>
                        <div class="c-orderItem__column--right">
                            <div class="c-orderItem__label--black">
                                3. Pick date
                            </div>
                            <?= $form->field($order, "[$index]dateStartTour")->widget(DatePicker::classname(), [
                                'type' => DatePicker::TYPE_INPUT,
                                'options' => [
                                    'id' => "guideDatepicker-$order->tourId",
                                    'class' => 'c-orderItem__input',
                                ],
                                'pluginOptions' => [
                                    'format' => 'd/m/yyyy',
                                    'beforeShowDay' => new JsExpression("
                                        function(date) {
                                            if ($.inArray(date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear(), guideDates[$order->tourId]) !== -1) {
                                                return;
                                            } else {
                                                return false;
                                            }
                                        }
                                    ")
                                ],
                            ])->label(false) ?>
                        </div>
                    </div>
                    <div class="c-orderItem__label--black">
                        4. Contact info
                    </div>
                    <div class="c-orderItem__columnBlock cf">
                        <div class="c-orderItem__column--left">
                            <div class="c-orderItem__label--normal">
                                Lead traveller’s first name
                            </div>
                            <?= $form->field($order, "[$index]leadFirstname")->textInput(['class' => 'c-orderItem__input', 'placeholder' => 'First name', 'value' => ($userdata['firstname']) ? $userdata['firstname'] : ""])->label(false) ?>
                        </div>
                        <div class="c-orderItem__column--right">
                            <div class="c-orderItem__label--normal">
                                Lead traveller’s last name
                            </div>
                            <?= $form->field($order, "[$index]leadLastname")->textInput(['class' => 'c-orderItem__input', 'placeholder' => 'Last name', 'value' => ($userdata['lastname']) ? $userdata['lastname'] : ""])->label(false) ?>
                        </div>
                        <div class="c-orderItem__separator"></div>
                        <div class="c-orderItem__column--left">
                            <div class="c-orderItem__label--normal">
                                Lead traveller’s phone number (with country code)
                            </div>
                            <?= $form->field($order, "[$index]leadPhone")->textInput(['class' => 'c-orderItem__input', 'placeholder' => 'Phone number', 'value' => ($userdata['phone']) ? $userdata['phone'] : ""])->label(false) ?>
                        </div>
                        <div class="c-orderItem__column--right">
                            <div class="c-orderItem__label--normal">
                                Lead traveller’s e-mail
                            </div>
                            <?= $form->field($order, "[$index]leadEmail")->textInput(['class' => 'c-orderItem__input', 'placeholder' => 'E-mail', 'value' => ($userdata['public_email']) ? $userdata['public_email'] : ""])->label(false) ?>
                        </div>
                    </div>
                    <div class="c-orderItem__label--black">
                        5. Pickup points
                    </div>
                    <div class="c-orderItem__pickupBlock">
                        <? if (!empty($order->tour->hotelPickup)) { ?>
                        <div <?= (empty($order->tour->hotelPickup)) ? 'style="display:none"' : '' ?> >
                            <div class="c-orderItem__label--normal">
                                Hotel Pick-Up
                            </div>
                            <?= $form->field($order, "[$index]pickInfo")->textInput([
                                'class' => 'c-orderItem__input',
                                'placeholder' => 'Enter your Hotel Name or Collection Address',
                            ])->label(false) ?>
                        </div>
                        <? } else { ?>
                        <div <?= (empty($order->tour->hotelPickup)) ? '' : 'style="display:none"' ?> >
                            <div class="c-orderItem__label--normal">
                                Pick-Up points
                            </div>
                            <?= $form->field($order, "[$index]pickInfo")->widget(Select2::classname(), [
                                'data' => $order->tour->pickupPointsList,
                                'theme' => 'default',
                                'options' => [
                                    'class' => 'c-orderItem__select',
                                    'placeholder' => '- Choose Pick-Up point -',
                                ],
                            ])->label(false) ?>
                        </div>
                        <? } ?>
                    </div>
                    <div class="c-orderItem__map" id="map-<?=$order->tourId?>"></div>
                    <div class="c-orderItem__errorBlock">
                        <div class="c-orderItem__errorTitle">
                            Important information
                        </div>
                        <div class="c-orderItem__errorText">
                            <?= $order->tour->addInfo ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-orderItem__bottom">
                <? if (!empty($order->groupId)) { ?>
                <div class="c-orderItem__bottomColumn">
                    <div class="c-orderItem__bottomBlock">
                        <span class="c-orderItem__bottomText--gray">
                            <?= $order->group->peopleFrom ?> - <?= $order->group->peopleTo ?> travellers:
                        </span>
                        <span class="c-orderItem__bottomText--lightblue">
							<?php if($order->tour->desireCurrency==$currency['id']):?>
                            <?php echo $currency['simbol'];?><?= $order->group->price ?>
							<?php else: ?>
								<?php $amount = $order->group->price ; $from=$currency_alias[$order->tour->desireCurrency-1]['alias'];$to=$currency['alias'];?>
								<?php $prGr = Converter::getValue($amount, $from, $to); ?>
								<?php if($prGr):?>
									<?php echo $currency['simbol'];?><?= $prGr ?>
								<?php endif;?>
							<?php endif;?>	
                        </span>
                    </div>
                </div>
                <div class="c-orderItem__bottomColumn"></div>
                <? } else { ?>
                <div class="c-orderItem__bottomColumn">
                    <? if ($order->totalAdult) { ?>
                    <div class="c-orderItem__bottomBlock">
                        <?php if($order->tour->desireCurrency==$currency['id']):?>
						<span class="c-orderItem__bottomText--gray">
                            <?php echo $currency['simbol'];?><?= "{$order->tour->priceAdult} x $order->totalAdult Adults:" ?>
                        </span>
                        <span class="c-orderItem__bottomText--lightblue">
                            <?php echo $currency['simbol'];?><?= $order->tour->priceAdult * $order->totalAdult ?>
                        </span>
						<?php else: ?>
							<?php $amount = $order->tour->priceAdult ; $from=$currency_alias[$order->tour->desireCurrency-1]['alias'];$to=$currency['alias'];?>
							<?php $prAdult = Converter::getValue($amount, $from, $to); ?>
							<?php if($prAdult):?>
								<span class="c-orderItem__bottomText--gray">
								<?php echo $currency['simbol'];?><?= "{$prAdult} x $order->totalAdult Adults:" ?>
								</span>
								<span class="c-orderItem__bottomText--lightblue">
								<?php echo $currency['simbol'];?><?= floatval($prAdult) * $order->totalAdult ?>
								</span>
							<?php endif; ?>
						<?php endif;?>	
                    </div>
                    <? } ?>
                    <? if ($order->totalChild) { ?>
                    <div class="c-orderItem__bottomBlock">
						<?php if($order->tour->desireCurrency==$currency['id']):?>
                        <span class="c-orderItem__bottomText--gray">
                           <?php echo $currency['simbol'];?><?= "{$order->tour->priceChild} x $order->totalChild Children:" ?>
                        </span>
                        <span class="c-orderItem__bottomText--lightblue">
                             <?php echo $currency['simbol'];?><?= $order->tour->priceChild * $order->totalChild ?>
                        </span>
						<?php else: ?>
							<?php $amount = $order->tour->priceChild ; $from=$currency_alias[$order->tour->desireCurrency-1]['alias'];$to=$currency['alias'];?>
							<?php $prCh = Converter::getValue($amount, $from, $to); ?>
							<?php if($prCh):?>
								<span class="c-orderItem__bottomText--gray">
								<?php echo $currency['simbol'];?><?= "{$prCh} x $order->priceChild Children:" ?>
								</span>
								<span class="c-orderItem__bottomText--lightblue">
								<?php echo $currency['simbol'];?><?= floatval($prCh) * $order->priceChild ?>
								</span>
							<?php endif; ?>
						<?php endif;?>	
                    </div>
                    <? } ?>
                </div>
                <div class="c-orderItem__bottomColumn">
                    <? if ($order->totalInfant) { ?>
                    <div class="c-orderItem__bottomBlock">
                        <?php if($order->tour->desireCurrency==$currency['id']):?>
						<span class="c-orderItem__bottomText--gray">
						   <?php echo $currency['simbol'];?><?= "{$order->tour->priceInfant} x $order->totalInfant Infants:" ?>
                        </span>
                        <span class="c-orderItem__bottomText--gray">
                            <?php echo $currency['simbol'];?><?= $order->tour->priceInfant * $order->totalInfant ?>
                        </span>
						<?php else: ?>
							<?php $amount = $order->tour->priceInfant ; $from=$currency_alias[$order->tour->desireCurrency-1]['alias'];$to=$currency['alias'];?>
							<?php $prInf = Converter::getValue($amount, $from, $to); ?>
							<?php if($prInf):?>
								<span class="c-orderItem__bottomText--gray">
								<?php echo $currency['simbol'];?><?= "{$prInf} x $order->priceInfant Infants:" ?>
								</span>
								<span class="c-orderItem__bottomText--lightblue">
								<?php echo $currency['simbol'];?><?= floatval($prInf) * $order->priceInfant ?>
								</span>
							<?php endif; ?>
						<?php endif;?>	
                    </div>
                    <? } ?>
                    <? if ($order->totalSenior) { ?>
                    <div class="c-orderItem__bottomBlock">
					<?php if($order->tour->desireCurrency==$currency['id']):?>
                        <span class="c-orderItem__bottomText--gray">
                            <?php echo $currency['simbol'];?><?= "{$order->tour->priceSenior} x $order->totalSenior Senior:" ?>
                        </span>
                        <span class="c-orderItem__bottomText--gray">
                            <?php echo $currency['simbol'];?><?= $order->tour->priceSenior * $order->totalSenior ?>
                        </span>
						<?php else: ?>
							<?php $amount = $order->tour->priceSenior ; $from=$currency_alias[$order->tour->desireCurrency-1]['alias'];$to=$currency['alias'];?>
							<?php $prSe = Converter::getValue($amount, $from, $to); ?>
							<?php if($prSe):?>
								<span class="c-orderItem__bottomText--gray">
								<?php echo $currency['simbol'];?><?= "{$prSe} x $order->priceSenior Senior:" ?>
								</span>
								<span class="c-orderItem__bottomText--lightblue">
								<?php echo $currency['simbol'];?><?= floatval($prSe) * $order->priceSenior ?>
								</span>
							<?php endif; ?>
						<?php endif;?>	
                    </div>
                    <? } ?>
                </div>
                <? } ?>
                <div class="c-orderItem__line"></div>
                <div class="c-orderItem__bottomColumn">
                    <div class="c-orderItem__bottomBlock">
                        <span class="c-orderItem__bottomText--black c-orderItem__bottomText--bold">
                             Required Booking Deposit you pay now:
                        </span>
                        <span class="c-orderItem__bottomText--lightblue">
							<?php if($order->tour->desireCurrency==$currency['id']):?>
                            <?php echo $currency['simbol'];?><?= $order->payPrice ?>
							<?php else: ?>
								<?php $amount = $order->payPrice ; $from=$currency_alias[$order->tour->desireCurrency-1]['alias'];$to=$currency['alias'];?>
								<?php $prDep = Converter::getValue($amount, $from, $to); ?>	
								<?php if($prDep):?>
									<?php echo $currency['simbol'];?><?= $prDep ?>
								<?php endif;?>	
							<?php endif;?>
                        </span>
                    </div>
                    <div class="c-orderItem__bottomBlock">
                        <span class="c-orderItem__bottomText--black">
                            Remaining balance to pay on the day of tour:
                        </span>
                        <span class="c-orderItem__bottomText--lightblue">
							<?php if($order->tour->desireCurrency==$currency['id']):?>
                            <?php echo $currency['simbol'];?><?= $order->totalPrice - $order->payPrice ?>
							<?php else: ?>
								<?php $amount = $order->totalPrice ; $from=$currency_alias[$order->tour->desireCurrency-1]['alias'];$to=$currency['alias'];?>
								<?php $prTot = Converter::getValue($amount, $from, $to); ?>	
								<?php if($prTot && $prDep):?>
									<?php echo $currency['simbol'];?><?= (floatval($prTot) - floatval($prDep)) ?>
								<?php endif;?>		
							<?php endif;?>	
                        </span>
                    </div>
                    <div class="c-orderItem__bottomBlock">
                        <span class="c-orderItem__bottomText--black">
                            Total per tour:
                        </span>
                        <span class="c-orderItem__bottomText--lightblue">
							<?php if($order->tour->desireCurrency==$currency['id']):?>
                            <?php echo $currency['simbol'];?><?= $order->totalPrice ?>
							<?php else: ?>
								<?php if($prTot):?>
									<?php echo $currency['simbol'];?><?= $prTot ?>
								<?php endif;?>	
							<?php endif;?>	
                        </span>
                    </div>
                </div>
                <div class="c-orderItem__bottomColumn">
                    <div class="c-orderItem__bottomBlock">
                        <span class="c-orderItem__total">
                            You pay now:
                        </span>
                        <span class="c-orderItem__totalPrice">
						<?php if($order->tour->desireCurrency==$currency['id']):?>
                            <?php echo $currency['simbol'];?><?= $order->payPrice ?>
						<?php else: ?>
							<?php if($prDep):?>
								<?php echo $currency['simbol'];?><?= $prDep ?>
							<?php endif;?>	
						<?php endif;?>		
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="c-orderItem__toggle">
            <svg class="c-orderItem__toggleIcon c-orderItem__toggleIcon--rotate" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129">
                <path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"/>
            </svg>
        </div>
        <? } ?>
        <div class="l-order__summaryBottom cf">
            <div class="l-order__summaryBlock">
                <div class="l-order__summaryTitle">
                    Order summary:
                </div>
                <div class="c-orderItem__bottomText--black c-orderItem__bottomText--bold">
                    Total Required Booking Deposit you pay now:
                    <span class="c-orderItem__bottomText--lightblue">
					<?php if($order->tour->desireCurrency==$currency['id']):?> 
                        <?php echo $currency['simbol'];?><?= $payPriceSum ?>
					<?php else: ?>
						<?php $amount = $payPriceSum; $from=$currency_alias[$order->tour->desireCurrency-1]['alias'];$to=$currency['alias'];?>
						<?php $prpaySum = Converter::getValue($amount, $from, $to); ?>	
						<?php if($prpaySum):?>
							<?php echo $currency['simbol'];?><?= $prpaySum ?>
						<?php endif;?>	
					<?php endif;?>		
                    </span>
                </div>
                <div class="l-order__bottomRow">
                    <div class="c-orderItem__bottomText--blackk">
                        Total Remaining Balance you pay on the day of the tour(s):
                        <span class="c-orderItem__bottomText--lightblue">
						<?php if($order->tour->desireCurrency==$currency['id']):?> 
                            <?php echo $currency['simbol'];?><?= $totalPriceSum - $payPriceSum ?>
						<?php else: ?>
						<?php $amount = $totalPriceSum;$from=$currency_alias[$order->tour->desireCurrency-1]['alias'];$to=$currency['alias'];?>
						<?php $prtotsum = Converter::getValue($amount,$from,$to); ?>	
							<?php if($prtotsum && $prpaySum):?>
								<?php echo $currency['simbol'];?><?php echo (floatval($prtotsum) - floatval($prpaySum)); ?>
							<?php endif;?>		
						<?php endif;?>		
                        </span>
                    </div>
                </div>
                <div class="l-order__bottomRow">
                    <div class="c-orderItem__bottomText--black">
                        TOTAL:
                        <span class="c-orderItem__bottomText--lightblue">
						<?php if($order->tour->desireCurrency==$currency['id']):?>
                            <?php echo $currency['simbol'];?><?= $totalPriceSum ?>
						<?php else: ?>
							<?php if($prtotsum):?>
								<?php echo $currency['simbol'];?><?php  echo $prtotsum; ?>
							<?php endif;?>		
						<?php endif;?>		
                        </span>
                    </div>
                </div>
            </div>
            <div class="l-order__priceBlock">
                <div class="l-order__priceText">
                    <div class="l-order__priceTitle">
                        Required Booking Deposit:
                    </div>
                    <div class="l-order__priceSum">
						<?php if($order->tour->desireCurrency==$currency['id']):?>
                        <?php echo $currency['simbol'];?><?= $payPriceSum ?>
						<?php else: ?>
							<?php if($prpaySum):?>
								<?php echo $currency['simbol'];?><?php  echo $prpaySum; ?>
							<?php endif;?>		
						<?php endif;?>		
                    </div>
                    <?= Html::submitButton('Payment', ['class' => 'c-button']) ?>
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
