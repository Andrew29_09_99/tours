<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 29.08.2017
 * Time: 18:03
 */
//debug($order);
//$this->registerCssFile('css/voucher.css');
?>

<div class="c-voucher">
    <div class="c-voucher__container">
        <div class="c-voucher__headerWrapper">
            <div class="c-voucher__header">
                Voucher
            </div>
            <div class="c-voucher__text">
                Tour/Activity Ticket & Booking Confirmation
            </div>
            <div class="c-voucher__text--boldUpper">
                (please, print this voucher and take with you!)
            </div>
            <div class="c-voucher__idBlock">
                <div class="c-voucher__idContent">
                    <span class="c-voucher__text--boldUpper">ID:</span>
                    <span class="c-voucher__text"><?= $order['orderId'] ?></span>
                    <div class="c-voucher__bookingRef">
                        <div class="c-voucher__text--bold">
                            Booking Ref:
                        </div>
                        <div class="c-voucher__text">
                            <?= $order['voucher'] ?>
                        </div>
                    </div>
                </div>
                <div class="c-voucher__tourName">
                    <?= strtoupper($order['tour']['name']) ?>
                </div>
            </div>
<!--            <img src="img/icon/logo.svg" class="c-voucher__logo">-->
            <img src="<?= \yii\helpers\Url::to('@web/img/icon/logo.svg', true) ?>" class="c-voucher__logo">
        </div>
        <div class="c-voucher__block">
            <div class="c-voucher__blockHeader">
                <div class="c-voucher__text--boldUpper">
                    Participants
                </div>
            </div>
            <div class="c-voucher__blockBody">
                <div class="c-voucher__text--bold"><?= $order['totalAdult'] ?> Adult(s) <?php if($order['tour']['priceSenior']){?> + <?=$order['tour']['priceSenior']?> Senior <?php } ?><?php if($order['tour']['priceChild']){?> + <?=$order['tour']['priceChild']?> Children <?php } ?><?php if($order['tour']['priceInfant']){?> + <?=$order['tour']['priceInfant']?> Infant <?php } ?></div>
<!--                <div class="c-voucher__text--bold">5 Adults + 2 Children</div>-->
                <div class="c-voucher__text">Lead Traveller: <?= $order['leadFirstname'] . ' ' . $order['leadLastname']?></div>
                <div class="c-voucher__text">Mobile phone: <?= $order['leadPhone'] ?></div>
            </div>
            <?php if(!is_int($order['pickInfo'])) : ?>
                <div class="c-voucher__blockFooter">
                    <div class="c-voucher__text--bold">Hotel:</div>
                    <div class="c-voucher__text"><?= $order['pickInfo'] ?></div>
                </div>
            <?php endif; ?>
        </div>
        <div class="c-voucher__block">
            <div class="c-voucher__blockHeader">
                <div class="c-voucher__text--boldUpper">
                    Details
                </div>
            </div>
            <div class="c-voucher__blockBody--full">
                <div class="c-voucher__columns">
                    <div class="c-voucher__column">
                        <div class="c-voucher__text--bold">Inclusions:</div>
                        <ul class="c-voucher__list">
                            <?php if(is_array($order['tour']['inclusion'])) : ?>
                                <?php foreach($order['tour']['inclusion'] as $inclusion) : ?>
                                    <li class="c-voucher__listItem"><?= $inclusion ?></li>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <?= $order['tour']['inclusion'] ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="c-voucher__column">
                        <div class="c-voucher__text--bold">Exclusions:</div>
                        <ul class="c-voucher__list">
                            <?php if(is_array($order['tour']['exclusion'])) : ?>
                                <?php foreach($order['tour']['exclusion'] as $exclusion) : ?>
                                    <li class="c-voucher__listItem"><?= $exclusion ?></li>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <?= $order['tour']['exclusion'] ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="c-voucher__column">
                        <div class="c-voucher__text--boldUpper">IMPORTANT NOTE:</div>
                        <ul class="c-voucher__list">
                            <?php if(is_array($order['tour']['addInfo'])) : ?>
                                <?php foreach($order['tour']['addInfo'] as $note) : ?>
                                    <li class="c-voucher__listItem"><?= $note ?></li>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <?= $order['tour']['addInfo'] ?>
                            <?php endif; ?>
        <!--                    <li class="c-voucher__listItem">Please, take with you water, light jackets and breathing shoes</li>-->
        <!--                    <li class="c-voucher__listItem">Look for the logo with dolphin "Atlantico Excursions" under the bus front glass</li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="c-voucher__block">
            <div class="c-voucher__blockHeader">
                <div class="c-voucher__text--boldUpper">
                    Start Time
                </div>
            </div>
            <div class="c-voucher__blockBody--full">
                <div class="c-voucher__text--big">
                    <?= date('h:i A', $order['dateStartTour']) ?> on <?= date('L F j Y', $order['dateStartTour']) ?>
<!--                    8:25 AM on SATURDAY / SEPTEMBER 30, 2017-->
                </div>
            </div>
        </div>
        <div class="c-voucher__block">
            <div class="c-voucher__blockHeader">
                <div class="c-voucher__text--boldUpper">
                    A DAY BEFORE THE TOUR / ACTIVITY
                </div>
            </div>
            <div class="c-voucher__blockBody--full">
                <div class="c-voucher__text--bold">
                    Please, reconfirm your booking 1 day before the booked date of the Tour/Activity
                    <br>
                    directly with the Tour/Activity Provider, calling phone nr:
                </div>
                <div class="c-voucher__text--big">
                    <?= $order['provider']['phoneProvider'] ?>
                    <?= $order['provider']['nameProvider'] ?>
                </div>
                <div class="c-voucher__text">
                    It's obligatory, because a lot of reasons like weather conditions, etc. If your phone is not
                    <br>
                    working, you can always call from your hotel's reception.
                </div>
            </div>
        </div>
        <div class="c-voucher__block">
            <div class="c-voucher__blockHeader">
                <div class="c-voucher__text--boldUpper">
                    MEETING POINT
                </div>
            </div>
            <div class="c-voucher__blockBody--full">
                <div class="c-voucher__text--big">
<!--                    Public Bus Stop by the road by the hotel TRIP TENERIFE-->
<!--                    --><?//= $order['pickupPoint']['name'] ?>
                </div>
                <div class="c-voucher__text--bold">
<!--                    Avenida Juan Carlos I, 29, 38650 Los Cristianos, Tenerife, Canary Islands, Spain-->
<!--                    --><?//= $order['pickupPoint']['addInfo'] ?>
                </div>
                <div class="c-voucher__text">
                    Please, see attached map showing Meeting Point location.
                    <br>
                    Please, contact activity Provider, if you are not sure where's the Meeting Point located exactly.
                </div>
            </div>
        </div>
        <div class="c-voucher__block">
            <div class="c-voucher__blockHeader">
                <div class="c-voucher__text--boldUpper">
                    Payment
                </div>
            </div>
            <p>Payment</p>
            <div class="c-voucher__blockBody--full">
                <div class="c-voucher__columns">
                    <div class="c-voucher__column">
                        <div class="c-voucher__text--boldUpper">Total</div>
                        <div class="c-voucher__text">Adults: <?= $order['totalAdult'] ?> x $<?= $order['tour']['priceAdult'] ?> = <?php echo $order['totalAdult']*$order['tour']['priceAdult'] ?></div>
                        <?php if($order['totalSenior']) : ?>
                            <div class="c-voucher__text">Seniors: <?= $order['totalSenior'] ?> x $<?= $order['tour']['priceSenior'] ?> = <?php echo $order['totalSenior']*$order['tour']['priceSenior'] ?></div>
                        <?php endif; ?>
                        <?php if($order['totalChild']) : ?>
                            <div class="c-voucher__text">Children: <?= $order['totalChild'] ?> x $<?= $order['tour']['priceChild'] ?> = <?php echo $order['totalChild']*$order['tour']['priceChild'] ?></div>
                        <?php endif; ?>
                        <?php if($order['totalInfant']) : ?>
                        <div class="c-voucher__text">Infant: <?= $order['totalInfant'] ?> x $<?= $order['tour']['priceInfant'] ?> = <?php echo $order['totalInfant']*$order['tour']['priceInfant'] ?></div>
                        <?php endif; ?>
                        <div class="c-voucher__text">TOTAL: $<?= $order['totalPrice'] ?></div>
                    </div>
                    <div class="c-voucher__column">
                        <div class="c-voucher__text--boldUpper">Paid booking deposit</div>
                        <div class="c-voucher__text">Adults: <?= $order['totalAdult'] ?> x $<?= $order['tour']['deposit'] ?> = $<?php echo $order['totalAdult']*$order['tour']['deposit'] ?></div>
                        <?php if($order['totalSenior']) : ?>
                            <div class="c-voucher__text">Seniors: <?= $order['totalSenior'] ?> x $<?= round(((Yii::$app->params['depositPercentage'] / 100) * $order['tour']['priceSenior']), 2)?> = $<?php echo $order['totalSenior']*round(((Yii::$app->params['depositPercentage'] / 100) * $order['tour']['priceSenior']), 2) ?></div>
                        <?php endif; ?>
                        <?php if($order['totalChild']) : ?>
                            <div class="c-voucher__text">Children: <?= $order['totalChild'] ?> x $<?= round(((Yii::$app->params['depositPercentage'] / 100) * $order['tour']['priceChild']), 2)?> = $<?php echo $order['totalChildren']*round(((Yii::$app->params['depositPercentage'] / 100) * $order['tour']['priceChild']), 2) ?></div>
                        <?php endif; ?>
                        <?php if($order['totalInfant']) : ?>
                            <div class="c-voucher__text">Infant: <?= $order['totalInfant'] ?> x $<?= round(((Yii::$app->params['depositPercentage'] / 100) * $order['tour']['priceInfant']), 2)?> = $<?php echo $order['totalInfant']*round(((Yii::$app->params['depositPercentage'] / 100) * $order['tour']['priceInfant']), 2) ?></div>
                        <?php endif; ?>
                        <div class="c-voucher__text">TOTAL: $<?= $order['payPrice'] ?></div>
                    </div>
                    <?php
                    /**
                     * Sorry, guys, I know - it's f*cking amazing bullshit
                     */
                    ?>

                    <div class="c-voucher__column">
                        <div class="c-voucher__text--boldUpper">Remaining balance to pay</div>
                        <div class="c-voucher__text">Adults: <?= $order['totalAdult'] ?> x $<?php echo $order['tour']['priceAdult']-$order['tour']['deposit'] ?> = $<?php echo $order['totalAdult'] *($order['tour']['priceAdult']-$order['tour']['deposit'])?></div>
                        <?php if($order['totalSenior']) : ?>
                            <div class="c-voucher__text">Seniors: <?= $order['totalSenior'] ?> x $<?= $order['tour']['priceSenior'] - round(((Yii::$app->params['depositPercentage'] / 100) * $order['tour']['priceSenior']), 2)?> = $<?php echo $order['totalSenior'] * ($order['tour']['priceSenior'] - round(((Yii::$app->params['depositPercentage'] / 100) * $order['tour']['priceSenior']), 2)) ?></div>
                        <?php endif; ?>
                        <?php if($order['totalChild']) : ?>
                            <div class="c-voucher__text">Children: <?= $order['totalChild'] ?> x $<?= $order['tour']['priceChild'] - round(((Yii::$app->params['depositPercentage'] / 100) * $order['tour']['priceChild']), 2)?> = $<?php echo $order['totalChildren'] * ($order['tour']['priceChild'] - round(((Yii::$app->params['depositPercentage'] / 100) * $order['tour']['priceChild']), 2)) ?></div>
                        <?php endif; ?>
                        <?php if($order['totalInfant']) : ?>
                            <div class="c-voucher__text">Seniors: <?= $order['totalInfant'] ?> x $<?= $order['tour']['priceInfant'] - round(((Yii::$app->params['depositPercentage'] / 100) * $order['tour']['priceInfant']), 2)?> = $<?php echo $order['totalInfant'] * ($order['tour']['priceInfant'] - round(((Yii::$app->params['depositPercentage'] / 100) * $order['tour']['priceInfant']), 2)) ?></div>
                        <?php endif; ?>
                        <div class="c-voucher__text">TOTAL: $<?php echo $order['totalPrice'] - $order['payPrice'] ?></div>
                    </div>
                </div>
                <div class="c-voucher__text--bold">
                    Please, prepare remaining balance of $<?php echo $order['totalPrice'] - $order['payPrice'] ?> to pay directly to the provider
                    <br>
                    on the day of the tour/activity
                </div>
                <div class="c-voucher__text">
                    You can pay the remaining balance with:
                </div>
                <ul class="c-voucher__list c-voucher__columns">
                    <li class="c-voucher__listItem">Credit Card</li>
                    <li class="c-voucher__listItem">Debit Card</li>
<!--                    <li class="c-voucher__listItem">Cash in EUR (438.64 EUR)</li>-->
                    <li class="c-voucher__listItem">Cash in USD (<?php echo $order['totalPrice'] - $order['payPrice'] ?> USD)</li>
                </ul>
            </div>
        </div>
        <div class="c-voucher__block c-voucher__footerWrapper">
            <div class="c-voucher__blockHeader">
                <div class="c-voucher__text--boldUpper">
                    TERMS & CONDITIONS
                </div>
            </div>
            <div class="c-voucher__blockBody--full">
                <div class="c-voucher__text">
                    Read our complete Terms & Conditions for information on modifications, changes, cancellations, etc,
                    by clicking on this link:<a href="" class="c-voucher__text--bold"> Terms & Conditions</a>. To do a modification, change or cancellation, please,
                    <a href="" class="c-voucher__text--bold"> login to Your TripsPoint Account </a> and do it there.
                    <br>
                    For the security reasons we do not do any modifications, changes or cancellations by phone or email.
                </div>
                <div class="c-voucher__columns--small">
                    <div class="c-voucher__column">
                        <div class="c-voucher__text--bold">Tour/Activity Provider:</div>
                        <div class="c-voucher__text--big">+34 618 786 749</div>
                    </div>
                    <div class="c-voucher__column">
                        <div class="c-voucher__text--bold">TripsPoint Help & Support Line:</div>
                        <div class="c-voucher__text--big">+1 234 2000 811</div>
                    </div>
                </div>
                <div class="c-voucher__copyright">
                    Service is offered by the Tour/Activity Provider, associated Supplier of TripsPoint.com
                </div>
            </div>
        </div>
    </div>
</div>


