<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerCssFile('/css/supplier.css');
$this->title = 'Supplier';
?>
<div class="l-action l-action--supplier">
    <div class="l-action__bg"></div>
    <div class="l-mainContent">
        <div class="l-action__content">
            <div class="l-action__header">
                <div class="l-action__head">Grow your Sales Worldwide</div>
                <div class="l-action__head l-action__head--small">Add existing or Create your own new Business
                </div>
            </div>
            <div class="l-action__body">
                <div class="l-action__text l-action__text--bold">TripsPoint connecting Travellers and Local
                    Businsses in Tours and Activities,
                    Holiday Acommodations, Local rentals & Services for Travellers
                </div>

            </div>
            <div class="l-action__footer">
                <div class="l-action__text l-action__text--green l-action__text--bold">
                    FREE SETUP - EASY - SIMPLE - FRIENDLY & FREE TO USE
                </div>
                <div class="l-action__text l-action__text--green l-action__text--bold">
                    Provide your net prices - get Secured Bookings & Direct Payments from Travellers
                </div>
                <div class="l-action__positioning">
                    <div class="c-button c-button--accent">Start now</div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- якщо користувач вже подав заявку як саплаєр - відображається наступний текст -->
<?php if(Yii::$app->session->hasFlash('test')){ ?>
    <?= Yii::$app->session->getFlash('test'); ?>
<?php } ?>

<div class="l-supplier">
    <div class="l-supplier__block l-mainContent">
        <div class="l-supplier__timeLine">
            <div class="c-timeLine">
                <div class="c-timeLine__content">
                    <div class="c-timeLine__block c-timeLine__block--first">
                        <div class="c-timeLine__picture">
                            <img src="/img/icon/number/step1.svg" class="c-timeLine__icon">
                        </div>
                        <div class="c-timeLine__blockContent">
                            <div class="c-timeLine__blockHead">Supplier sign up</div>
                            <div class="c-timeLine__text">Register your existing business or create a new
                                business as a private tour guide, property owner or equipment, service provider for
                                Travellers.
                            </div>
                            <div class="c-timeLine__buttonWrap">
                                <a href="<?= Url::to(['/user/register']) ?>" class="c-button">Sign Up</a>
                            </div>
                        </div>
                    </div>
                    <div class="c-timeLine__block">
                        <div class="c-timeLine__picture">
                            <img src="/img/icon/number/step2.svg" class="c-timeLine__icon">
                        </div>
                        <div class="c-timeLine__blockContent">
                            <div class="c-timeLine__blockHead">Get account confirmation</div>
                            <div class="c-timeLine__text">Your registration will be reviewed. In the case, if we
                                need to verify details about you and your services, we may contact you directly.
                                Your supplier Account will be activated right after verification.
                            </div>
                        </div>
                    </div>
                    <div class="c-timeLine__block">
                        <div class="c-timeLine__picture">
                            <img src="/img/icon/number/step3.svg" class="c-timeLine__icon">
                        </div>
                        <div class="c-timeLine__blockContent">
                            <div class="c-timeLine__blockHead">Add and manage your listings</div>
                            <div class="c-timeLine__text">You will be able to add unlimited number of your Tours, Holiday Properties and Rental Services, manage availability, all the details and make it accessible through TripsPoint.com to the millions of Travellers whole the world around.
                            </div>
                        </div>
                    </div>
                    <div class="c-timeLine__block">
                        <div class="c-timeLine__picture">
                            <img src="/img/icon/number/step4.svg" class="c-timeLine__icon">
                        </div>
                        <div class="c-timeLine__blockContent">
                            <div class="c-timeLine__blockHead">Get bookings and direct payments</div>
                            <div class="c-timeLine__text">After your listing appear on TripsPoint.com you will start receiving bookings and what makes TripsPoint.com different, you wouldn't need to wait for payments - your money comes to you directly with Customers. You will be also able to communicate directly with Customers through TripsPoint.com if you will need some more details or information.
                            </div>
                        </div>
                    </div>
                    <div class="c-timeLine__block">
                        <div class="c-timeLine__picture">
                            <img src="/img/icon/number/step5.svg" class="c-timeLine__icon">
                        </div>
                        <div class="c-timeLine__blockContent">
                            <div class="c-timeLine__blockHead">Get reviews and opinions</div>
                            <div class="c-timeLine__text">Build your credibility and trust on TripsPoint.com providing for your Customers the best and highest possible level of service and getting promoted with direct reviews of real Cusmomers. Only your real Customers are leaving Reviews for you, it strictly controlled by TripsPoint.com, makes your Reviews a Real Experience that Travellers shares with others and eliminates any possibility of fake reviews or opinions. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="l-supplier__block l-supplier__block--fixedBg">
        <div class="l-supplier__bg"></div>
        <div class="l-supplier__inside l-mainContent">
            <div class="l-supplier__head l-supplier__head--white">Free, secure & easy</div>
            <div class="l-supplier__list list">
                <div class="l-supplier__item l-supplier__item--big item">
                    <img src="/img/icon/tickets.svg" class="l-supplier__img">
                    <div class="l-supplier__name">free setup & free to use</div>
                    <div class="l-supplier__text">Supplier Account on TripsPoint.com is absolutely and 100% Free-Of-Charge to set up and use. No matter are you a Company or a Person, are you existing business or new one, you can get Supplier Account and grow your sales in incredibly easy way, because TripsPoint.com will do the most or work for you. Moreover, you can create your New Business based on TripsPoint.com and the only thing we demand from you is highest quality of services. </div>
                </div>
                <div class="l-supplier__item l-supplier__item--big item">
                    <img src="/img/icon/tickets.svg" class="l-supplier__img">
                    <div class="l-supplier__name">sure and secured bookings </div>
                    <div class="l-supplier__text">All the bookings are secured by the bookings deposit - the difference between your net price and our best price for a Traveller. Traveller pays us the booking deposit at the time of booking and then pays the balance (net price) directly to you. It makes your booking secure and sure, eliminating last moment cancellations, no show cases and a mess caused with unsure bookings and requests. In short, TripsPoint.com works for you as your International travel agent.</div>
                </div>
                <div class="l-supplier__item l-supplier__item--big item">
                    <img src="/img/icon/tickets.svg" class="l-supplier__img">
                    <div class="l-supplier__name">easy to use </div>
                    <div class="l-supplier__text">We are investing huge amounts of money and efforts to deliver for Customers and Suppliers the most advanced tool in the world with the most user friendly, intuitive and practical interface offering troubleless and smooth way of making and managing bookings. That makes TripsPoint.com extremely easy and friendly both for the Travellers and Suppliers, providing Tours and Activities, Holiday Accommodations and Rental Services all the world around.</div>
                </div>
            </div>
            <div class="l-supplier__head l-supplier__head--white">Support & double booking care</div>
            <div class="l-supplier__list list">
                <div class="l-supplier__item l-supplier__item--big item">
                    <img src="/img/icon/tickets.svg" class="l-supplier__img">
                    <div class="l-supplier__name">personal account manager </div>
                    <div class="l-supplier__text">If you are constantly busy with your routine daily tasks or have no constant access to the Internet, but wish your Tours or Activities, Holiday Accommodations or Rental Services to be listed on TripsPoint.com, that's no problem. Please, contact our Destination Manager in your country and our representative in your country will not just help you to list your services, but will also manage bookings, confirming every booking through the phone directly with you. </div>
                </div>
                <div class="l-supplier__item l-supplier__item--big item">
                    <img src="/img/icon/tickets.svg" class="l-supplier__img">
                    <div class="l-supplier__name">fast booking confirmation </div>
                    <div class="l-supplier__text">A Traveller, booking a Tour, Activity, Holiday Accommodation or a Rental Service, expect quick booking confirmation and will rather resign from a booking, if he or she have to wait too long time for the booking confirmation. Even, if you are not able to react fast for a booking, our Destination Manager will contact you by phone to inform the booking details to confirm it. And that mean your Customer always happy quickly getting his booking confirmation.</div>
                </div>
                <div class="l-supplier__item l-supplier__item--big item">
                    <img src="/img/icon/tickets.svg" class="l-supplier__img">
                    <div class="l-supplier__name">24/7 support </div>
                    <div class="l-supplier__text">If you need a help or support, TripsPoint Team always ready to help you to solve different kind of situations - no matter technical questions or questions connected with bookings. Please, use the contact form or simply contact our Destination Manager in your country and we will do all possible to answer your questions or solve problems. Please, firstly try to find the answer in the FAQ section, which we constantly updating with the most popular typical subjects.</div>
                </div>
            </div>
        </div>
    </div>
    <div class="l-supplier__block">
        <div class="l-supplier__inside l-mainContent">
            <div class="l-supplier__head">Who can join?</div>
            <div class="l-supplier__list list">
                <div class="l-supplier__item l-supplier__item--small item">
                    <img src="/img/icon/tickets--blue.svg" class="l-supplier__img">
                    <div class="l-supplier__name">Day tours, trips</div>
                    <div class="l-supplier__name">& excursions providers</div>
                    <div class="l-supplier__text">Providers of bus sightseeng tours, boat trips, kayak tours, adventure tours, jeep tours, hiking and trekking tours, bike and motorbike tours, quad and buggy trips, air trips and all other kind of day trips, tours and excursions. </div>
                </div>
                <div class="l-supplier__item l-supplier__item--small item">
                    <img src="/img/icon/tickets--blue.svg" class="l-supplier__img">
                    <div class="l-supplier__name">multi-day tours</div>
                    <div class="l-supplier__name">& cruises providers</div>
                    <div class="l-supplier__text">Operators, providers and organizers of multi-day tours, including bus and jeep tours, sea and river cruises, multi-day boat trips, safaris, adventure expeditions, travel and holiday packages, etc. </div>
                </div>
                <div class="l-supplier__item l-supplier__item--small item">
                    <img src="/img/icon/tickets--blue.svg" class="l-supplier__img">
                    <div class="l-supplier__name">private</div>
                    <div class="l-supplier__name">tour guides</div>
                    <div class="l-supplier__text">Private guides offering private tours, private VIP tours, private walking city tours, private hiking and trekking tours, private boat charters and other private tours, offered exclusively for private groups of Travellers. </div>
                </div>
                <div class="l-supplier__item l-supplier__item--small item">
                    <img src="/img/icon/tickets--blue.svg" class="l-supplier__img">
                    <div class="l-supplier__name">amusement parks</div>
                    <div class="l-supplier__name">attractions & shows</div>
                    <div class="l-supplier__text">Amusement parks, regular shows, water parks, adventure parks and shows, any local attractions working on a regular basis and allowing for Travellers to book in advance their entry tickets or reserve their seats or places.  </div>
                </div>
                <div class="l-supplier__item l-supplier__item--small item">
                    <img src="/img/icon/tickets--blue.svg" class="l-supplier__img">
                    <div class="l-supplier__name">private</div>
                    <div class="l-supplier__name">holiday properties</div>
                    <div class="l-supplier__text">Property owners that offer holiday accommodation for Trevellers in their private owned apartments, private villas, holiday homes, bungalows, rooms, bed and breakfast and other kind of private vacation rentals. </div>
                </div>
                <div class="l-supplier__item l-supplier__item--small item">
                    <img src="/img/icon/tickets--blue.svg" class="l-supplier__img">
                    <div class="l-supplier__name">hotels, hostels</div>
                    <div class="l-supplier__name">& holiday resorts</div>
                    <div class="l-supplier__text">Your registration will be reviewed. In the case, if we need to verify details about you and your services, we may contact you directly. Your supplier Account will be activated right after verification. </div>
                </div>
                <div class="l-supplier__item l-supplier__item--small item">
                    <img src="/img/icon/tickets--blue.svg" class="l-supplier__img">
                    <div class="l-supplier__name">private</div>
                    <div class="l-supplier__name">tour guides</div>
                    <div class="l-supplier__text">Your registration will be reviewed. In the case, if we need to verify details about you and your services, we may contact you directly. Your supplier Account will be activated right after verification. </div>
                </div>
                <div class="l-supplier__item l-supplier__item--small item">
                    <img src="/img/icon/tickets--blue.svg" class="l-supplier__img">
                    <div class="l-supplier__name">local</div>
                    <div class="l-supplier__name">attractions providers</div>
                    <div class="l-supplier__text">Your registration will be reviewed. In the case, if we need to verify details about you and your services, we may contact you directly. Your supplier Account will be activated right after verification. </div>
                </div>
            </div>
        </div>
    </div>
    <div class="l-supplier__action">
        <div class="l-action l-action--supplier">
            <div class="l-action__bg"></div>
            <div class="l-mainContent">
                <div class="l-action__content">
                    <div class="l-action__header">
                        <div class="l-action__head">Not sure, if you can join?</div>
                        <div class="l-action__head">Ask us.</div>
                    </div>
                    <div class="l-action__body">
                        <div class="l-action__text l-action__text--bold">Generally any local business providing activities, accomodations,
                        </div>
                        <div class="l-action__text l-action__text--bold">
                            rentals and services for Travellers can be listed on TripsPoint.
                        </div>
                        <div class="l-action__text l-action__text--bold">
                            Not sure, if your services can be listed?
                        </div>
                        <div class="l-action__text l-action__text--bold">
                            Please, contact us.
                        </div>

                    </div>
                    <div class="l-action__footer">
                        <div class="c-button">Ask now</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="l-supplier__block">
        <div class="l-mainContent">
            <div class="l-supplier__head">What can be listed?</div>
            <div class="l-supplier__list list">
                <div class="l-supplier__item item">
                    <div class="c-supplierDescription">
                        <div class="c-supplierDescription__head"> Trips, Tours, Excursions & Activities </div>
                        <div class="c-supplierDescription__picture">
                            <div class="c-supplierDescription__bg"></div>
                            <img src="/img/supplier/c-supplierDesctiption__img1.png" class="c-supplierDescription__img">
                        </div>
                        <ul class="c-supplierDescription__list">
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Private Tours</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Shore Excursions</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Bus Sightseeing Tours</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Boat Trips & Charters</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Water Fun & Sports</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Fishing Tours & Charters</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Trekking & Hiking Tours</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Air Trips & Sports</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Airport Transfers</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Amusement Park Tickets</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Shows & Entertainment</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Motor Sports & Tours</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Bike Tours & Rentals</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Rent a Car or Motorbike</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">All the Trips, Tours & Activities</div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="l-supplier__item item">
                    <div class="c-supplierDescription">
                        <div class="c-supplierDescription__head"> Trips, Tours, Excursions & Activities </div>
                        <div class="c-supplierDescription__picture">
                            <div class="c-supplierDescription__bg"></div>
                            <img src="/img/supplier/c-supplierDesctiption__img2.png" class="c-supplierDescription__img">
                        </div>
                        <ul class="c-supplierDescription__list">
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Apartments</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Villas</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Bed & Breakfast</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Hotels</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Hostels</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Design Hotels</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Guesthouses</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Castles & Palaces</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Motels</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Penthouses</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Farmhouses & Eco Hotels</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">RVs, Motorhomes</div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="l-supplier__item item">
                    <div class="c-supplierDescription">
                        <div class="c-supplierDescription__head"> Trips, Tours, Excursions & Activities </div>
                        <div class="c-supplierDescription__picture">
                            <div class="c-supplierDescription__bg"></div>
                            <img src="/img/supplier/c-supplierDesctiption__img3.png" class="c-supplierDescription__img">
                        </div>
                        <ul class="c-supplierDescription__list">
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Rent a Car</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Rent a Car with Driver</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Rent a Motor Bike or Scooter</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Rent a Bike, MTB, Electric Bike</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Rent a Kayak or Canoe</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Rent a Yacht</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Rent a Motor Boat</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Rent a Surfing Equipment</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Rent a Diving Equipment</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Rent a Camp Equipment</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Home Help Services</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">Holiday Childcare Services</div>
                            </li>
                            <li class="c-supplierDescription__item">
                                <img src="/img/icon/done-tick.svg" alt="" class="c-supplierDescription__icon">
                                <div class="c-supplierDescription__text">All the Rentals & Services</div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>