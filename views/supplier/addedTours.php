<?php

use yii\helpers\Html;
use app\components\OwnToursWidget;

$this->title = empty($supplierProfile->firstname && $supplierProfile->lastname) ? Html::encode($supplierProfile->username) : Html::encode($supplierProfile->firstname) . ' ' . Html::encode($supplierProfile->lastname);
//$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/supplierAddedTours.css');
$this->registerCssFile('/css/components/c-tour.css');
$this->registerCssFile('/css/components/pagination.css');
?>

<div class="l-action l-action--textBlock">
    <div class="l-mainContent">
        <div class="l-action__content--textBlock">
            <div class="l-action__textBlock">
                <p class="l-action__text--white l-action__text--bold">
                    <?php if(Yii::$app->request->get('availbe')==1):?>
						Availiability alert!
					<?php elseif(Yii::$app->request->get('availbe')==2):?>
						Inactive Listing to Renew
					<?php elseif(Yii::$app->request->get('clone')==1):?>
						Listing to Publish
					<?php elseif(Yii::$app->request->get('active')==0):?>
						Awaiting Approval Listing
					<?php elseif(Yii::$app->request->get('active')==1):?>
						Active Listing		
					<?php else:?>
						Added tours!
					<?php endif;?>
                </p>
            </div>
        </div>
    </div>
</div>
<!--<div class="l-tools">
    <div class="l-tools__content l-mainContent cf">
        <div class="l-tools__leftPart">
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Europe
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Europe
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Europe
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Europe
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>-->



<div class="l-profilePage">
    <div class="l-mainContent">
        <div class="l-profilePage__content cf">

            <!-- блок меню користувача -->
            <?=  $this->render('/layouts/_menu') ?>
            <!-- права частина -->
            <div class="l-profilePage__rightPart">
                <div class="c-supplierProfile__title">
                    <?php if(Yii::$app->request->get('availbe')==1):?>
						Availiability alert!
					<?php elseif(Yii::$app->request->get('availbe')==2):?>
						Inactive Listing to Renew
					<?php elseif(Yii::$app->request->get('clone')==1):?>
						Listing to Publish
					<?php elseif(Yii::$app->request->get('active')==0):?>
						Awaiting Approval Listing
					<?php elseif(Yii::$app->request->get('active')==1):?>
						Active Listing		
					<?php else:?>
						Added tours!
					<?php endif;?>
                </div>
				<?php if(false):?>
                <div class="c-supplierProfile__buttonWrapper">
                    <?= Html::a('Add tour', ['tour/create','currency'=>$currencynow['id']], ['class' => 'c-button c-button--noneTransform']); ?>
                </div>
				<?php endif;?>
                <?= OwnToursWidget::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '../_list',
                    'page'  => 'supp'
                ]); ?>
            </div>
        </div>
    </div>
</div>
