<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\StickyKitAsset;

/* @var $this yii\web\View */

$this->registerCssFile('/css/staticPage.css');
$this->registerJsFile('/js/staticPage.js');
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyB5sXmmhv29RSYXsyobgNq4gks1OT-zLr0&libraries=places&language=en&callback=initAutocomplete');
StickyKitAsset::register($this);
?>

<div class="l-action l-action--empty">
    <div class="l-mainContent">
        <div class="l-action__content--destination">
            <div class="l-action__formWrap">
                <?php $form = ActiveForm::begin([
                    'action' => '/search',
                    'method' => 'get',
                    'class' => 'c-searchForm'
                ]) ?>
                <div class="l-action__formItem item">
                    <div class="c-searchForm__inputBlock">
                        <?= $form->field($tourSearch, 'name')->textInput(['class' => 'c-searchForm__input', 'placeholder' => 'Where are you going?', 'required' => true])->label(false) ?>
                        <div class="c-searchForm__img">
                            <svg class="c-searchForm__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 430.114 430.114">
                                <path d="M356.208 107.051c-1.531-5.738-4.64-11.852-6.94-17.205C321.746 23.704 261.611 0 213.055 0 148.054 0 76.463 43.586 66.905 133.427v18.355c0 .766.264 7.647.639 11.089 5.358 42.816 39.143 88.32 64.375 131.136 27.146 45.873 55.314 90.999 83.221 136.106 17.208-29.436 34.354-59.259 51.17-87.933 4.583-8.415 9.903-16.825 14.491-24.857 3.058-5.348 8.9-10.696 11.569-15.672 27.145-49.699 70.838-99.782 70.838-149.104v-20.262c.001-5.347-6.627-24.081-7-25.234zm-141.963 92.142c-19.107 0-40.021-9.554-50.344-35.939-1.538-4.2-1.414-12.617-1.414-13.388v-11.852c0-33.636 28.56-48.932 53.406-48.932 30.588 0 54.245 24.472 54.245 55.06 0 30.587-25.305 55.051-55.893 55.051z"></path>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="l-action__formItem l-action__formItem--button item">
                    <?= Html::submitButton('Search', ['class' => 'c-button']) ?>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>
<!--<div class="l-tools">
    <div class="l-tools__content l-mainContent cf">
        <div class="l-tools__leftPart">
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Main
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Cookies
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>-->
<div class="l-mainContent cf">
    <div class="l-columnPage">
        <div class="l-columnPage__title">
            <?= $page['title'] ?>
        </div>
        <div class="l-columnPage__block cf">
            <div class="l-columnPage__leftSide">
                <p class="l-columnPage__text">
                    <?= $page['text'] ?>
                </p>
                <div class="l-columnPage__bottom">
                    <div class="l-columnPage__bottomTitle">
                        TripsPoint Help & Support:
                    </div>
                    <div class="l-columnPage__phone">
                        <svg class="l-columnPage__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 578.106 578.106">
                            <path d="M577.83,456.128c1.225,9.385-1.635,17.545-8.568,24.48l-81.396,80.781c-3.672,4.08-8.465,7.551-14.381,10.404c-5.916,2.857-11.729,4.693-17.439,5.508c-0.408,0-1.635,0.105-3.676,0.309c-2.037,0.203-4.689,0.307-7.953,0.307c-7.754,0-20.301-1.326-37.641-3.979s-38.555-9.182-63.645-19.584c-25.096-10.404-53.553-26.012-85.376-46.818c-31.823-20.805-65.688-49.367-101.592-85.68c-28.56-28.152-52.224-55.08-70.992-80.783c-18.768-25.705-33.864-49.471-45.288-71.299c-11.425-21.828-19.993-41.616-25.705-59.364S4.59,177.362,2.55,164.51s-2.856-22.95-2.448-30.294c0.408-7.344,0.612-11.424,0.612-12.24c0.816-5.712,2.652-11.526,5.508-17.442s6.324-10.71,10.404-14.382L98.022,8.756c5.712-5.712,12.24-8.568,19.584-8.568c5.304,0,9.996,1.53,14.076,4.59s7.548,6.834,10.404,11.322l65.484,124.236c3.672,6.528,4.692,13.668,3.06,21.42c-1.632,7.752-5.1,14.28-10.404,19.584l-29.988,29.988c-0.816,0.816-1.53,2.142-2.142,3.978s-0.918,3.366-0.918,4.59c1.632,8.568,5.304,18.36,11.016,29.376c4.896,9.792,12.444,21.726,22.644,35.802s24.684,30.293,43.452,48.653c18.36,18.77,34.68,33.354,48.96,43.76c14.277,10.4,26.215,18.053,35.803,22.949c9.588,4.896,16.932,7.854,22.031,8.871l7.648,1.531c0.816,0,2.145-0.307,3.979-0.918c1.836-0.613,3.162-1.326,3.979-2.143l34.883-35.496c7.348-6.527,15.912-9.791,25.705-9.791c6.938,0,12.443,1.223,16.523,3.672h0.611l118.115,69.768C571.098,441.238,576.197,447.968,577.83,456.128z"/>
                        </svg>
                        +1 234 2000 811
                    </div>
                    <div class="l-columnPage__mail">
                        <svg class="l-columnPage__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">
                            <path d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"></path>
                            <path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8L13.684,2.271z"></path>
                            <polygon points="0,2.878 0,11.186 4.833,7.079"></polygon>
                            <polygon points="9.167,7.079 14,11.186 14,2.875"></polygon>
                        </svg>
                        tp_kiev@gmail.com
                    </div>
                </div>
            </div>
            <div class="l-columnPage__rightSide">
                <div class="c-modalForm">
                    <? if (Yii::$app->session->hasFlash('supportMsg')) { ?>
                        <div class="c-modalForm__alertBlock">
                            <div class="c-modalForm__alertText">
                                Thank you for your message.<br/>
                                We will do our best to contact you as soon as possible.
                            </div>
                            <?= Html::a('Send new message', Yii::$app->request->url, ['class' => 'c-button']) ?>
                        </div>
                    <? } else { ?>
                        <?php $form = ActiveForm::begin([]) ?>
                        <div class="c-modalForm__header">
                            <div class="c-modalForm__title">
                                Contact us for any additional information.
                            </div>
                            <div class="c-modalForm__text">
                                Start to enter country and city to find Tripspoint contacts at your location.
                            </div>
                            <div class="c-modalForm__searchBlock">
                                <input type="text" id="locationAutocomplete" class="c-modalForm__input" placeholder="Country, city">
                                <?= $form->field($supportMsg, 'country')->hiddenInput()->label(false) ?>
                            </div>
                        </div>
                        <div class="c-modalForm__body">
                            <div class="c-modalForm__form cf">
                                <?= $form->field($supportMsg, 'name')->textInput(['class' => 'c-modalForm__input', 'placeholder' => 'Name'])->label(false) ?>
                                <?= $form->field($supportMsg, 'email')->textInput(['class' => 'c-modalForm__input', 'placeholder' => 'Email'])->label(false) ?>
                                <?= $form->field($supportMsg, 'text')->textarea(['class' => 'c-modalForm__input c-modalForm__textarea', 'placeholder' => 'Message'])->label(false) ?>
                                <?= Html::submitButton('Send', ['class' => 'c-button']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end() ?>
                    <? } ?>
                    <div class="c-modalForm__footer cf">
                        <div class="c-modalForm__flag">
                            <img class="c-modalForm__img" src="/img/additionalPages/Ukraine_Flag.png">
                        </div>
                        <div class="c-modalForm__content">
                            <div class="c-modalForm__country">
                                Ukraine, Kiev
                            </div>
                            <div class="c-modalForm__text--bold">
                                Help & Support
                            </div>
                            <div class="c-modalForm__bottom">
                                <div class="c-modalForm__phone">
                                    <svg class="c-modalForm__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 578.106 578.106">
                                        <path d="M577.83,456.128c1.225,9.385-1.635,17.545-8.568,24.48l-81.396,80.781c-3.672,4.08-8.465,7.551-14.381,10.404c-5.916,2.857-11.729,4.693-17.439,5.508c-0.408,0-1.635,0.105-3.676,0.309c-2.037,0.203-4.689,0.307-7.953,0.307c-7.754,0-20.301-1.326-37.641-3.979s-38.555-9.182-63.645-19.584c-25.096-10.404-53.553-26.012-85.376-46.818c-31.823-20.805-65.688-49.367-101.592-85.68c-28.56-28.152-52.224-55.08-70.992-80.783c-18.768-25.705-33.864-49.471-45.288-71.299c-11.425-21.828-19.993-41.616-25.705-59.364S4.59,177.362,2.55,164.51s-2.856-22.95-2.448-30.294c0.408-7.344,0.612-11.424,0.612-12.24c0.816-5.712,2.652-11.526,5.508-17.442s6.324-10.71,10.404-14.382L98.022,8.756c5.712-5.712,12.24-8.568,19.584-8.568c5.304,0,9.996,1.53,14.076,4.59s7.548,6.834,10.404,11.322l65.484,124.236c3.672,6.528,4.692,13.668,3.06,21.42c-1.632,7.752-5.1,14.28-10.404,19.584l-29.988,29.988c-0.816,0.816-1.53,2.142-2.142,3.978s-0.918,3.366-0.918,4.59c1.632,8.568,5.304,18.36,11.016,29.376c4.896,9.792,12.444,21.726,22.644,35.802s24.684,30.293,43.452,48.653c18.36,18.77,34.68,33.354,48.96,43.76c14.277,10.4,26.215,18.053,35.803,22.949c9.588,4.896,16.932,7.854,22.031,8.871l7.648,1.531c0.816,0,2.145-0.307,3.979-0.918c1.836-0.613,3.162-1.326,3.979-2.143l34.883-35.496c7.348-6.527,15.912-9.791,25.705-9.791c6.938,0,12.443,1.223,16.523,3.672h0.611l118.115,69.768C571.098,441.238,576.197,447.968,577.83,456.128z"></path>
                                    </svg>
                                    +1 234 2000 811
                                </div>
                            </div>
                            <div class="c-modalForm__bottom">
                                <div class="c-modalForm__mail">
                                    <svg class="c-modalForm__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">
                                        <path d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"></path>
                                        <path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8L13.684,2.271z"></path>
                                        <polygon points="0,2.878 0,11.186 4.833,7.079"></polygon>
                                        <polygon points="9.167,7.079 14,11.186 14,2.875"></polygon>
                                    </svg>
                                    tp_kiev@gmail.com
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
