$(document).ready(function () {
  /* 100vh height for page 404 */
  var headerHeight = $("header").outerHeight();
  $(".l-404__content").css({height: 'calc(100vh - ' + headerHeight + 'px + 1.25em)'});
});