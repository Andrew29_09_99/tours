var markers = [];
var pickupPoints = $.parseJSON($("#pickuppoints-allpoints").val());
uniqueId = 1;
var infos = [];

function initMap() {
    var map = new google.maps.Map(document.getElementById("map"), {
	  zoom: 5,
      center: new google.maps.LatLng(41.923, 12.513), 
      mapTypeId: google.maps.MapTypeId.ROADMAP
	});
	
    var bounds = new google.maps.LatLngBounds();
	var isRadius = false;
	var isCicle = false;
	for (var i = 0; i < pickupPoints.length; i++) {
		if(pickupPoints[i]['radius']>0){isRadius = true;}
	}
	
    for (var i = 0; i < pickupPoints.length; i++) {
		
    	var markerPosition = new google.maps.LatLng(pickupPoints[i]['lat'], pickupPoints[i]['lng']);
		
        bounds.extend(markerPosition);
        var marker = new google.maps.Marker({
            position: markerPosition,
            icon: '/img/map/map_marker.png',
            map: map
        });
		
        marker.id = (pickupPoints[i]['id'])?pickupPoints[i]['id']:pickupPoints[i]['lat'];
        //Add marker to the array.
        markers.push(marker);
		
		if(pickupPoints[i]['radius']>0 && pickupPoints[i]['lat'] && pickupPoints[i]['lng'])
		{
			
			currentlatlng = new google.maps.LatLng(pickupPoints[i]['lat'], pickupPoints[i]['lng']);
			var circle = new google.maps.Circle({
			strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: currentlatlng,
            radius: pickupPoints[i]['radius']*1000
		  
			});
			circle.bindTo('center', marker, 'position');
			var isCicle = true;
		}
		
        // Allow each marker to have an info window
		marker.addListener('click', (function (marker, i) {
            return function () {
				//if(pickupPoints[i]['radius']>0){isRadius = true;}
				if(typeof pickupPoints[i]['id']!="undefined")
				//if(true)
				{
						if(isCicle)
						{
						var butDel ='<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers(' + pickupPoints[i]['id'] + ');'+circle.setMap(null)+';">Delete</button>';
						}
						else
						{
						var butDelAll ='<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers(' + pickupPoints[i]['id'] + ');">Delete</button>';
						}
				}
				else
				{
					if(isCicle)
					{
					var butDel ='<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers(' + marker.id + ');'+circle.setMap(null)+';">Deleteti</button>';
					}
					else
					{
					var butDelAll ='<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers(' + marker.id + ');">Delete</button>';
					}
				}
						
						if(isRadius)
						{
							var content ='<div class="c-mapForm">' +
								'<div class="c-mapForm__title">' +
								'Point' +
								'</div>' +
								'<div class="c-mapForm__block">' +
								'<input type="hidden" id="pointLat" value=' + pickupPoints[i]['lat'] + '>' +
								'<input type="hidden" id="pointLng" value=' + pickupPoints[i]['lng'] + '>' +
								'<div class="c-mapForm__label">' +
								'Name the point' +
								'</div>' +
								'<input class="c-mapForm__input" id="pointName" placeholder="- Point name -" value="' + pickupPoints[i]['name'] + '">' +
								'<div class="c-mapForm__label">' +
								'</div>' +
								'<div class="c-mapForm__label">' +
								'Radius ' +
								'</div>'+
								'<input type="text" class="c-mapForm__input" id="pointRadius" placeholder="- radius in km-" value="' + pickupPoints[i]['radius'] + '" onblur="">'+
								'<div class="c-mapForm__label">' +
								'Departure time' +
								'</div>' +
								'<input type="time" class="c-mapForm__input" id="pointTime" placeholder="- 24-hour format, e.g. 21:00 -" value="' + pickupPoints[i]['time'] + '">' +
								'<div class="c-mapForm__label">' +
								'Additional information' +
								'</div>' +
								'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information">' + pickupPoints[i]['addInfo'] + '</textarea>' +
								'<div class="c-mapForm__buttonGroup">' + butDel +							 
								'<button type="button" class="c-button c-button--noneTransform" onclick="saveDataRadius()">Save the point</button>' +
								'</div>' +
								'</div>' +
								'</div>';
						}
						else
						{
							content ='<div class="c-mapForm">' +
								'<div class="c-mapForm__title">' +
								'Point' +
								'</div>' +
								'<div class="c-mapForm__block">' +
								'<input type="hidden" id="pointLat" value=' + pickupPoints[i]['lat'] + '>' +
								'<input type="hidden" id="pointLng" value=' + pickupPoints[i]['lng'] + '>' +
								'<div class="c-mapForm__label">' +
								'Name the point' +
								'</div>' +
								'<input class="c-mapForm__input" id="pointName" placeholder="- Point name -" value="' + pickupPoints[i]['name'] + '">' +
								'<div class="c-mapForm__label">' +
								'Departure time' +
								'</div>' +
								'<input type="time" class="c-mapForm__input" id="pointTime" placeholder="- 24-hour format, e.g. 21:00 -" value="' + pickupPoints[i]['time'] + '">' +
								'<div class="c-mapForm__label">' +
								'Additional information' +
								'</div>' +
								'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information">' + pickupPoints[i]['addInfo'] + '</textarea>' +
								'<div class="c-mapForm__buttonGroup">' + butDelAll + 
								'<button type="button" class="c-button c-button--noneTransform" onclick="saveData()">Save the point</button>' +
								'</div>' +
								'</div>' +
								'</div>'; 
						}
						var infowindow = new InfoBox({
							closeBoxURL: '',
							content: content
								
						});
						//close the previous infowindow
						closeInfos();
						infowindow.open(map, marker);
						//keep the handle, in order to close it on next click event
						infos[0] = infowindow;
					
				//}
			}
        })(marker, i));
		
        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
		map.setZoom(5);
    }

    if(isCicle)
	{
		google.maps.event.addListener(circle, 'click', CicleHandler,true); 
	}
	
	
	
	map.addListener('click', function(event) {
        var location = event.latLng;

        var marker = new google.maps.Marker({
            position: location,
            icon: '/img/map/map_marker.png',
            map: map
        });

        //marker.id = uniqueId;
        //uniqueId++;
		marker.id=marker.getPosition().lat();
		//	
		if(isCicle)
		{
		//alert('h');
		
		var infowindow = new InfoBox({
				closeBoxURL: '',
				content:
					'<div class="c-mapForm">' +
					'<div class="c-mapForm__title">' +
					'Point' +
					'</div>' +
					'<div class="c-mapForm__block">' +
					'<input type="hidden" id="pointLat" value=' + marker.getPosition().lat() + '>' +
					'<input type="hidden" id="pointLng" value=' + marker.getPosition().lng() + '>' +
					'<div class="c-mapForm__label">' +
					'Name the point' +
					'</div>' +
					'<input class="c-mapForm__input" id="pointName" placeholder="- Point name -">' +
					'<div class="c-mapForm__label">' +
					'Radius ' +
					'</div>'+
					'<input type="text" class="c-mapForm__input" id="pointRadius" placeholder="- radius in km-">'+
					'<div class="c-mapForm__label">' +
					'Departure time' +
					'</div>' +
					'<input type="time" class="c-mapForm__input" id="pointTime" placeholder="- 24-hour format, e.g. 21:00 -">' +
					'<div class="c-mapForm__label">' +
					'Additional information' +
					'</div>' +
					'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information"></textarea>' +
					'<div class="c-mapForm__buttonGroup">' +
					'<button class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers(' + marker.id + ')">Delete</button>' +
					'<button class="c-button c-button--noneTransform" onclick="saveDataRadius()">Save the point</button>' +
					'</div>' +
					'</div>' +
					'</div>'
				
			});
		
				// uncomment if necessary - marker.addListener('click', function() {
				//close the previous infowindow
				closeInfos();
				infowindow.open(map, marker);
				//keep the handle, in order to close it on next click event
				infos[0] = infowindow;
				// uncomment if necessary - });

				//Add marker to the array.
				markers.push(marker);
		
		
		//это не работает
		google.maps.event.addListener(marker, 'click', function () {
			//alert('h1');
			var getInputName='';var getInputTime='';var getInputInfo='';
				for (var i = 0; i < pickupPoints.length; i++) {
					for (var prop in pickupPoints[i]) {
						var value = pickupPoints[i][prop];
						
						if(pickupPoints[i].lat == marker.getPosition().lat() && pickupPoints[i].lng == marker.getPosition().lng())
						{
							var getInputName = pickupPoints[i].name;
							var getInputTime = pickupPoints[i].time;
							var getInputInfo = pickupPoints[i].addInfo;
						}
					}
				}
				
				var infowindow = new InfoBox({
					closeBoxURL: '',
					content:
						'<div class="c-mapForm">'+
							'<div class="c-mapForm__title">' +
								'Edit Point' +
							'</div>'+
							'<div class="c-mapForm__block">'+
								'<input type="hidden" id="pointLat" value='+marker.getPosition().lat()+'>'+
								'<input type="hidden" id="pointLng" value='+marker.getPosition().lng()+'>'+
								'<div class="c-mapForm__label">' +
									'Name the point' +
								'</div>'+
							   '<input type="text" class="c-mapForm__input" id="pointName"  value="'+ getInputName +'" required>'+
								'<div class="c-mapForm__label">' +
									'Departure time' +
								'</div>'+
								'<input type="time" class="c-mapForm__input" id="pointTime" value="'+  getInputTime +'" required>'+
								'<div class="c-mapForm__label">' +
									'Additional information' +
								'</div>'+
								'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information" required>'+ getInputInfo +'</textarea>'+
								'<div class="c-mapForm__buttonGroup">'+
									'<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers('+marker.id+')">Delete</button>'+
									'<button type="button" class="c-button c-button--noneTransform" onclick="saveData()">Save the edit</button>'+
								'</div>'+
							'</div>'+
						'</div>'
				});
				closeInfos();
				infowindow.open(map, marker);
				infos[0] = infowindow;
				markers.push(marker);
			});
		}	
		//
		else
		{	
		
			var infowindow = new InfoBox({
				closeBoxURL: '',
				content:
					'<div class="c-mapForm">' +
					'<div class="c-mapForm__title">' +
					'Point' +
					'</div>' +
					'<div class="c-mapForm__block">' +
					'<input type="hidden" id="pointLat" value=' + marker.getPosition().lat() + '>' +
					'<input type="hidden" id="pointLng" value=' + marker.getPosition().lng() + '>' +
					'<div class="c-mapForm__label">' +
					'Name the point' +
					'</div>' +
					'<input class="c-mapForm__input" id="pointName" placeholder="- Point name -">' +
					'<div class="c-mapForm__label">' +
					'Departure time' +
					'</div>' +
					'<input type="time" class="c-mapForm__input" id="pointTime" placeholder="- 24-hour format, e.g. 21:00 -">' +
					'<div class="c-mapForm__label">' +
					'Additional information' +
					'</div>' +
					'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information"></textarea>' +
					'<div class="c-mapForm__buttonGroup">' +
					'<button class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers(' + marker.id + ')">Delete</button>' +
					'<button class="c-button c-button--noneTransform" onclick="saveData()">Save the point</button>' +
					'</div>' +
					'</div>' +
					'</div>'
				
			});
		
        // uncomment if necessary - marker.addListener('click', function() {
        //close the previous infowindow
        closeInfos();
        infowindow.open(map, marker);
        //keep the handle, in order to close it on next click event
        infos[0] = infowindow;
        // uncomment if necessary - });

        //Add marker to the array.
        markers.push(marker);
		google.maps.event.addListener(marker, 'click', function () {
			//alert('h1');
			var getInputName='';var getInputTime='';var getInputInfo='';
				for (var i = 0; i < pickupPoints.length; i++) {
					for (var prop in pickupPoints[i]) {
						var value = pickupPoints[i][prop];
						
						if(pickupPoints[i].lat == marker.getPosition().lat() && pickupPoints[i].lng == marker.getPosition().lng())
						{
							var getInputName = pickupPoints[i].name;
							var getInputTime = pickupPoints[i].time;
							var getInputInfo = pickupPoints[i].addInfo;
						}
					}
				}
				
				var infowindow = new InfoBox({
					closeBoxURL: '',
					content:
						'<div class="c-mapForm">'+
							'<div class="c-mapForm__title">' +
								'Edit Point' +
							'</div>'+
							'<div class="c-mapForm__block">'+
								'<input type="hidden" id="pointLat" value='+marker.getPosition().lat()+'>'+
								'<input type="hidden" id="pointLng" value='+marker.getPosition().lng()+'>'+
								'<div class="c-mapForm__label">' +
									'Name the point' +
								'</div>'+
							   '<input type="text" class="c-mapForm__input" id="pointName"  value="'+ getInputName +'" required>'+
								'<div class="c-mapForm__label">' +
									'Departure time' +
								'</div>'+
								'<input type="time" class="c-mapForm__input" id="pointTime" value="'+  getInputTime +'" required>'+
								'<div class="c-mapForm__label">' +
									'Additional information' +
								'</div>'+
								'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information" required>'+ getInputInfo +'</textarea>'+
								'<div class="c-mapForm__buttonGroup">'+
									'<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers('+marker.id+')">Delete</button>'+
									'<button type="button" class="c-button c-button--noneTransform" onclick="saveData()">Save the edit</button>'+
								'</div>'+
							'</div>'+
						'</div>'
				});
				closeInfos();
				infowindow.open(map, marker);
				infos[0] = infowindow;
				markers.push(marker);
			});
		
		}
    });
}
function deleteMarkers(id) {
    //Find and remove the marker from the Array
	
    for (var i = 0; i < markers.length; i++) {
        if (markers[i].id == id) {
		//console.log(id);
            //Remove the marker from Map
            markers[i].setMap(null);
            //Remove the marker and data from arrays
            markers.splice(i, 1);
            pickupPoints.splice(i, 1);
        }
    }
    for (var i = 0; i < pickupPoints.length; i++) {
        if (pickupPoints[i].lat == id) {
            pickupPoints.splice(i, 1);
        }
    }

   
    //Insert all PickupPoints into input on the page
    $("#pickuppoints-allpoints").val(JSON.stringify(pickupPoints));
	 closeInfos();
	 initMap();
}


function CicleHandler(event) 
{
		
			//alert('k');
			var location = event.latLng;
			var marker = new google.maps.Marker({
				position: location,
				icon: '/img/map/map_marker.png',
				map: map
			});
			
			//marker.id = uniqueId;
			//uniqueId++;
			marker.id=marker.getPosition().lat();
			google.maps.event.addListener(marker, 'click', function () {
			var getInputName='';var getInputTime='';var getInputInfo='';
				for (var i = 0; i < pickupPoints.length; i++) {
					for (var prop in pickupPoints[i]) {
						var value = pickupPoints[i][prop];
						
						if(pickupPoints[i].lat == marker.getPosition().lat() && pickupPoints[i].lng == marker.getPosition().lng())
						{
							var getInputName = pickupPoints[i].name;
							var getInputTime = pickupPoints[i].time;
							var getInputInfo = pickupPoints[i].addInfo;
							var getInputRadius = pickupPoints[i].radius;
						}
					}
				}
				
				var infowindow = new InfoBox({
					closeBoxURL: '',
					content:
						'<div class="c-mapForm">'+
							'<div class="c-mapForm__title">' +
								'Edit Point' +
							'</div>'+
							'<div class="c-mapForm__block">'+
								'<input type="hidden" id="pointLat" value='+marker.getPosition().lat()+'>'+
								'<input type="hidden" id="pointLng" value='+marker.getPosition().lng()+'>'+
								'<div class="c-mapForm__label">' +
									'Name the point' +
								'</div>'+
							   '<input type="text" class="c-mapForm__input" id="pointName"  value="'+ getInputName +'" required>'+
							   '<div class="c-mapForm__label">' +
								'Radius ' +
								'</div>'+
								'<input type="text" class="c-mapForm__input" id="pointRadius" value="'+  getInputRadius +'" required onblur="checkit()">'+
								'<div class="c-mapForm__label">' +
									'Departure time' +
								'</div>'+
								'<input type="time" class="c-mapForm__input" id="pointTime" value="'+  getInputTime +'" required>'+
								'<div class="c-mapForm__label">' +
									'Additional information' +
								'</div>'+
								'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information" required>'+ getInputInfo +'</textarea>'+
								'<div class="c-mapForm__buttonGroup">'+
									'<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers('+marker.id+')">Delete</button>'+
									'<button type="button" class="c-button c-button--noneTransform" onclick="saveDataRadius()">Save the edit</button>'+
								'</div>'+
							'</div>'+
						'</div>'
				});
				closeInfos();
				infowindow.open(map, marker);
				infos[0] = infowindow;
				markers.push(marker);
			});
			
			var infowindow = new InfoBox({
				closeBoxURL: '',
				content:
					'<div class="c-mapForm">'+
						'<div class="c-mapForm__title">' +
							'Point' +
						'</div>'+
						'<div class="c-mapForm__block">'+
							'<input type="hidden" id="pointLat" value='+marker.getPosition().lat()+'>'+
							'<input type="hidden" id="pointLng" value='+marker.getPosition().lng()+'>'+
							'<div class="c-mapForm__label">' +
								'Name the point' +
							'</div>'+
							'<input class="c-mapForm__input" id="pointName" placeholder="- Point name -" required>'+
							
							'<div class="c-mapForm__label">' +
								'Departure time' +
							'</div>'+
							'<input type="time" class="c-mapForm__input" id="pointTime" placeholder="- 24-hour format, e.g. 21:00 -" required>'+
							'<div class="c-mapForm__label">' +
								'Additional information' +
							'</div>'+
							'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information" required></textarea>'+
							'<div class="c-mapForm__buttonGroup">'+
								'<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers('+marker.id+')">Delete</button>'+
								'<button type="button" class="c-button c-button--noneTransform" onclick="saveData()">Save the point</button>'+
							'</div>'+
						'</div>'+
					'</div>'
			});
			closeInfos();
			infowindow.open(map,marker);
			infos[0] = infowindow;
			markers.push(marker);	
}
function saveDataNew() {
    var point = {};
    point.name = document.getElementById('pointName').value;
    point.time = document.getElementById('pointTime').value;
    point.lat = document.getElementById('pointLat').value;
    point.lng = document.getElementById('pointLng').value;
    point.addInfo = document.getElementById('pointAddInfo').value;
	point.radius = document.getElementById('pointRadius').value;
	if(point.name && point.time && point.lat && point.lng && point.addInfo && isNumeric(point.radius))
	{
		
	}
	else
	{
		// все поля должны быть введены
		return false;
	}
    //Remove similar pickupPoint's data from array
    for (var i = 0; i < pickupPoints.length; i++) {
        if (pickupPoints[i].lat == point.lat && pickupPoints[i].lng == point.lng) {
            pickupPoints.splice(i, 1);
        }
    }

    pickupPoints.push(point);

    //Insert all PickupPoints into input on the page
    $("#pickuppoints-allpoints").val(JSON.stringify(pickupPoints));

    // using setTimeout prevents adding the marker by click on button
	getAdressNew(point.lat,point.lng,point.radius)
    setTimeout(closeInfos, 4);
}
function getAdressNew(lat,lng,radius,alreadydraw) {
      if (lat && lng && radius) 
	  {
        currentlatlng = new google.maps.LatLng(lat, lng);
		var marker = new google.maps.Marker({
		map: map,
		position: currentlatlng,
		icon: '/img/map/map_marker.png'
		});
			var circle = new google.maps.Circle({
			  strokeColor: '#FF0000',
				strokeOpacity: 0.8,
				strokeWeight: 2,
				fillColor: '#FF0000',
				fillOpacity: 0.35,
				map: map,
				center: currentlatlng,
				radius: radius*1000
			  
			});
		
		circle.bindTo('center', marker, 'position');
		//marker.id = uniqueId;
        //uniqueId++;
		marker.id=marker.getPosition().lat();
		google.maps.event.addListener(marker, 'click', function () {
        var getInputName='';var getInputTime='';var getInputInfo='';var getInputRadius='';
			for (var i = 0; i < pickupPoints.length; i++) {
				for (var prop in pickupPoints[i]) {
					
					if(pickupPoints[i].lat == marker.getPosition().lat() && pickupPoints[i].lng == marker.getPosition().lng())
					{
						var getInputName = pickupPoints[i].name;
						var getInputTime = pickupPoints[i].time;
						var getInputInfo = pickupPoints[i].addInfo;
						var getInputRadius = pickupPoints[i].radius;
					}
				}
			}
			
			var infowindow = new InfoBox({
				closeBoxURL: '',
				content:
					'<div class="c-mapForm">'+
						'<div class="c-mapForm__title">' +
							'Edit Point' +
						'</div>'+
						'<div class="c-mapForm__block">'+
							'<input type="hidden" id="pointLat" value='+marker.getPosition().lat()+'>'+
							'<input type="hidden" id="pointLng" value='+marker.getPosition().lng()+'>'+
							'<div class="c-mapForm__label">' +
								'Name the point' +
							'</div>'+
						   '<input type="text" class="c-mapForm__input" id="pointName"  value="'+ getInputName +'" required>'+
						   '<div class="c-mapForm__label">' +
                            'Radius ' +
							'</div>'+
							'<input type="text" class="c-mapForm__input" id="pointRadius" value="'+  getInputRadius +'" required onblur="checkit()">'+
							'<div class="c-mapForm__label">' +
								'Departure time' +
							'</div>'+
							'<input type="time" class="c-mapForm__input" id="pointTime" value="'+  getInputTime +'" required>'+
							'<div class="c-mapForm__label">' +
								'Additional information' +
							'</div>'+
							'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information" required>'+ getInputInfo +'</textarea>'+
							'<div class="c-mapForm__buttonGroup">'+
								'<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers('+marker.id+');'+circle.setMap(null)+';">Delete</button>'+
								'<button type="button" class="c-button c-button--noneTransform" onclick="saveDataNew()">Save the edit</button>'+
							'</div>'+
						'</div>'+
					'</div>'
			});
			closeInfos();
			infowindow.open(map, marker);
			infos[0] = infowindow;
			markers.push(marker);
		});
		//google.maps.event.addListener(circle, 'click', CicleHandler,true); 
		//новый обработчик для отрисовки нового круга
		 //map.addListener('click', function(e) {placeMarkerAndPanTo(e.latLng, map);});

		

	  }
	  else
	  {
        alert('no lat lng radius');
      }
}
function saveData() {
    var point = {};
    point.name = document.getElementById('pointName').value;
    point.time = document.getElementById('pointTime').value;
    point.lat = document.getElementById('pointLat').value;
    point.lng = document.getElementById('pointLng').value;
    point.addInfo = document.getElementById('pointAddInfo').value;
	
    //Remove similar pickupPoint's data from array
    for (var i = 0; i < pickupPoints.length; i++) {
        if (pickupPoints[i].lat == point.lat && pickupPoints[i].lng == point.lng) {
            pickupPoints.splice(i, 1);
        }
    }
    pickupPoints.push(point);
    //Insert all PickupPoints into input on the page
    $("#pickuppoints-allpoints").val(JSON.stringify(pickupPoints));
    closeInfos();
	initMap();
}
function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
function saveDataRadius() {
    var point = {};
    point.name = document.getElementById('pointName').value;
    point.time = document.getElementById('pointTime').value;
    point.lat = document.getElementById('pointLat').value;
    point.lng = document.getElementById('pointLng').value;
    point.addInfo = document.getElementById('pointAddInfo').value;
	point.radius = document.getElementById('pointRadius').value;
	if(point.name && point.time && point.lat && point.lng && point.addInfo && isNumeric(point.radius))
	{
		
	}
	else
	{
		// все поля должны быть введены
		return false;
	}
    //Remove similar pickupPoint's data from array
    for (var i = 0; i < pickupPoints.length; i++) {
        if (pickupPoints[i].lat == point.lat && pickupPoints[i].lng == point.lng) {
            pickupPoints.splice(i, 1);
        }
    }
    pickupPoints.push(point);
    $("#pickuppoints-allpoints").val(JSON.stringify(pickupPoints));
    closeInfos();
	initMap();
}







function deleteMarker(id) {
    //Find and remove the marker from the Array
	console.log(id);
    for (var i = 0; i < markers.length; i++) {
        if (markers[i].id == id) {
            //Remove the marker from Map
            markers[i].setMap(null);
            //Remove the marker and data from arrays
            markers.splice(i, 1);
            pickupPoints.splice(i, 1);
        }
    }
    
    //Insert all PickupPoints into input on the page
    $("#pickuppoints-allpoints").val(JSON.stringify(pickupPoints));
	 closeInfos();
	 initMap();
}

function closeInfos(){
    if(infos.length > 0){
        //detach the infowindow from the marker
        infos[0].set("marker", null);
        //and close it
        infos[0].close();
        //blank the array
        infos.length = 0;
    }
}

$(document).ready(function() {
    // init Google Maps
    initMap();

    // init jQuery steps
    $(".l-addTour").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "fade",
        enableFinishButton: false,
        labels: {
            previous: "< Previous step",
            next: "Next step"
        },

        onInit: function (event, currentIndex) {
            $('.actions > ul > li:first-child').attr('style', 'display:none');
        },
		onStepChanging: function (event, currentIndex, newIndex) {
			
			if (currentIndex == 0) {
				var step_typeid = document.getElementById('tour-typeid').selectedIndex;
				var steptourname = document.getElementById('tour-name').value;
				var steplocation = document.getElementById('locationAutocomplete').value;
				var stepImageTitle = $('#imageTitle').html();
				var steptourphotos = $('.kv-file-content').html();
				//alert('stepImageTitle='+stepImageTitle);
				//alert('steptourphotos='+steptourphotos);
				if(step_typeid && steptourname && steplocation && stepImageTitle!="" && typeof steptourphotos!="undefined")
				{
					return true;
				} 
				else
				{
                                        
					//return true;
					return false;
				}
			
			}
			else if(currentIndex == 1)
			{
				
				var steptduration0 = $('input[name="Tour[duration][0]"]').val();
				var steptduration1 = $('input[name="Tour[duration][1]"]').val();
				var steptduration2 = $('input[name="Tour[duration][2]"]').val();
				
				var steptourdescshort = $('#tour-descshort').val();
				
				var steptourdesclong = $('#tour-desclong').val();
				
				var stepttourdescextra = document.getElementById('tour-descextra-0').value;
				
				if((steptduration0 || steptduration1 || steptduration2) && steptourdescshort && steptourdesclong && stepttourdescextra)
				{
					return true;
				}
				else
				{
					
					//return true;
					return false;
				}	
			}
			else if(currentIndex == 2)
			{
				
				var steptinc = document.getElementById('tour-inclusion-0').value;
				
				var stepexcl = $('#tour-exclusion-0').val();
				
				//var steptit = $('#tour-itinerary-0-itinerarytime').val();
				var steptit = $('#tour-itinerary').val(); 
				var steptcur = $('input[name="Tour[payMethod][]"]').val();
                                
				console.log('steptinc:' + steptinc);
				console.log('stepexcl:' + stepexcl);
				console.log('steptit:' + steptit);
				console.log('steptcur:' + steptcur);
				
				if(steptinc && stepexcl && steptit && steptcur)
				{
					return true;
				}
				else
				{
					
					//return true;
					return false;
				}	
			}
			else if(currentIndex == 3)
			{
				
				//шаг календарь и цены
				var steptinc = document.getElementById('tour-inclusion-0').value;
				var stepprAd = $('#tour-priceadult').val();
				var steptgr = $('#groups-newgroups-0-price').val();
				
				if(stepprAd || steptgr)
				{
					return true;
				}
				else
				{
					
					//return true;
					return false;
				}	
			}
			else if(currentIndex == 4)
			{
				
				//pickup point
				
				var stephpickup = $('input[name="Tour[hotelPickup]"]').val();
				var steppickup = $('input[name="PickupPoints[allPoints]"]').val();
				//alert(stephpickup);
				
				if(stephpickup)
				{
					return true;
				}
				else if(steppickup)
				{
					return true;
				}
				else
				{
					
					//return false;
				}	
			}
			return true;	
		},
        onStepChanged: function (event, currentIndex, priorIndex) {
            if (currentIndex > 0) {
                $('.actions > ul > li:first-child').attr('style', '');
            } else {
                $('.actions > ul > li:first-child').attr('style', 'display:none');
            }

            if (currentIndex == 5) {
                $('.actions > ul').attr('style', 'display:none');
            }

            // reload map
            if (currentIndex == 4) {
                initMap();
            }
            return true;
        }
    });


    var autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('locationAutocomplete')),
        {types: ['(cities)']});

    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        place.address_components.forEach(function(component) {
            component.types.forEach(function(type) {
                if (type === 'country') {
                    $("#countries-id").val(component.short_name);
                    $("#countries-name").val(component.long_name);
                } else if (type === 'locality') {
                    $("#cities-name").val(component.long_name);
                }
            });
        });
    });

    var cropPreview = $('#cropPreview');
    var cropResult = $('#cropResult');
    var fileInput = $('#fileInput');
    var cropButton = $('#cropButton');
    var imageInfo = $('#imageInfo');
    var imageTitle = $('#imageTitle');
    var deleteImage = $('#deleteImage');
    var promoImage = $('#tourphotos-promoimage');

    var uploadCrop = cropPreview.croppie({
        // inner borders
        viewport: {
            width: 1024,
            height: 400
        }
    });

    fileInput.on('change', function (e) {
        var reader = new FileReader();
        reader.onload = function (e) {
            uploadCrop.croppie('bind', {
                url: e.target.result
            });
        };
        reader.readAsDataURL(this.files[0]);

        imageTitle.text(e.target.files[0].name);

        fileInput.prop('disabled', true);
        imageInfo.show();
        cropButton.show();
    });

    cropButton.on('click', function () {
        uploadCrop.croppie('result', {
            size: 'viewport',
            format: 'jpeg'
        }).then(function (resp) {
            if (resp.indexOf('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') === -1) {
                cropPreview.parent().hide();
                cropButton.hide();

                promoImage.val(resp);

                var html = '<img src="' + resp + '" />';
                cropResult.html(html);
            } else {
                alert('Please, try again to crop or choose another image')
            }
        });
    });

    deleteImage.on("click", function() {
        cropResult.empty();
        cropPreview.parent().show();

        //reset croppie plugin
        uploadCrop.croppie('bind', {
            url: ''
        });

        fileInput.val('');
        fileInput.prop('disabled', false);
        imageInfo.hide();
        cropButton.hide();
    });

    $('#removePoints').click(function() {
        //Remove all markers from the map
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        //Clear array of markers
        markers = [];
        //Clear all pickupPoints data and array of it
        $("#pickuppoints-allpoints").val('');
        pickupPoints = [];
    });

    $('input[name=radioTravellers]').change(function(){
        if (this.value == 0) {
            $('#chooseGroup').hide().find(':input').prop('disabled', true);
            $('#chooseTravellers').show().find(':input').prop('disabled', false);
        } else if (this.value == 1) {
            $('#chooseTravellers').hide().find(':input').prop('disabled', true);
            $('#chooseGroup').show().find(':input').prop('disabled', false);
        }
    });
	// add new ones for check prices
	$('#checkAdult').on('change', function() {
        $(this).parent().next().find(':input').prop('disabled', !this.checked);
		$('#tour-netpriceadult').prop('disabled', !this.checked);
		$('#NetcheckAdult').prop('checked', this.checked);
		
    });
	$('#checkChild').on('change', function() {
        $(this).parent().next().find(':input').prop('disabled', !this.checked);
		$('#tour-netpricechild').prop('disabled', !this.checked);
		$('#NetChildHide').removeClass('hide');
		$('#NetcheckChild').prop('checked', this.checked);
		$('#nstyleNetprice').css({'justify-content':'flex-start'});
		$('#NetChildHide').css({'margin-left':'65px'});
    });
	$('#checkInfant').on('change', function() {
        $(this).parent().next().find(':input').prop('disabled', !this.checked);
		$('#tour-netpriceinfant').prop('disabled', !this.checked);
		$('#NetInfantHide').removeClass('hide');
		$('#NetcheckInfant').prop('checked', this.checked);
		$('#nstyleNetprice').css({'justify-content':'flex-start'});
		$('#NetInfantHide').css({'margin-left':'65px'});
    });
	$('#checkSenior').on('change', function() {
        $(this).parent().next().find(':input').prop('disabled', !this.checked);
		$('#tour-netpricesenior').prop('disabled', !this.checked);
		$('#NetSeniorHide').removeClass('hide');
		$('#NetcheckSenior').prop('checked', this.checked);
		$('#nstyleNetprice').css({'justify-content':'flex-start'});
		$('#NetSeniorHide').css({'margin-left':'65px'});
    });
	//price adult
	$('#tour-priceadult').blur(function(){
		if(this.value > 0)
		{
			$('#tour-netpriceadult').focus();
		}
		else
		{
			//alert('Must be a number');
			$('#tour-priceadult').focus();
		}
	});
	$('#tour-netpriceadult').blur(function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('tour-priceadult').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".c-tourForm__chooseTravellers .text-danger").remove();
				},
			success: function (data) {
				$(".c-tourForm__chooseTravellers").append(data);
				if(data){$('#tour-netpriceadult').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#tour-netpriceadult').focus();
		}
	});
	//price child
	$('#tour-pricechild').blur(function(){
		if(this.value > 0)
		{
			$('#tour-netpricechild').focus();
		}
		else if(this.value == 0)
		{
			$('#tour-netpricechild').val(0);
			$('#tour-netpricechild').attr('disabled',true);
			
		}
		else
		{
			//alert('Must be a number');
			$('#tour-pricechild').focus();
		}
	});
	$('#tour-netpricechild').blur(function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('tour-pricechild').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".c-tourForm__chooseTravellers .text-danger").remove();
				},
			success: function (data) {
				$(".c-tourForm__chooseTravellers").append(data);
				if(data){$('#tour-netpricechild').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else if(this.value == document.getElementById('tour-pricechild').value && this.value == 0)
		{
			
			$(".c-tourForm__chooseTravellers .text-danger").remove();
			return;
		}
		else
		{
			//alert('Must be a number');
			
			$('#tour-netpricechild').focus();
			
		}
	});
	//price infant
	$('#tour-priceinfant').blur(function(){
		if(this.value > 0)
		{
			$('#tour-netpriceinfant').focus();
		}
		else if(this.value == 0)
		{
			$('#tour-netpriceinfant').val(0);
			$('#tour-netpriceinfant').attr('disabled',true);
			
		}
		else
		{
			//alert('Must be a number');
			$('#tour-priceinfant').focus();
		}
	});
	$('#tour-netpriceinfant').blur(function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('tour-priceinfant').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".c-tourForm__chooseTravellers .text-danger").remove();
				},
			success: function (data) {
				$(".c-tourForm__chooseTravellers").append(data);
				if(data){$('#tour-netpriceinfant').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else if(this.value == document.getElementById('tour-priceinfant').value && this.value == 0)
		{
			$(".c-tourForm__chooseTravellers .text-danger").remove();
			return;
		}
		else
		{
			//alert('Must be a number');
			$('#tour-netpriceinfant').focus();
		}
	});
	//price senior
	$('#tour-pricesenior').blur(function(){
		if(this.value > 0)
		{
			$('#tour-netpricesenior').focus();
		}
		else if(this.value == 0)
		{
			$('#tour-netpricesenior').val(0);
			$('#tour-netpricesenior').attr('disabled',true);
			
		}
		else
		{
			//alert('Must be a number');
			$('#tour-pricesenior').focus();
		}
	});
	$('#tour-netpricesenior').blur(function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('tour-pricesenior').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".c-tourForm__chooseTravellers .text-danger").remove();
				},
			success: function (data) {
				$(".c-tourForm__chooseTravellers").append(data);
				if(data){$('#tour-netpricesenior').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else if(this.value == document.getElementById('tour-pricesenior').value && this.value == 0)
		{
			$(".c-tourForm__chooseTravellers .text-danger").remove();
			return;
		}
		else
		{
			//alert('Must be a number');
			$('#tour-netpricesenior').focus();
		}
	});
	
	//check price for group
	
	//необходимо прототипировать
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-0-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-0-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-0-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
			$('#tour-priceadult').val('');
			$('#tour-netpriceadult').val('');
			$('#tour-pricechild').val('');
			$('#tour-netpricechild').val('');
			$('#tour-priceinfant').val('');
			$('#tour-netpriceinfant').val('');
			$('#tour-pricesenior').val('');
			$('#tour-netpricesenior').val('');
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-1-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-1-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-1-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-1-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-1-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-2-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-2-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-2-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-2-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-3-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-3-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-3-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-3-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-4-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-4-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-4-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-4-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-5-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-5-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-5-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-5-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-6-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-6-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-6-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-6-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-7-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-7-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-7-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-7-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-8-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-8-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-8-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-8-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-9-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-9-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-9-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-9-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-10-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-10-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-10-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-10-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-11-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-11-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-11-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-11-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-12-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-12-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-12-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-12-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-13-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-13-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-13-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-13-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-14-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-14-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-14-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-14-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-15-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-15-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-15-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-15-netprice').focus();
		}
	});
	// вот до сюда необходимо прототипировать 
   $('#c-addTour__applyTour').click(function(){
        alert("Your new Tour is successfully saved and waiting for TripsPoint.com approval. It could take a while. You will receive confirmation message right after your new Tour is approved. From the moment of approval your new Tour will be listed and available to book on TripsPoint.com");
		
		$('#c-addTour__loading').css({
			'position':'fixed'
		});
		$('#c-addTour__loading').show();
		$('#c-addTour__loading').after('<div style="font-size:40px;text-align:center;">Please wait your data saving...</div>');	
    });
	
	
});