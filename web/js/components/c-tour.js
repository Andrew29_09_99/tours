$(document).ready(function() {
    // add/remove to/from wishlist
    $(".js-wishListHeart").click(function() {
        var currentHeart = $(this);
        if ($(this).data('userId')) {
            $.post('/favorites/create', {userId: $(this).data('userId'), tourId: $(this).data('tourId')})
                .done(function(data){
                    currentHeart.toggleClass('active');
                })
                .fail(function(){
                    alert('Error');
                });
        } else {
            alert('You must be logged in!');
        }
    });
});
