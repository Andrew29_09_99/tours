$(document).ready(function(){
    $('#profile_settings-alert').slideUp(2000);
});

var placeSearch, autocomplete, autocompleteSupp;
var componentForm = {
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};
var componentFormSupp = {
    country: 'long_name',
    locality: 'long_name'
};

function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        {types: ['geocode']});

    autocompleteSupp = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('autocompleteSupplier')),
        {types: ['geocode']});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
    autocompleteSupp.addListener('place_changed', fillInAddressSupp);
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

    for (var component in componentForm) {
        if(component != 'street_number') {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {

            /* Extended functional for merging name street and number of house */
            if (addressType == 'route') {
                var val = place.address_components[i][componentForm[addressType]] + ', ' + place.address_components[i-1]['long_name'];
                document.getElementById(addressType).value = val;
            }
            else if(addressType == 'street_number'){
                continue;
            }
            else {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }
}
function fillInAddressSupp() {
    // Get the place details from the autocomplete object.
    var place = autocompleteSupp.getPlace();

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {

        var addressType = place.address_components[i].types[0];
        if(addressType == 'country') {
            var shortCountry = place.address_components[i].short_name;
            document.getElementById('idCountrySupplier').value = shortCountry;

        }

        var elem = null;

        if(addressType == 'locality'){
            elem = 'localitySupplier';
        }
        else if(addressType == 'country'){
            elem = 'countrySupplier';
        }
        else{
            continue;
        }
        document.getElementById(elem).value = '';
        document.getElementById(elem).disabled = false;

        if (componentFormSupp[addressType]) {

            var val = place.address_components[i][componentFormSupp[addressType]];
            document.getElementById(elem).value = val;
        }
    }
}