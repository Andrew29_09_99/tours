$(document).ready(function() {
    // init jQuery UI Tabs
    $('#c-tabs__reviews').tabs();

    // Toggle form for the exact tour by click on button (based on element's index on the page)
    $('.c-button--reviews').click(function () {
        var buttonIndex = $('#tabs-todo').find('.c-button--reviews').index(this);
        $('#w' + buttonIndex).slideToggle();
    });
});
