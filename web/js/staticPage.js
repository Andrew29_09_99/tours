function initAutocomplete() {
    var autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('locationAutocomplete')),
        {types: ['(cities)']});

    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        place.address_components.forEach(function(component) {
            component.types.forEach(function(type) {
                if (type === 'country') {
                    $("#supportmsg-country").val(component.long_name);
                }
            });
        });
    });
}

$(document).ready(function() {
    // init sticky-kit for support form
    $(".l-columnPage__rightSide").stick_in_parent({
        offset_top: 20
    });
});
