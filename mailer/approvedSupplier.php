<div class="l-mail__block" style="margin: 0;padding: 130px 76px 135px;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;text-align: center;background: #FFF">
    <div class="l-mail__title" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;color: #005096;font-size: 30px;font-weight: 700;margin-bottom: 38px">
        Hi, <?= $params->nameProvider ?> :)
    </div>
    <div class="l-mail__text" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;color: #3b3b3b;font-size: 16px;margin-bottom: 65px;line-height: 1.4">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed pulvinar velit, sit amet fringilla tellus. Proin pulvinar vehicula neque, vitae tempus erat. Quisque nec massa mi. Quisque sit amet vestibulum arcu, vel sodales nisi. Ut in nibh non erat viverra iaculis mattis id mi. Sed ultrices pretium justo in consequat. Duis auctor lacus a ornare luctus. Nam eu turpis lectus. Quisque blandit mi sapien, non cursus justo laoreet sit amet. Proin elementum elit lectus, a gravida justo ullamcorper fringilla. Suspendisse consequat elit a blandit vulputate.
    </div>
</div>