<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>

<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <?php $this->head() ?>
</head>
<body style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;height: 100%;line-height: 1">
    <?php $this->beginBody() ?>
    <div class="l-wrapper" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;height: 100%;width: 100%;display: table">
        <div class="l-content" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;height: 100%;display: table-row;background: #fff">
            <div class="l-mail" style="margin: 0 auto;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 900px">
                <div class="l-mail__header" style="background: #f7f7f7;height: 150px;width: 100%;padding: 24px 50px;border: 1px solid #dcdcdc;margin: 0;font: inherit;vertical-align: baseline;box-sizing: border-box">
                    <div class="l-mail__logo" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;float: left">
                        <a class="l-mail__logoLink" href="<?= Url::to(['/site/index'], true) ?>" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box">
                            <img src="https://tripspoint.com/web/img/mail/logoformail.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                        </a>
                    </div>
                    <div class="l-mail__socialPart" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;float: right;margin-top: 40px">
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/pinterest.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/youtube.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/google-plus.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/facebook-app-logo.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/social.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/instagram.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/vimeo-logo.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="l-mail__content" style="margin: 0;padding: 50px;border: 1px solid #dcdcdc;font: inherit;vertical-align: baseline;box-sizing: border-box;background-image: url('https://tripspoint.com/web/img/mail/backgroundphotoMail.png'); background-position: center center; background-repeat: no-repeat; background-size: cover;">
                    <?= $content ?>
                </div>
                <div class="l-mail__footer" style="background: #f7f7f7;height: 150px;width: 100%;padding: 50px;border: 1px solid #dcdcdc;margin: 0;font: inherit;vertical-align: baseline;box-sizing: border-box">
                    <div class="l-mail__phone" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;color: #888;font-weight: 700;float: left">
                        <div class="l-mail__phoneText" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;font-size: 13px;margin-bottom: 10px;font-weight: 700">
                            TripsPoint Help & Support Line
                        </div>
                        <a class="l-mail__number" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;font-size: 21px;color: #888;font-weight: 700">
                            +1 234 2000 811
                        </a>
                    </div>
                    <div class="l-mail__socialPart l-mail__socialPart--footer" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;float: right;margin-top: 10px">
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/pinterest.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/youtube.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/google-plus.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/facebook-app-logo.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/social.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/instagram.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                        <div class="l-mail__social" style="margin: 0 15px;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;display: inline-block">
                            <div class="c-social" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;width: 100%;height: 100%">
                                <a class="c-social__link" href="#" target="_blank" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;outline: 0;box-sizing: border-box;text-align: center;width: 100%;display: block;text-decoration: none">
                                    <img src="https://tripspoint.com/web/img/mail/vimeo-logo.png" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

